# Uncertainty Specification and Analysis for Thermosphere Observations (USATO)

This repository is the official implementation of the following paper.

* Paper title: [Uncertainty of thermosphere mass density observations derived from accelerometer and GNSS tracking data](https://doi.org/10.1016/j.asr.2024.02.057)

## Description

The software can specify the uncertainty of thermosphere mass density observations derived from accelerometer and GNSS tracking data. The user must specify the accuracy of input parameters describing the measurements, satellite geometry, surface properties, atmospheric conditions, and radiation fluxes. These uncertainties are propagated to the uncertainty of density observations. The software will generate several visualizations to assess and analyze the accuracy of the density observations. The software is implemented in Matlab and uses several external libraries linked via Matlab's MEX functionality.
   
The software can be run by entering 'simulator' in the Matlab command window, which triggers the execution of the main function in file 'simulator.m'. The file 'simulator_settings.m' controls what the software does by defining key/value pairs.  A manual of the software is available in subdirectory 'manual', which explains the meaning of all key/value pairs. The software output is visualizations and a data file stored in the directory specified in 'simulator_settings.m'.

## Documentation

Subdirectory 'manual' contains a manual of the software in MS Word and PDF formats.

## Author(s)

This software has been developed by

**Christian Siemes** ![ORCID logo](https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png) [0000-0001-8316-1130](https://orcid.org/0000-0001-8316-1130), c.siemes@tudelft.nl, Delft University of Technology


## Installation  

The software has been developed using Matlab version R2023b. It will likely work for older and newer versions of Matlab.

The external libraries need to be compiled on the user's system. Change to subdirectory 'external' to find the external libraries.
* For the IERS interface, edit the file 'IERS_interface/compile_mex_wrappers.m' to specify the compiler and architecture of your system. Then change to subdirectory 'IERS_interface' and run 'compile_mex_wrappers' in the Matlab command window.
* For NRLMSISE-00, change to subdirectory 'NRLMSISE-00' and run 'compile_nrlmsise00_mex_wrapper' in the Matlab command window.
* For the SOFA library, open the bash or other terminal/shell of your choice, change to directory 'SOFA_library/20190722/c/src', and edit the 'makefile' following the instructions in that file. Note that the install directory has been set to '../../../' so that the compiled library will be copied to subdirectory 'SOFA_library/lib' and the header file to subdirectory 'SOFA_library/lib'. Run the command 'make' to compile the SOFA library. Then, in Matlab, change to subdirectory 'src/SOFA_interface' and run 'compile_mex_wrappers' in the Matlab command window.

After these installation steps, the software is ready to be used.

You can test the installation by running 'simulator' without changing the file 'simulator settings.m'. This will generate visualizations similar to those presented by Siemes et al. (2024), which are included in the distribution.

## Structure

The software is structured as follows.
*  In the root directory, the file 'simulator.m' is the software's main function. File 'simulator_settings.m' defines key/value pairs to control what the software does.
* All other source code is located in subdirectory 'src'.
* The software uses external libraries and code in subdirectory 'external'.
* Subdirectory 'manual' contains a manual of the software in MS Word and PDF formats.
* Example input data files with data from the GRACE mission are located in the subdirectory 'my_input_data', which has been used by Siemes et al. (2024).
* Example output data is located in subdirectory 'my_output_data'.

## License

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)  

The contents of this repository are licensed under an **Apache License 2.0** license (see LICENSE file), except for the contents in the directory 'external' subject to the following licenses:
* external/IERS_interface is public domain software
* external/NRLMSISE-00 is public domain software
* external/SOFA_library is subject to the SOFA Software License (see file external/SOFA_library/SOFA_license.txt) 

Copyright notice:  

Technische Universiteit Delft hereby disclaims all copyright interest in the program “USATO”. USATO is a Matlab software for specifying the uncertainty of thermosphere observations written by the Author(s). 
Henri Werij, Dean of Faculty of Aerospace Engineering, Technische Universiteit Delft.

&copy; 2024, C. Siemes

## References

In the future, we will use the software to specify the uncertainty of the observations provided on our [webpage](http://thermosphere.tudelft.nl).

## Citation

If you use this software, please cite it as indicated below or check out the CITATION.cff file.

**How to cite this repository**: Siemes, C., 2024. Uncertainty Specification and Analysis for Thermosphere Observations (USATO). 4TU.ResearchData. Software. https://doi.org/10.4121/08c86d28-e44e-496d-a3b3-185ec43475d4

## Would you like to contribute?

If you have any comments, feedback, or recommendations, feel free to **reach out** by sending an email to c.siemes@tudelft.nl

Thank you, and enjoy!



