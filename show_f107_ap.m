%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Uncertainty Specification and Analysis for Thermosphere Observations
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This software uses several external source codes and libraries, which are
% subject to their own licenses. These source codes and libraries are the
% following:
%
% 1) SOFA library
%
%    This software uses the SOFA library for the conversion of time values
%    and for constructing the transformation from the celestial to the 
%    terrestrial reference frame following the IERS conventions. The SOFA
%    library is available under license, where the license text is included
%    in its source code files. This software implements an interface to the
%    SOFA library via Matlab's mex functionality, which was not provided or
%    endorsed by SOFA. Acknowledgement:
% 
%    Software Routines from the IAU SOFA Collection were used. Copyright
%    (c) International Astronomical Union Standards of Fundamental
%    Astronomy (http://www.iausofa.org)
%
% 2) NRLMSISE-00 C code implementation by Dominik Brodowski
%
%    Dominik Brodowski created a C code implementation of the NRLMSISE-00
%    model, which is available in the public domain. The C code can be
%    accessed here: https://www.brodo.de/space/nrlmsise/ (last accessed on
%    7 January 2024)
%
% 3) The "interp.f" source code by Ch. Bizouard, Observatoire de Paris
%
%    The function implements the interpolation of Earth orientation
%    parameters from the IERS. It can be accessed here:
%    ftp://hpiers.obspm.fr/iers/models/interp.f (last accessed on
%    7 January 2024)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


addpath('src')
load('../data/space_weather_indices.mat','t_ap','ap','t_f107','f107')

figure
H1 = subplot(2,1,1);
plot(t_f107,f107,'color',[1,1,1]*0.5)
hold on
plot(t_f107,conv(f107,ones(81,1)/81,'same'),'k','LineWidth',2)
hold off
title('F10.7 solar radio flux index')
ylabel('sfu')
legend('daily','81-day mean')
datetick('x',10)
fix_figure() % need for dark mode in Matlab 2023b for M processors

H2 = subplot(2,1,2);
plot(t_ap,ap,'k','LineWidth',2)
title('ap index')
ylabel('2 nT')
linkaxes([H1,H2],'x')
xlim([datenum(2000,1,1),datenum(2024,1,1)])
datetick('x',10)
fix_figure() % need for dark mode in Matlab 2023b for M processors
print -dpng space_weather_indices.png


