%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Uncertainty Specification and Analysis for Thermosphere Observations
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This software uses several external source codes and libraries, which are
% subject to their own licenses. These source codes and libraries are the
% following:
%
% 1) SOFA library
%
%    This software uses the SOFA library for the conversion of time values
%    and for constructing the transformation from the celestial to the 
%    terrestrial reference frame following the IERS conventions. The SOFA
%    library is available under license, where the license text is included
%    in its source code files. This software implements an interface to the
%    SOFA library via Matlab's mex functionality, which was not provided or
%    endorsed by SOFA. Acknowledgement:
% 
%    Software Routines from the IAU SOFA Collection were used. Copyright
%    (c) International Astronomical Union Standards of Fundamental
%    Astronomy (http://www.iausofa.org)
%
% 2) NRLMSISE-00 C code implementation by Dominik Brodowski
%
%    Dominik Brodowski created a C code implementation of the NRLMSISE-00
%    model, which is available in the public domain. The C code can be
%    accessed here: https://www.brodo.de/space/nrlmsise/ (last accessed on
%    7 January 2024)
%
% 3) The "interp.f" source code by Ch. Bizouard, Observatoire de Paris
%
%    The function implements the interpolation of Earth orientation
%    parameters from the IERS. It can be accessed here:
%    ftp://hpiers.obspm.fr/iers/models/interp.f (last accessed on
%    7 January 2024)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = simulator()
% encapsulating the calculation in a function to avoid polluting the workspace


% fetch simulator settings (scripts will generate data structures with settings)
simulator_settings

% we always need the EOP file
EOP_FILE = 'EOP_20_C04_one_file_1962-now.txt';

% add path to source code and data
pth = pwd();
if ispc()
    addpath([pth '\src'])
    addpath([pth '\data'])
else
    addpath([pth '/src'])
    addpath([pth '/data'])
end

%--------------------------------------
% simulation settings
%--------------------------------------

% length of simulation in days and time step in seconds
dt = SIM.period; % days
time_step = SIM.time_step; % seconds
consider_averaging = SIM.consider_averaging;
use_real_data = SIM.use_real_data;


%--------------------------------------
% load monthly maps for albedo
% and Earth infrared irradiance
%--------------------------------------

load('ceres_radiation_flux_data.mat')

RP_COV_INPUTS.Pa_fraction = RP_COV_INPUTS.Pa_fraction * ones(size(M_alb.pixel_areas)); % careful: fraction of pixel, not the full Earth
RP_COV_INPUTS.Pi_fraction = RP_COV_INPUTS.Pi_fraction * ones(size(M_ir.pixel_areas)); % careful: fraction of pixel, not the full Earth



%--------------------------------------
% constants
%--------------------------------------

% 1 astronomical unit
AU = 149597870700; % m

% speed of light
c = 299792458;

% radiation pressure at distance of 1 AU (W/m2)
P = 1367;

% solar radiation flux at 1 AU (W/m*s)
Phi = 4.56e-6; % = P/c

% define gravity field
R  = 6378136.60;
GM = 3.986004415e14;
J2 = 1082.645e-6;


%--------------------------------------
% solar and geomagnetic activity
%--------------------------------------

% geomagnetic and solar indices
load('space_weather_indices.mat','t_ap','ap','t_f107','f107')


%--------------------------------------
% load real orbit/attitude/mass
% or use simulated ones 
%--------------------------------------

if use_real_data
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % load GRACE A satellite data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % exportdataproducts.exe GA_TOLEOS_V1/Orbit_ITRF GA_TOLEOS_V1/Attitude_Quaternions_ICRF2SCF GA_TOLEOS_V1/Spacecraft_Mass  t=GPS:20080101,20080102 > ~/GA_RP_TEST_INPUT.txt
    d = read_nrtdm(DATA.INPUT_FILE);

    DATA.t_gps = datenum(d(:,1:6));
    DATA.rt = d(:,7:9); % satellite positions in Earth-fixed, Earth-centered reference (ITRF) frame (m)
    DATA.vt = d(:,10:12); % satellite velocities in Earth-fixed, Earth-centered reference (ITRF) frame (m/s)
    DATA.q = d(:,13:16); % satellite attitude quaternions connecting the celestial (ICRF) and satellite body (SBF) reference frames (-)
    DATA.m = d(:,17); % satellite mass (kg)

    % max_epochs = 2000;
    max_epochs = numel(DATA.t_gps);

    % shorten the time series to keep computation time reasonable
    fn = fieldnames(DATA);
    for k = 1:numel(fn)
        if ~strcmp(fn{k},'INPUT_FILE')
            DATA.(fn{k}) = DATA.(fn{k})(1:max_epochs,:);
        end
    end
    
    % Earth orientation parameters from IERS
    DATA.EOP = read_iers_eop_c20(EOP_FILE); 
    
    % ITRF - ICRF rotation matrix
    DATA.R_c2t = iers2010_gcrs2itrs_matrix(DATA.EOP,interface_Cal2jd(gps2utc(datevec(DATA.t_gps))));
    DATA.R_t2c = DATA.R_c2t(:,[1 4 7 2 5 8 3 6 9]);
    
    % transform position and velocity from ITRF to ICRF
    DATA.rc = rot_mat_times_vec_vec(DATA.R_t2c,DATA.rt);
    DATA.vc = rot_mat_times_vec_vec(DATA.R_t2c,DATA.vt);

    % orbital period from position and velocity (Montenbruck and Gill, 2012, Sect 2.2.4)
    T_orb = median(2*pi*sqrt((2./sqrt(sum(DATA.rc.^2,2)) - sum(DATA.vc.^2,2)/GM).^-3/GM));
    
    % ICRF - SBF rotation matrix
    DATA.R_c2b = quat2rot_vec(quat_conj_vec(DATA.q(:,[4 1:3])));
    DATA.rb = rot_mat_times_vec_vec(DATA.R_c2b,DATA.rc);
    DATA.vb = rot_mat_times_vec_vec(DATA.R_c2b,DATA.vc);
    
    % setting initial temperatures (data from NRTDM) to mimic reality
    DATA.Tb = 298;
    DATA.T = [272.8500  273.2690  272.8440  272.9840  272.9510  272.8900];


    ta = DATA.t_gps(1);
    tb = DATA.t_gps(end);
    time_step = median(diff(DATA.t_gps))*86400;
    dt = tb - ta;
    t = DATA.t_gps'*86400;
    td = DATA.t_gps';
    N = length(t);

    rx = DATA.rt(:,1)';
    ry = DATA.rt(:,2)';
    rz = DATA.rt(:,3)';
    vx = DATA.vt(:,1)';
    vy = DATA.vt(:,2)';
    vz = DATA.vt(:,3)';
    
    rt = [rx',ry',rz'];
    vt = [vx',vy',vz'];

    gps_sec = gps_time_to_gps_sec(datevec(ta)) + (t-t(1))';

    % high-precision version (for verification purposes)
    EOP = read_iers_eop_c20(EOP_FILE);
    R_c2t = iers2010_gcrs2itrs_matrix(EOP,interface_Cal2jd(gps2utc(gps_sec)));
    R_t2c = R_c2t(:,[1 4 7 2 5 8 3 6 9]);
    
    rc = rot_mat_times_vec_vec(R_t2c,rt);
    vc = rot_mat_times_vec_vec(R_t2c,vt);

else 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % define simulated orbit and attitude
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    ra = R + ORBIT.altitude_at_apogee; % apogee (m)
    rp = R + ORBIT.altitude_at_perigee; % perigee (m)
    ec = (ra-rp)/(ra+rp); % eccentricity (-)
    a = ra/(1+ec); % semi-major axis (m)
    i = ORBIT.inclination; % inclination (deg)
    omega = ORBIT.argument_of_perigee; % argument of perigee (deg)
    Omega = ORBIT.longitude_of_ascending_node; % longitude of the ascending node (deg)
    M0 = ORBIT.true_anomaly; % true anomaly (deg)

    T_orb = 2*pi*sqrt(a^3/GM);


    %---------------------------------------------------
    % define UTC time
    %---------------------------------------------------
    
    d = SIM.start_time;
    ta = d; % start time of simulation (datenum)
    tb = ta + dt; % end time of simulation (datenum)
    t = ta*86400 + (0:time_step:dt*86400); % time vector (s)
    td = t/86400;
    N = length(t);


    %---------------------------------------------------
    % generate orbit
    %---------------------------------------------------
    
    [rx,ry,rz,vx,vy,vz] = kepler_orbit(a,ec,i,omega,Omega,M0,ta,t,GM,R,J2);

    % create Earth orientation matrix
    gps_sec = utc_time_to_gps_sec(datevec(ta)) + (t-t(1))';
    R_c2t = rot_mat_IRF_to_EFRF_approx(gps_sec);
    R_t2c = R_c2t(:,[1 4 7 2 5 8 3 6 9]);


    % position and velocity in celestial (inertial), Earth-centered reference frame (m)
    rc = [rx',ry',rz'];
    vc = [vx',vy',vz'];

    % create Earth orientation matrix
    gps_sec = utc_time_to_gps_sec(datevec(ta)) + (t-t(1))';
    R_c2t = rot_mat_IRF_to_EFRF_approx(gps_sec);

    % orbit in Earth-fixed, Earth-centered reference frame (m)
    rt = rot_mat_times_vec_vec(R_c2t,rc);
    vt = rot_mat_times_vec_vec(R_c2t,vc);

end

% construct LORF - ITRF rotation
er = rt./repmat(sqrt(sum(rt.^2,2)),1,3);
ev = vt./repmat(sqrt(sum(vt.^2,2)),1,3);

ex = ev;           % along-track = x-axis of LORF
ey = cross(er,ev); % cross-track = y-axis of LORF
ez = cross(ex,ey); % zentih = z-axis of LORF

R_l2t = [ex,ey,ez];
R_t2l = R_l2t(:,[1 4 7 2 5 8 3 6 9]);


% velocity of corotating atmosphere in the celestial reference frame
wc = corotating_wind(rc);

% velicity of corotating atmosphere relative to satellite
% (if you are sitting on the satellite, then the 'wind' will come from that direction)
wb = wc - vc;

% velicity of satellite relative to corotating atmosphere in m/s in celestial reference frame
% (this is how the satellite moves through a "stationary atmosphere")
vrc = -wb;


%--------------------------------------
% orbital period to averaging window
%--------------------------------------

% convert units to seconds for the averaging window width
if strcmp(ACC.averaging_unit,'orbits')
    averaging_width = ACC.averaging_period * T_orb;
elseif strcmp(ACC.averaging_unit,'days')
    averaging_width = ACC.averaging_period * 86400;
elseif strcmp(ACC.averaging_unit,'seconds')
    averaging_width = ACC.averaging_period;
else
    error(['unknown unit: ' ACC.averaging_unit])
end

% half of the averaging window width in units of 'samples' or 'epochs'
averaging_half_width = round((averaging_width/time_step - 1) / 2);


%--------------------------------------
% accelerometer and GNSS tracking noise
%--------------------------------------

% standard deviation (precision) of accelerometer measurements
acc_std = ACC.acc_std; % m/s2 (GRACE)

% define position noise in LORF
sx = ACC.sx; % standard deviation assuming one measurement per second (no averaging), m
sy = ACC.sy;
sz = ACC.sz;

Sa_acc = diag(ACC.acc_std.^2); % 3 x 3 covariance matrix, m2/s4

rho_xy = ACC.rho_xy; % no correlation
rho_xz = ACC.rho_xz; % x and z are typically highly correlated
rho_yz = ACC.rho_yz;

sxy = sx * sy * rho_xy; % covariance, m2
sxz = sx * sz * rho_xz;
syz = sy * sz * rho_yz;

Sr = [sx^2 sxy sxz % construct covariance matrix of position noise in LORF
      sxy sy^2 syz
      sxz syz sz^2];

% averaging length for noise component from evaluating the gravity vector
dt_gravity_averaging_acc = ACC.dt_gravity_averaging_acc; % s
dt_gravity_averaging_gnss = time_step * (2 * averaging_half_width + 1); % s

% noise coming from numerical differentiation (only along-track is relevant for density)
position_noise_asd_slope = ACC.position_noise_asd_slope; % assume a slight increase (-0.25 gives a factor 10 increase at f = 0.1 mHz)
hu = 5 + 2 * position_noise_asd_slope;
hs = 1 + 2 * position_noise_asd_slope;

diff_std_acc_x  = sqrt((2*pi)^4 * sx^2 * hs/hu * (1/dt_gravity_averaging_acc)^hu  * ACC.dt_gnss_tracking^hs);
diff_std_acc_y  = sqrt((2*pi)^4 * sy^2 * hs/hu * (1/dt_gravity_averaging_acc)^hu  * ACC.dt_gnss_tracking^hs);
diff_std_acc_z  = sqrt((2*pi)^4 * sz^2 * hs/hu * (1/dt_gravity_averaging_acc)^hu  * ACC.dt_gnss_tracking^hs);

diff_std_gnss_x = sqrt((2*pi)^4 * sx^2 * hs/hu * (1/dt_gravity_averaging_gnss)^hu * ACC.dt_gnss_tracking^hs);
diff_std_gnss_y = sqrt((2*pi)^4 * sy^2 * hs/hu * (1/dt_gravity_averaging_gnss)^hu * ACC.dt_gnss_tracking^hs);
diff_std_gnss_z = sqrt((2*pi)^4 * sz^2 * hs/hu * (1/dt_gravity_averaging_gnss)^hu * ACC.dt_gnss_tracking^hs);


diff_std_acc_xy = diff_std_acc_x * diff_std_acc_y * rho_xy; % covariance, m2/s4
diff_std_acc_xz = diff_std_acc_x * diff_std_acc_z * rho_xz;
diff_std_acc_yz = diff_std_acc_y * diff_std_acc_z * rho_yz;

diff_std_gnss_xy = diff_std_gnss_x * diff_std_gnss_y * rho_xy; % covariance, m2/s4
diff_std_gnss_xz = diff_std_gnss_x * diff_std_gnss_z * rho_xz;
diff_std_gnss_yz = diff_std_gnss_y * diff_std_gnss_z * rho_yz;

Sa_diff_acc = [diff_std_acc_x^2 diff_std_acc_xy diff_std_acc_xz
               diff_std_acc_xy diff_std_acc_y^2 diff_std_acc_yz
               diff_std_acc_xz diff_std_acc_yz diff_std_acc_z^2]; % 3 x 3 covariance matrix, m2/s4

Sa_diff_gnss = [diff_std_gnss_x^2 diff_std_gnss_xy diff_std_gnss_xz
                diff_std_gnss_xy diff_std_gnss_y^2 diff_std_gnss_yz
                diff_std_gnss_xz diff_std_gnss_yz diff_std_gnss_z^2]; % 3 x 3 covariance matrix, m2/s4


%---------------------------------------------------
% define satellite attitude
%---------------------------------------------------

if use_real_data

    R_c2b = DATA.R_c2b;
    R_b2c = R_c2b(:,[1 4 7 2 5 8 3 6 9]);

else

    % satellite x-axis points into flight direction with small adjustment to
    % "put the nose into the wind" to be a bit more aerodynamic
    ex = vrc;
    ex = ex./repmat(sqrt(sum(ex.^2,2)),1,3);
    
    % satellite y-axis is practically cross-track (normal to orbital plane,
    % with small adjustment to "put the nose into the wind" --> see above,
    % we have to use -rc because the z-axis of the body frame is pointing
    % into nadir direction)
    ey = cross(-rc,vrc);
    ey = ey./repmat(sqrt(sum(ey.^2,2)),1,3);
    
    % satellite z-axis is practically in nadir direction
    ez = cross(ex,ey);
    
    % Transform relative velocity from celestial reference frame to satellite
    % reference frame, noting that the matrix [ex,ey,ez] is the rotation from
    % the satellite reference frame to the celestial reference frame
    R_b2c = [ex,ey,ez];
    R_c2b = R_b2c(:,[1 4 7 2 5 8 3 6 9]);

    % approximate relation between LORF and SFB:
    % x_lorf = + x_sfb
    % y_lorf = - y_sfb
    % z_lorf = - z_sfb

end

% relative velocity in satellite body frame
vrb = rot_mat_times_vec_vec(R_c2b,vrc);


%---------------------------------------------------
% acceleration noise (via gravity vector)
%---------------------------------------------------

% construct covariance matrix for position noise (same covariance matrix every epoch)
Sr = repmat(reshape(Sr,1,9),numel(t),1);

% acceleration noise stemming from position noise via evaluation of the
% gravity vector (Sa_grav is defined in the ITRF, units = m2/s4)
Sa_grav = pos2acc_noise(rt,R_l2t,Sr,GM);

% rotation matrix from SBF to ITRF
R_b2t = rot_mat_times_rot_mat_vec(R_c2t,R_b2c);
R_t2b = R_b2t(:,[1 4 7 2 5 8 3 6 9]);

% transform the position covariance matrix from ITRF to SFB
Sa_grav = rot_mat_times_rot_mat_vec(R_t2b,Sa_grav);
Sa_grav = rot_mat_times_rot_mat_vec(Sa_grav,R_b2t);

% consider that this noise will be averaged
Sa_grav_gnss = Sa_grav * ACC.dt_gnss_tracking / dt_gravity_averaging_gnss; 
Sa_grav_acc = Sa_grav * ACC.dt_gnss_tracking / dt_gravity_averaging_acc;


%---------------------------------------------------
% evaluate aerodynamic acceleration
%---------------------------------------------------

% satellite mass
if use_real_data
    SAT.ms = DATA.m;
else
    SAT.ms = SAT.ms * ones(N,1);
end

% atmospheric density
[dm,Ta,Te,dmv,dnv,cmv,molar_mass,ap,f107a,f107] = nrlmsise00(td',rt,t_ap,ap,t_f107,f107);

% aerodynamic coefficient
[Caero,CD,CL] = sentman(vrb,SAT.np,SAT.Ap,dmv,molar_mass*1000,Ta,SAT.alpha_E,repmat(SAT.Tw,N,1));

% aerodynamic acceleration in satellite frame
ab = -0.5 * repmat(dm ./ SAT.ms .* sum(vrb.^2,2),1,3) .* Caero;


%---------------------------------------------------
% evaluate radiation pressure acceleration
%---------------------------------------------------

K = numel(td);

a_rp = zeros(K,3); % total radiation pressure acceleration
a_srp = zeros(K,3); % solar radiation pressure acceleration
a_alb = zeros(K,3); % albedo radiation pressure acceleration
a_irp = zeros(K,3); % Earth infrared radiation pressure acceleration
a_trp = zeros(K,3); % thermal radiation pressure acceleration
T = zeros(K,numel(SAT.Ap)); % panel temperatures
Tb = zeros(K,1); % satellite body temperature

dens = zeros(K,1); % neutral mass density
dens_std = zeros(K,1); % neutral mass density standard deviation

a_rp_std = zeros(K,3); % radiation pressure acceleration standard deviation
T_std = zeros(K,numel(SAT.Ap)); % panel temperatures standard deviation
Tb_std = zeros(K,1); % satellite body temperature standard deviation

acc_std_acc = zeros(K,3); % radiation pressure acceleration standard deviation
acc_std_gnss = zeros(K,3); % radiation pressure acceleration standard deviation

T_std(1,:) = RP_COV_INPUTS.T_std;
Tb_std(1,:) = RP_COV_INPUTS.Tb_std;

% init temperatures
if use_real_data
    T(1,:) = DATA.T;
    Tb(1) = DATA.Tb;
else
    T(1,:) = SAT.Tw;
    Tb(1) = SAT.Tb;
end

% init covariance matrix
Cx1 = init_radiation_pressure_covariance_matrix(RP_COV_INPUTS.T_std,RP_COV_INPUTS.Tb_std,...
    RP_COV_INPUTS.ca_vis_std,RP_COV_INPUTS.cd_vis_std,RP_COV_INPUTS.cs_vis_std,...
    RP_COV_INPUTS.ca_ir_std,RP_COV_INPUTS.cd_ir_std,RP_COV_INPUTS.cs_ir_std,...
    RP_COV_INPUTS.Ap_std,RP_COV_INPUTS.kp_std,RP_COV_INPUTS.Cp_std,RP_COV_INPUTS.ms_std,RP_COV_INPUTS.Cb_std,RP_COV_INPUTS.Pb_std);

if consider_averaging
    average_a_rp = zeros(K,3); % radiation pressure acceleration standard deviation
    average_ab = zeros(K,3); % radiation pressure acceleration standard deviation
    average_a_rp_std = zeros(K,3); % radiation pressure acceleration standard deviation
    average_dens = zeros(K,1); % neutral mass density
    average_dens_std = zeros(K,1); % neutral mass density standard deviation

    average_a_rp_ext_std = zeros(K,3);
    average_a_rp_ext_std = zeros(K,3);

    NT = numel(SAT.Ap) + 1;
    Nx = size(Cx1,1) - NT;
    Nw = 2*averaging_half_width + 1;

    HSHT_Hrp = cell(Nw,1);
    HSHT_Cx1 = cell(Nw,1);
    HSHT_rp_fluxes = cell(Nw,1); % d(a_rp)/d(fluxes) * Cov_mat(fluxes) * d(a_rp)/d(fluxes)'
end


% loop over epochs
tic
for k = 1:K

    % if mod(k,100)==1
    %     disp(sprintf('%d/%d\n',k,K))
    % end

    % rotation from satellite body to Earth-centered, Earth-fixed frame 
    R_b2t = reshape(R_c2t(k,:),3,3) * reshape(R_b2c(k,:),3,3);
    
    % radiation fluxes (unit = W/2) and directions (unit vectors) from the radiation sources to the satellite
    [Ps,Pa,Pi,es,ea,ei] = calculate_radiation_flux_vectors(td(k),rt(k,:)',reshape(R_c2t(k,:),3,3),M_alb,M_ir);

    Piss(k) = sum(sum(Pi));
    Pass(k) = sum(sum(Pa));

    % covariance matrix
    Cx2 = construct_flux_data_covariance_matrix(Ps,Pa,Pi,RP_COV_INPUTS.Ps_fraction,RP_COV_INPUTS.Pa_fraction,RP_COV_INPUTS.Pi_fraction);

    % rotate from Earth-centered, Earth-fixed to satellite body
    % (note that the vectors are transposed, hence the tranposed rotation matrix)
    es = es * R_b2t;
    ea = ea * R_b2t;
    ei = ei * R_b2t;

    % radiation pressure acceleration
    a_srp(k,:) = radiation_pressure_panel(es,Ps,SAT.np,SAT.Ap,SAT.ca_vis,SAT.cd_vis,SAT.cs_vis,SAT.ms(k));
    a_alb(k,:) = radiation_pressure_panel(ea,Pa,SAT.np,SAT.Ap,SAT.ca_vis,SAT.cd_vis,SAT.cs_vis,SAT.ms(k));
    a_irp(k,:) = radiation_pressure_panel(ei,Pi,SAT.np,SAT.Ap,SAT.ca_ir,SAT.cd_ir,SAT.cs_ir,SAT.ms(k));

    % thermal radiation pressure and temperatures
    [a_trp(k,:),T_tmp,Tb_tmp] = thermal_radiation_pressure(time_step,SAT.np,SAT.Ap,SAT.ca_vis,SAT.ca_ir,T(k,:)',Tb(k),SAT.kp,SAT.Cp,SAT.Cb,SAT.Pb,SAT.ms(k),Ps,Pa,Pi,es,ea,ei);

    if k < K
        T(k+1,:) = T_tmp';
        Tb(k+1) = Tb_tmp;
    end

    % total radiation pressure
    a_rp(k,:) = a_srp(k,:) + a_irp(k,:) + a_alb(k,:) + a_trp(k,:);

    % error propagation for radiation pressure (Ca ... a_rp, Cx1 ... temperatures and other input parameters to account for correlations)
    [Ca,Cx1,Hrp] = radiation_pressure_uncertainty(time_step,SAT.np,SAT.Ap,SAT.ca_vis,SAT.cd_vis,SAT.cs_vis,SAT.ca_ir,SAT.cd_ir,SAT.cs_ir,SAT.mp,T(k,:)',Tb(k),SAT.kp,SAT.Cp,SAT.Cb,SAT.Pb,SAT.ms(k),Ps,Pa,Pi,es,ea,ei,Cx1,Cx2);

    % averaging effect on radiation pressure
    if consider_averaging
        ind = mod(k,Nw);
        if ind == 0
            ind = Nw;
        end

        HSHT_Hrp{ind} = Hrp(:,1:NT+Nx); % don't need to store the partials for fluxes
        HSHT_Cx1{ind} = Cx1;
        HSHT_rp_fluxes{ind} = (Hrp(:,NT+Nx+1:end) .* repmat(Cx2',3,1)) * Hrp(:,NT+Nx+1:end)';

        % do calculations only when the window is fully on the data
        if k >= Nw

            % perform summations over past Nw elements
            Cov_rp_average = zeros(3,3);
            Cov_rp_average_ext = zeros(3,3);
            Cov_rp_average_int = zeros(3,3);
            for m = 1:Nw
                Cov_rp_average = Cov_rp_average + HSHT_rp_fluxes{m};
                Cov_rp_average_ext = Cov_rp_average_ext + HSHT_rp_fluxes{m};
            end

            % add parts of sum involving temperatures and parameters other than the fluxes
            for nr = 1:Nw
                % note: HSHT_Cx1{nr}(NT+1:NT+Nx,NT+1:NT+Nx) is the same for all 'nr'
                M_x1_x1_tmp = HSHT_Hrp{nr}(:,NT+1:NT+Nx) * HSHT_Cx1{nr}(NT+1:NT+Nx,NT+1:NT+Nx);
                M_T_x1_tmp = HSHT_Hrp{nr}(:,1:NT) * HSHT_Cx1{nr}(1:NT,NT+1:NT+Nx);
                M_T_T = HSHT_Hrp{nr}(:,1:NT) * HSHT_Cx1{nr}(1:NT,1:NT) * HSHT_Hrp{nr}(:,1:NT)';
                for nc = 1:Nw
                    M_x1_x1 = M_x1_x1_tmp * HSHT_Hrp{nc}(:,NT+1:NT+Nx)';
                    M_T_x1 = M_T_x1_tmp * HSHT_Hrp{nc}(:,NT+1:NT+Nx)';
                    Cov_rp_average = Cov_rp_average + M_x1_x1 + M_T_x1 + M_T_x1' + M_T_T;
                    Cov_rp_average_ext = Cov_rp_average_ext + M_x1_x1;
                    Cov_rp_average_int = Cov_rp_average_int + M_T_T;
                end
            end
            
            % include common factor
            Cov_rp_average = Cov_rp_average / Nw^2;

            Cov_rp_average_ext = Cov_rp_average_ext / Nw^2;
            Cov_rp_average_int = Cov_rp_average_int / Nw^2;

            % note the shift by half the averaging width
            m = k - averaging_half_width;

            average_a_rp_std(m,:) = sqrt(diag(Cov_rp_average));

            average_a_rp_ext_std(m,:) = sqrt(diag(Cov_rp_average_ext));
            average_a_rp_int_std(m,:) = sqrt(diag(Cov_rp_average_int));

            average_a_rp(m,:) = mean(a_rp(k-Nw+1:k,:));
            average_ab(m,:) = mean(ab(k-Nw+1:k,:));

            % design matrix for aerodynamic error propagation
            e = SIM.sensing_direction; % assuming that +x-axis = along track
            H = aerodynamics_uncertainty(vrb(m,:)',SAT.np,SAT.Ap,dmv(m,:)',molar_mass*1000,Ta(m),SAT.alpha_E,T(m,:)',average_ab(m,:)',e,SAT.ms(m));

            H_mat = [H.rho_T H.rho_rho' H.rho_alpha H.rho_Tw' H.rho_A' H.rho_ms H.rho_aax H.rho_aay H.rho_aaz H.rho_vrx H.rho_vry H.rho_vrz];

            % add signal sizes for construction of covariance matrix
            AERO_COV_INPUTS.Ta = Ta(m);
            AERO_COV_INPUTS.rho = dmv(m,:)';
            AERO_COV_INPUTS.Tw = T_std(m,:)';

            % construct noise covariance matrix for average aerodynamic acceleration
            Ca_aero_average = Sa_diff_gnss + reshape(Sa_grav_gnss(m,:),3,3) + Cov_rp_average;

            % assume noise sources for GNSS-derived acceleration noise
            acc_std_total = sqrt(e'*(Sa_diff_gnss + reshape(Sa_grav_gnss(m,:),3,3))*e);
            acc_std_gnss(m,:) = acc_std_total';
            AERO_COV_INPUTS.aa_std = sqrt(e'*Ca_aero_average*e);

            % construct a covariance matrix
            Cov_aero = construct_aerodynamic_covariance_matrix(AERO_COV_INPUTS);

            % error propagation
            Cov_rho = H_mat * Cov_aero * H_mat';

            average_dens_std(m) = sqrt(Cov_rho);
        end
    end

    a_rp_std(k,:) = sqrt(diag(Ca));
    if k < K
        T_std(k+1,:) = sqrt(diag(Cx1(1:numel(SAT.Ap),1:numel(SAT.Ap))));
        Tb_std(k+1) = sqrt(Cx1(numel(SAT.Ap)+1,numel(SAT.Ap)+1));
    end

    % design matrix for aerodynamic error propagation
    e = SIM.sensing_direction; % assuming that +x-axis = along track
    H = aerodynamics_uncertainty(vrb(k,:)',SAT.np,SAT.Ap,dmv(k,:)',molar_mass*1000,Ta(k),SAT.alpha_E,T(k,:)',ab(k,:)',e,SAT.ms(k));

    H_mat = [H.rho_T H.rho_rho' H.rho_alpha H.rho_Tw' H.rho_A' H.rho_ms H.rho_aax H.rho_aay H.rho_aaz H.rho_vrx H.rho_vry H.rho_vrz];

    % add signal sizes for construction of covariance matrix
    AERO_COV_INPUTS.Ta = Ta(k);
    AERO_COV_INPUTS.rho = dmv(k,:)';
    AERO_COV_INPUTS.Tw = T_std(k,:)';

    % construct covariance matrix of aerodynamic acceleration
    Ca_aero = Sa_acc + Sa_diff_acc + reshape(Sa_grav_acc(k,:),3,3) + Ca;

    % assume noise sources for accelerometer
    acc_std_total = sqrt(e'*(Sa_acc + Sa_diff_acc + reshape(Sa_grav_acc(k,:),3,3))*e);
    acc_std_acc(k,:) = acc_std_total';
    AERO_COV_INPUTS.aa_std = sqrt(e'*Ca_aero*e);

    % construct a covariance matrix
    Cov_aero = construct_aerodynamic_covariance_matrix(AERO_COV_INPUTS);

    % error propagation
    Cov_rho = H_mat * Cov_aero * H_mat'; % H_mat = nan?

    dens(k) = dm(k);
    dens_std(k) = sqrt(Cov_rho);
end
if SIM.consider_averaging
    average_dens = -2 * (average_ab*e) ./ (Caero*e) .* SAT.ms ./ sum(vrb.^2,2);
end
toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for testing nice graphics (quick and dirty import)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% load('/Users/csiemes/Matlab/CCMC/GB_2008_11_01/OUTPUT_DATA.mat')
% 
% td = OUTPUT_DATA.td;
% ab = OUTPUT_DATA.ab;
% a_rp = OUTPUT_DATA.a_rp;
% a_rp_std = OUTPUT_DATA.a_rp_std;
% dens = OUTPUT_DATA.dens;
% dens_std = OUTPUT_DATA.dens_std;
% 
% acc_std_acc = OUTPUT_DATA.acc_std_acc;
% acc_std_gnss = OUTPUT_DATA.acc_std_gnss;
% 
% consider_averaging = OUTPUT_DATA.consider_averaging;
% T_orb = OUTPUT_DATA.T_orb;
% averaging_half_width = OUTPUT_DATA.averaging_half_width;
% 
% average_ab = OUTPUT_DATA.average_ab;
% average_dens = OUTPUT_DATA.average_dens;
% average_dens_std = OUTPUT_DATA.average_dens_std;
% average_a_rp = OUTPUT_DATA.average_a_rp;
% average_a_rp_std = OUTPUT_DATA.average_a_rp_std;
% 
% K = numel(td);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% visualize the results of the error propagation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% plot(1e9*average_a_rp_std(:,1),'LineWidth',2)
% hold on
% plot(1e9*average_a_rp_ext_std(:,1),'LineWidth',2)
% plot(1e9*average_a_rp_int_std(:,1),'LineWidth',2)
% hold off
% 
% keyboard

if ~exist(SIM.OUTPUT_DIR,'dir')
    mkdir(SIM.OUTPUT_DIR)
end


tc = mean(td);
tlim = tc + [-1.1,1.1] * T_orb/86400;

n = 1:K;
n2 = averaging_half_width+1:K-averaging_half_width;
th = td(n);
th2 = td(n2);

FS = 14;

% 166,206,227 % light blue
% 31,120,180 % dark blue
% 178,223,138 % light green
% 51,160,44 % dark green
% 251,154,153 % light red
% 227,26,28 % dark red
% 253,191,111 % light orange
% 255,127,0 % dark orange
% 202,178,214 % light purple
% 106,61,154 % dark purple

% option 2 from colorbrewer (dark)
col1_line = [31,120,180]/255;
col2_line = [51,160,44]/255;
% col3_line = [227,26,28]/255;
% col4_line = [255,127,0]/255;

% option 2 from colorbrewer (light due to transparency)
col1_area = [31,120,180]/255;
col2_area = [51,160,44]/255;
% col3_area = [251,154,153]/255;
% col4_area = [253,191,111]/255;


transparency1 = 0.4; % 1 = opaque, 0 = transparent
transparency2 = 0.4;


for ax = 1:3
    figure('Visible','off')
    hf = fill([th,flip(th)]',1e9*[a_rp(n,ax)+a_rp_std(n,ax);flip(a_rp(n,ax)-a_rp_std(n,ax))],col1_area);
    hf.FaceAlpha = transparency1;
    hf.EdgeColor = 'none';
    hold on
    hp1 = plot(th,1e9*a_rp(n,ax),'LineWidth',2,'Color',col1_line);
    hold off
    xlim(tlim)
    ylabel('Acceleration (nm/{s^2})')
    xlabel('Time (days)')
    title('Radiation pressure')
    if consider_averaging
        hold on
        hf = fill([th2,flip(th2)]',1e9*[average_a_rp(n2,ax)+average_a_rp_std(n2,ax);flip(average_a_rp(n2,ax)-average_a_rp_std(n2,ax))],col2_area);
        hf.FaceAlpha = transparency2;
        hf.EdgeColor = 'none';
        hp2 = plot(th2,1e9*average_a_rp(n2,ax),'--','LineWidth',2,'Color',col2_line);
        hold off
        legend([hp1,hp2],'in situ','average')
    end
    datetick('x','HH:MM','keeplimits')
    xlabel(datestr(tc,'dd-mmm-yyyy'))
    fix_figure
    print('-dpng',sprintf('%s/rp_ax%d.png',SIM.OUTPUT_DIR,ax))
    close all
end

for ax = 1:3
    figure('Visible','off')
    hp1 = plot(th,1e9*a_rp_std(n,ax),'LineWidth',2,'Color',col1_line);
    xlim(tlim)
    ylabel('Acceleration (nm/{s^2})')
    xlabel('Time (days)')
    title('Radiation pressure')
    if consider_averaging
        hold on
        hp2 = plot(th2,1e9*average_a_rp_std(n2,ax),'--','LineWidth',2,'Color',col2_line);
        hold off
        legend([hp1,hp2],'in situ','average')
    end
    datetick('x','HH:MM','keeplimits')
    xlabel(datestr(tc,'dd-mmm-yyyy'))
    fix_figure
    print('-dpng',sprintf('%s/rp_std_ax%d.png',SIM.OUTPUT_DIR,ax))
    close all
end

% THIS FIGURE SHOWS THE PANEL TEMPERATURES (NOT NEEDED)
% 
% for panel = 1:numel(Ap)
%     figure('Visible','off')
%     hf = fill([th,flip(th)]',[T(n,panel)+T_std(n,panel);flip(T(n,panel)-T_std(n,panel))],col);
%     hf.FaceAlpha = 0.5;
%     hf.EdgeColor = 'none';
%     hold on
%     plot(th,T(n,panel),'LineWidth',2,'Color',col)
%     hold off
%     % xlim([th(1),th(end)])
%     xlim(tlim)
%     ylabel('Temperature (K)')
%     xlabel('Time (days)')
%     title('Temperature')
% end



figure('Visible','off')
hf = fill([th,flip(th)]',[dens(n)+dens_std(n);flip(dens(n)-dens_std(n))],col1_area);
hf.FaceAlpha = transparency1;
hf.EdgeColor = 'none';
hold on
set(gca,'FontSize',FS)
hp1 = plot(th,dens(n),'LineWidth',2,'Color',col1_line);
hold off
set(gca,'FontSize',FS)
xlim(tlim)
ylabel('Density (kg/{m^3})')
xlabel('Time (days)')
title('Density')
if consider_averaging
    hold on
    hf = fill([th2,flip(th2)]',[average_dens(n2)+average_dens_std(n2);flip(average_dens(n2)-average_dens_std(n2))],col2_area);
    hf.FaceAlpha = transparency2;
    hf.EdgeColor = 'none';
    hp2 = plot(th2,average_dens(n2),'--','LineWidth',2,'Color',col2_line);
    hold off
    legend([hp1,hp2],'in situ','average')
end
datetick('x','HH:MM','keeplimits')
xlabel(datestr(tc,'dd-mmm-yyyy'))
fix_figure
savefig(sprintf('%s/dens.fig',SIM.OUTPUT_DIR))
print('-dpng',sprintf('%s/dens.png',SIM.OUTPUT_DIR))
close all


% THIS FIGURE DUPLICATES INFORMATION (NOT NEEDED)
% 
% figure('Visible','off')
% tmp = average_dens(n2)./average_dens_std(n2);
% hf2 = fill([th2,flip(th2)]',[tmp;100*zeros(size(tmp))],col2t);
% hf2.EdgeColor = 'none';
% hold on
% hp2 = plot(th2,tmp,'LineWidth',2,'color',col2);
% tmp = dens(n)./dens_std(n);
% hf1 = fill([th,flip(th)]',[tmp;zeros(size(tmp))],colt);
% hf1.EdgeColor = 'none';
% hp1 = plot(th,tmp,'LineWidth',2,'color',col);
% hold off
% xlim(tlim)
% xlabel('Time (days)')
% ylabel('SNR')
% legend([hp1,hp2],'in situ','average')
% print('-dpng',sprintf('%s/snr.png',SIM.OUTPUT_DIR))
% close all




% option 2 from colorbrewer (dark)
col1_line = [117,112,179]/255;
col2_line = [217,95,2]/255;
col3_line = [27,158,119]/255;

col1_area = mean([col1_line; 1 1 1]);
col2_area = mean([col2_line; 1 1 1]);
col3_area = mean([col3_line; 1 1 1]);


figure('Visible','off')
tmp1 = 100 * dens_std(n)./dens(n);
hf1 = fill([th,flip(th)]',[tmp1;-ones(size(tmp1))],col1_area);
hf1.EdgeColor = col1_line;
hf1.LineStyle = '-';
hf1.LineWidth = 2;
set(gca,'FontSize',FS)
hold on
set(gca,'FontSize',FS)
% NEED TO IMPLEMENT: something like this sqrt(e' * (Sa_rp + ...) * e)
tmp2 = -100 * sqrt((a_rp_std(:,1).^2 + acc_std_acc(:,1).^2))./(ab*e);
hf2 = fill([th,flip(th)]',[tmp2;-ones(size(tmp2))],col2_area);
hf2.EdgeColor = 'none';
hf2.EdgeColor = col2_line;
hf2.LineStyle = '-';
hf2.LineWidth = 2;
% NEED TO IMPLEMENT: something like this sqrt(e' * (Sa_rp + ...) * e)
tmp3 = -100 * acc_std_acc(:,1)./(ab*e);
hf3 = fill([th,flip(th)]',[tmp3;-ones(size(tmp3))],col3_area);
hf3.EdgeColor = 'none';
hf3.EdgeColor = col3_line;
hf3.LineStyle = '-';
hf3.LineWidth = 2;
hold off
xlim(tlim)
xlabel('Time (days)')
ylabel('Relative error (% of density)')
title('Stacked error')
legend([hf1,hf2,hf3],'Aerodynamics','Radiation pressure','GNSS tracking')
datetick('x','HH:MM','keeplimits')
xlabel(datestr(tc,'dd-mmm-yyyy'))
yl = ylim;
ylim([0,yl(2)]);
fix_figure
savefig(sprintf('%s/error_contributions_in_situ.fig',SIM.OUTPUT_DIR))
print('-dpng',sprintf('%s/error_contributions_in_situ.png',SIM.OUTPUT_DIR))
close all


if consider_averaging
    figure('Visible','off')
    tmp1 = 100 * average_dens_std(n2)./average_dens(n2);
    hf1 = fill([th2,flip(th2)]',[tmp1;-ones(size(tmp1))],col1_area);
    hf1.EdgeColor = 'none';
    hf1.EdgeColor = col1_line;
    hf1.LineStyle = '-';
    hf1.LineWidth = 2;
    hold on
    set(gca,'FontSize',FS)
    % NEED TO IMPLEMENT: something like this sqrt(e' * (Sa_rp + ...) * e)
    tmp2 = -100 * sqrt(average_a_rp_std(n2,1).^2 + acc_std_gnss(n2,1).^2)./(average_ab(n2,:)*e);
    hf2 = fill([th2,flip(th2)]',[tmp2;-ones(size(tmp2))],col2_area);
    hf2.EdgeColor = col2_line;
    hf2.LineStyle = '-';
    hf2.LineWidth = 2;
    % NEED TO IMPLEMENT: something like this sqrt(e' * (Sa_rp + ...) * e)
    tmp3 = -100 * acc_std_gnss(n2,1)./(average_ab(n2,:)*e);
    hf3 = fill([th2,flip(th2)]',[tmp3;-ones(size(tmp3))],col3_area);
    hf3.EdgeColor = col3_line;
    hf3.LineStyle = '-';
    hf3.LineWidth = 2;
    hold off
    xlim(tlim)
    xlabel('Time (days)')
    ylabel('Relative error (% of density)')
    legend([hf1,hf2,hf3],'Aerodynamics','Radiation pressure','GNSS tracking')
    title('Stacked errors')
    datetick('x','HH:MM','keeplimits')
    xlabel(datestr(tc,'dd-mmm-yyyy'))
    title('Stacked error')
    yl = ylim;
    ylim([0,yl(2)]);
    fix_figure
    savefig(sprintf('%s/error_contributions_average.fig',SIM.OUTPUT_DIR))
    print('-dpng',sprintf('%s/error_contributions_average.png',SIM.OUTPUT_DIR))
    close all
end


% option 2 from colorbrewer (dark)
col1_line = [31,120,180]/255;
col2_line = [51,160,44]/255;

figure('Visible','off')
hp1 = plot(th,dens_std(n),'LineWidth',2,'Color',col1_line);
xlim(tlim)
set(gca,'FontSize',FS)
ylabel('Density (kg/m^3)')
xlabel('Time (days)')
title('Density')
if consider_averaging
    hold on
    hp2 = plot(th2,average_dens_std(n2),'LineWidth',2,'Color',col2_line);
    hold off
    legend([hp1,hp2],'in situ','average')
end
datetick('x','HH:MM','keeplimits')
xlabel(datestr(tc,'dd-mmm-yyyy'))
fix_figure
savefig(sprintf('%s/dens_std.fig',SIM.OUTPUT_DIR))
print('-dpng',sprintf('%s/dens_std.png',SIM.OUTPUT_DIR))
close all


NFFT = round(5*T_orb/time_step);
if NFFT > numel(td)
    NFFT = numel(td) - 1;
end
if mod(NFFT,2)~=1
    NFFT = NFFT+1;
end


% % option 2 from colorbrewer (dark)
% col1_line = [117,112,179]/255;
% col2_line = [217,95,2]/255;
% col3_line = [27,158,119]/255;
% 
% colororder([col1_line;col2_line;col3_line])

[p,f] = psd_welch(ab*e,hanning(NFFT),0.5,1/time_step,'onesided'); 

% Sa_grav
psd_dd = (sx * (2*pi*f).^(2-position_noise_asd_slope)).^2;
psd_g = mean(Sa_grav(:,1)) * ones(size(f));
psd_gnss = psd_dd + psd_g;


close all
figure('Visible','off')
loglog(f,sqrt([p psd_gnss*2]),'LineWidth',2) % factor two comes from one-sided vs. two-sided PSD
yl = ylim();
yl(1) = sqrt(min(psd_gnss))/10;
ylim(yl)
xlim([f(1),f(end)])
hold on
loglog([1,1]/T_orb,yl,'k--','LineWidth',2)
hold off
set(gca,'FontSize',FS)
grid on
set(gca,'MinorGridLineStyle','none')
ylabel('ASD (m/s^2)')
xlabel('Frequency (Hz)')
legend('Aerodynamic acceleration','GNSS tracking noise','Orbital frequency')
fix_figure
savefig(sprintf('%s/psd_snr.fig',SIM.OUTPUT_DIR))
print('-dpng',sprintf('%s/psd_snr.png',SIM.OUTPUT_DIR))
close all

OUTPUT_DATA.td = td;
OUTPUT_DATA.ab = ab;
OUTPUT_DATA.a_rp = a_rp;
OUTPUT_DATA.a_rp_std = a_rp_std;
OUTPUT_DATA.dens = dens;
OUTPUT_DATA.dens_std = dens_std;

OUTPUT_DATA.acc_std_acc = acc_std_acc;
OUTPUT_DATA.acc_std_gnss = acc_std_gnss;

OUTPUT_DATA.T_orb = T_orb;

OUTPUT_DATA.consider_averaging = consider_averaging;

if consider_averaging
    OUTPUT_DATA.averaging_half_width = averaging_half_width;
    OUTPUT_DATA.average_ab = average_ab;
    OUTPUT_DATA.average_dens = average_dens;
    OUTPUT_DATA.average_dens_std = average_dens_std;
    OUTPUT_DATA.average_a_rp = average_a_rp;
    OUTPUT_DATA.average_a_rp_std = average_a_rp_std;
end

save(sprintf('%s/OUTPUT_DATA.mat',SIM.OUTPUT_DIR),'OUTPUT_DATA')
