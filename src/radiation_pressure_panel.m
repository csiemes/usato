%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [a] = radiation_pressure_panel(e,P,n,A,ca,cd,cs,m)
% [a] = radiation_pressure_panel(e,P,n,A,ca,cd,cs,m)
%
% S ... number of radiation sources
% P ... number of panels
%
% e .... unit vector toward radiation sources, S x 3
% P .... radiation flux from radiation sources, unit = W/m2, S x 1
%        (P is actually "Phi" in the Montenbruck and Gill book)
% n .... panel normals (unit vector pointing outward), P x 3
% A .... panel areas, P x 1
% ca ... absorption coefficient, P x 1
% cd ... diffuse reflection coefficient, P x 1
% cs ... specular reflection coefficient, P x 1
% m .... satellite mass, unit = kg, 1 x 1
%
% a .... radiation pressure acceleration, unit = m/s2, 1 x 3

% speed of light, unit = m
c = 299792458;

% cosine of angle between outward surface normal and direction to radiation source
gamma = e*n'; % S x P

% panels not illuminated
gamma(gamma < 0) = 0;

% force coefficients: absorption, specular and diffuse reflections, unit = m2, S x 3
Ca = -repmat(gamma*(ca.*A),1,3).*e; % repmat(SxP * Px1,1,3) .* Sx3 = Sx3
Cs = -2*gamma.^2*(repmat(cs.*A,1,3).*n); % SxP * (repmat(Px1,1,3) .* Px3) = Sx3
Cd = -repmat(gamma*(cd.*A),1,3).*e - 2/3*gamma*(repmat(cd.*A,1,3).*n); % repmat(SxP * Px1,1,3) .* Sx3

% radiation pressure force coefficient, unit = m2, S x 3
C = Ca + Cs + Cd;

% total acceleration, unit = m/s2, 1 x 3
a = sum(repmat(P/m/c,1,3) .* C,1); % P is actually "Phi" in the Montenbruck and Gill book


