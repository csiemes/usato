%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [r_sun,r_moon] = low_precision_lunisolar_coordinates_irf(gps_sec)
% [r_sun,r_moon] = low_precision_lunisolar_coordinates_irf(gps_sec)
%
% compute low-precision coordinates of Sun and Moon (orientation in IRF in meters, Earth-centered)
% (see "Satellite orbits" by Montenbruck and Gill, 2000, pp 70-73)

% JD = zeros(size(gps_sec));
% for n = 1:length(gps_sec)
%     [Y,M,D,h,m,s] = gps_sec_to_utc_time(gps_sec(n));
%     D = D + h/24 + m/24/60 + s/24/60/60;
%     MJD = calendar_date_to_modified_julian_date(Y,M,D);
%     JD(n) = modified_julian_date_to_julian_date(MJD);
% end
JD = gps_sec_to_julian_date(gps_sec);

% Julian centuries since J2000 (12h, 1 January 2000)
T = (JD - 2451545.0)/36525.0; 

% Obliquity of J2000 ecliptic [rad]
eps = 23.43929111*pi/180; 
%eps=0;


%% Solar coordinates

% Mean anomaly, ecliptic longitude and radius
M = 2*pi * Frac(0.9931267 + 99.9973583*T);                                        % [rad]
L = 2*pi * Frac(0.7859444 + M/(2*pi) + (6892.0*sin(M)+72.0*sin(2.0*M)) / 1296.0e3); % [rad]
r = 149.619e9 - 2.499e9*cos(M) - 0.021e9*cos(2*M);                                % [m]

% Equatorial position vector
if size(JD,1) == 1
    r_sun(:,1) = r.*cos(L);          % [m]
    r_sun(:,2) = cos(eps)*r.*sin(L); % [m]
    r_sun(:,3) = sin(eps)*r.*sin(L); % [m]
else
    r_sun(1,:) = r.*cos(L);          % [m]
    r_sun(2,:) = cos(eps)*r.*sin(L); % [m]
    r_sun(3,:) = sin(eps)*r.*sin(L); % [m]
end
r_sun = r_sun';

%% Lunar coordinates

% Mean elements of lunar orbit
L_0 =      Frac ( 0.606433 + 1336.851344*T );     % Mean longitude [rev] w.r.t. J2000 equinox
l   = 2*pi*Frac ( 0.374897 + 1325.552410*T );     % Moon's mean anomaly [rad]
lp  = 2*pi*Frac ( 0.993133 +   99.997361*T );     % Sun's mean anomaly [rad]
D   = 2*pi*Frac ( 0.827361 + 1236.853086*T );     % Diff. long. Moon-Sun [rad]
F   = 2*pi*Frac ( 0.259086 + 1342.227825*T );     % Argument of latitude

% Ecliptic longitude (w.r.t. equinox of J2000)
dL = +22640*sin(l) - 4586*sin(l-2*D) + 2370*sin(2*D) +  769*sin(2*l)...
    -668*sin(lp) - 412*sin(2*F) - 212*sin(2*l-2*D) - 206*sin(l+lp-2*D)...
    +192*sin(l+2*D) - 165*sin(lp-2*D) - 125*sin(D) - 110*sin(l+lp)...
    +148*sin(l-lp) - 55*sin(2*F-2*D);

L = 2*pi * Frac( L_0 + dL/1296.0e3 );  % [rad]

% Ecliptic latitude
S  = F + (dL+412*sin(2*F)+541*sin(lp)) / (3600*180/pi);
h  = F-2*D;
N  = -526*sin(h) + 44*sin(l+h) - 31*sin(-l+h) - 23*sin(lp+h) +11*sin(-lp+h) - 25*sin(-2*l+F) + 21*sin(-l+F);
B = ( 18520.0*sin(S) + N ) / (3600*180/pi);   % [rad]

% Distance [m]
R = 385000e3 - 20905e3*cos(l) - 3699e3*cos(2*D-l) - 2956e3*cos(2*D)...
    -570e3*cos(2*l) + 246e3*cos(2*l-2*D) - 205e3*cos(lp-2*D)...
    -171e3*cos(l+2*D) - 152e3*cos(l+lp-2*D);

% Equatorial coordinates
if size(JD,1) == 1
    r_moon(:,1) = R.*cos(L).*cos(B);
    r_moon(:,2) = cos(eps)*R.*sin(L).*cos(B) - sin(eps)*R.*sin(B);
    r_moon(:,3) = sin(eps)*R.*sin(L).*cos(B) + cos(eps)*R.*sin(B);
else
    r_moon(1,:) = R.*cos(L).*cos(B);
    r_moon(2,:) = cos(eps)*R.*sin(L).*cos(B) - sin(eps)*R.*sin(B);
    r_moon(3,:) = sin(eps)*R.*sin(L).*cos(B) + cos(eps)*R.*sin(B);
end
r_moon = r_moon';

function [x] = Frac(x)
    x = x-floor(x);


