%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x,y,z,vx,vy,vz] = kepler_orbit(a,e,i,omega,Omega,M0,t0,t,GM,R,J2)
% [x,y,z,vx,vy,vz] = kepler_orbit(a,e,i,omega,Omega,M0,t0,t,GM)
%
% a ... semi-major axis in km
% e ... eccentricity
% i ... inclination in degrees
% omega ... argument of perigee in degrees
% Omega ... right ascension of ascending node
% M0 ... mean anomaly in degrees
% t0 ... time corresponding to M0 in seconds
% t ... time in seconds
% GM ... gravitational constant times mass of Earth in km^3/s^2
% R ... semi-major axis of Earth in km
%
% x,y,z ... inertial coordinates of orbit in km
% vx,vy,vz ... inertial velocities in km/s


%% convert: degrees --> radians
i = i * pi/180;
omega = omega * pi/180;
Omega = Omega * pi/180;
M0 = M0 * pi/180;

%% mean motion in rad/s
n = sqrt(GM/a^3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% apply secular changes to Kepler orbit due to the flattening of the Earth - start
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% semi-latus rectum in km
p = a*(1-e^2);


%% secular changes due to the flattening of the Earth in radians/sec
dOmega = -3/2*n*J2*(R/p)^2*cos(i);
domega =  3/4*n*J2*(R/p)^2*(4-5*sin(i)^2);

%% apply changes per orbit linearly over time (have no clue whether this approach is ok...)
Omega = Omega + dOmega * (t-t(1));
omega = omega + domega * (t-t(1));

% a
% e
% i
% omega
% Omega
% M0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% apply changes to Kepler orbit due to the flattening of the Earth - end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% vector pointing towards perigee
P = [ cos(omega).*cos(Omega)-sin(omega).*sin(Omega)*cos(i)
      cos(omega).*sin(Omega)+sin(omega).*cos(Omega)*cos(i)
      sin(omega)*sin(i)];
  
%% vector in orbital plane corresponding to true anomaly = 90 degrees
Q = [-sin(omega).*cos(Omega)-cos(omega).*sin(Omega)*cos(i)
     -sin(omega).*sin(Omega)+cos(omega).*cos(Omega)*cos(i)
      cos(omega)*sin(i)];

% P(:,1)
% Q(:,1)

%% mean anomaly in rad
M = M0 + (t-t0)*n;
M = M - 2*pi*floor(M/(2*pi)); % restrict to interval [0,2*pi]

% M(1)

%% compute eccentric anomaly
if e > 0.8
    E = pi;
else
    E = M;
end
% E
max_abs_dE = 1; % for entering the while loop
while max_abs_dE > 1e-14
%     max_abs_dE
    E_old  = E; % remember the old E
    E = E - (E-e*sin(E)-M)./(1-e*cos(E)); % compute the new E
    max_abs_dE = max(abs(E-E_old));
end
% E(1)

%% inertial coordinates
if size(P,1)*size(P,2) == 3
    x = a*(cos(E)-e)*P(1) + a*sqrt(1-e.^2)*sin(E)*Q(1);
    y = a*(cos(E)-e)*P(2) + a*sqrt(1-e.^2)*sin(E)*Q(2);
    z = a*(cos(E)-e)*P(3) + a*sqrt(1-e.^2)*sin(E)*Q(3);
else
    x = a*(cos(E)-e).*P(1,:) + a*sqrt(1-e.^2)*sin(E).*Q(1,:);
    y = a*(cos(E)-e).*P(2,:) + a*sqrt(1-e.^2)*sin(E).*Q(2,:);
    z = a*(cos(E)-e).*P(3,:) + a*sqrt(1-e.^2)*sin(E).*Q(3,:);
end


%% inertial velocities
r = sqrt(x.^2+y.^2+z.^2);
if size(P,1)*size(P,2) == 3
    vx = sqrt(GM*a)./r.*(-sin(E)*P(1) + sqrt(1-e^2)*cos(E)*Q(1));
    vy = sqrt(GM*a)./r.*(-sin(E)*P(2) + sqrt(1-e^2)*cos(E)*Q(2));
    vz = sqrt(GM*a)./r.*(-sin(E)*P(3) + sqrt(1-e^2)*cos(E)*Q(3));
else
    vx = sqrt(GM*a)./r.*(-sin(E).*P(1,:) + sqrt(1-e^2)*cos(E).*Q(1,:));
    vy = sqrt(GM*a)./r.*(-sin(E).*P(2,:) + sqrt(1-e^2)*cos(E).*Q(2,:));
    vz = sqrt(GM*a)./r.*(-sin(E).*P(3,:) + sqrt(1-e^2)*cos(E).*Q(3,:));
end


