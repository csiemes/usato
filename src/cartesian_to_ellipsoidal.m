%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [L,B,h,N] = cartesian_to_ellipsoidal(x,y,z,varargin)
% [L,B,h,N] = cartesian_to_ellipsoidal(x,y,z) using a,f of GRS-80
% [L,B,h,N] = cartesian_to_ellipsoidal(x,y,z,a,f)
%
%     Ellipsoid: Airy 1830[6]
%     Semi-major axis a: 6377563.396 m
%     Semi-minor axis b: 6356256.909 m
%     Flattening (derived constant): 1/299.3249646 


% WGS84
% a = 6378137;
% f = 1 / 298.257223563;

if nargin == 3
    % GRS-80
    a = 6378137; % m
    f = 1/298.257222101;
elseif nargin == 5
    a = varargin{1};
    f = varargin{2};
else
    error('Incorrect number of input arguments.')
end

p = sqrt(x.^2+y.^2);
e = sqrt(1-(1-f)^2);

L = atan2(y,x);

B_old = atan2(z,p);
N = a./sqrt(1-e^2*(1-cos(B_old).^2));

B = atan2((z+N.*(e^2*sin(B_old))),p);
while max(abs(B-B_old)) > 1e-14
    B_old = B;
    N = a./sqrt(1-e^2*(1-cos(B_old).^2));
    B = atan2((z+N.*(e^2*sin(B_old))),p);
end

c = abs(B)<45*pi/180;
h = zeros(size(c));
h(c) = p(c)./cos(B(c))-N(c);
h(~c) = z(~c)./sin(B(~c))-N(~c)*(1-e^2);
    
