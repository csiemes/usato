%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [era] = earth_rotation_angle(t)
% [era] = earth_rotation_angle(t)
%
% era ... Earth rotation angle in radians
% t ... UT1 time in Julian days
%
% see: IERS 2010 conventions, page 52

Tu = t - 2451545.0;

% the next line of code should be equivalent to:
% era = 2*pi*(0.7790572732640 + 1.00273781191135448 * Tu);
era = 2*pi*(0.7790572732640 + mod(Tu,1) + mod(0.00273781191135448 * Tu,1));

era = mod(era,2*pi);