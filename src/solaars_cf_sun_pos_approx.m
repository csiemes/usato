%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [shadow_function] = solaars_cf_sun_pos_approx(t_gps_sec,r_sat_eci)
% [shadow_function] = solaars_cf_sun_pos_approx(t_gps_sec,r_sat_eci)
%
% t_gps_sec ......... time in GPS seconds (N x 1 matrix), unit: s
% r_sat_eci ......... satellite positions (N x 3 matrix), ECI reference frame (Earth-centered, inertial), unit: m
% shadow_function ... shadow function (N x 1 matrix), 1 = sun, 0 = shadow
% 
% This function implements the SOLAARS-CF shadow function described in:
%
% Robert Robertson (2015) Highly physical solar radiation pressure
% modelling during penumbra transitions. Dissertation, Virginia Polytechnic
% Institute and State University, US.
%
% The SOLAARS-CF shadow function makes the following simplifications:
% 
% 1. USS atmosphere density and mixing ratio profiles
% 2. Medium aerosol extinction coefficient profile (see Table 3.1 of reference)
% 3. Cloud top height (hCT) of 5892.5 m, the mean of zonal averages from Lelli et al. (2011)
% 4. Constant satellite optical properties as a function of wavelength
% 5. Axial tilt of Earth with respect to the ecliptic is negligible in oblateness modeling

% % direction towards Sun in ECI reference frame
e_sun_eci = low_precision_lunisolar_coordinates_irf(t_gps_sec);
if numel(t_gps_sec)==1
    e_sun_eci = e_sun_eci';
end
e_sun_eci = e_sun_eci./repmat(sqrt(sum(e_sun_eci.^2,2)),1,3);

% unit normal on the ecliptic (ecliptic = mean plane of the apparent path
% in the Earth's sky that the Sun follows over the course of one year)
dt_quarter_year = 365.25/4*86400; % = 1/4 of a year in days
if numel(t_gps_sec)==1
    e_ecliptic = cross(low_precision_lunisolar_coordinates_irf(t_gps_sec + dt_quarter_year)',e_sun_eci);
else
    e_ecliptic = cross(low_precision_lunisolar_coordinates_irf(t_gps_sec + dt_quarter_year),e_sun_eci);
end
e_ecliptic = e_ecliptic./repmat(sqrt(sum(e_ecliptic.^2,2)),1,3);

% projection of the satellite position onto negative Sun direction
rr = - sum(r_sat_eci.*e_sun_eci,2);

% component of satellite position parallel to unit normal on the ecliptic
re_parallel = abs(sum(e_ecliptic.*r_sat_eci,2)); 

% component of satellite position perpendicular to ecliptic vector
% (not the equations from Robertson, but the result is exactly the same:
%  rE_orthogonal = length of projection of vector "rE" along unit vector
%  "cross(R,E)" where "rE = r - dot(r,R)*R", so we get
%  rE_orthogonal = dot(cross(R,E),rE) noting that dot(cross(R,E),R) = 0)
re_perpendicular = abs(sum(cross(e_sun_eci,e_ecliptic).*r_sat_eci,2));

% oblate Earth scaling factor = (Earth's equatorial radius / Earth's polar radius) - WGS84
so = 6378137.0 / 6356752.3142; 

% projection of the satellite position onto plane normal to sun direction,
% adjusted for Earth's oblateness
re = sqrt(re_perpendicular.^2 + (so*re_parallel).^2);

% change unit from meters to 1e6 meters
re = re / 1e6;
rr = rr / 1e6;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% this code section replicates the calculation in GROOPS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% R = e_sun_eci; % const Vector3d R  = normalize(posSun);
% r = r_sat_eci; % Vector3d r = position;
% rr = -1e-6 * sum(r.*R,2); % const Double rR = -1e-06 * inner(position, R); // in 1e6 meters
% r(:,3) = r(:,3) * 6378137/6356752; % r.z() *= 6378137./6356752.; // account for flattening
% re =  1e-6 * sqrt(sum((r - R.*repmat(sum(r.*R,2),1,3)).^2,2)); % const Double rE = 1e-6 * (r-R*inner(r, R)).r();
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% a-coefficients (Robert Robertson, 2015, Table 3.2 on page 73)
a1 = 0.1715 * exp(-0.1423*rr) + 0.01061 * exp(-0.01443*rr);
a2 = 0.008162 * rr + 0.3401;
a3 = 260.9 * exp(-0.4661*rr) + 27.81 * exp(-0.009437*rr);
a4 = -0.006119 * rr.^1.176 + 6.385;
a5 = 87.56 * exp(-0.09188*rr) + 19.3 * exp(-0.01089*rr);
a6 = 0.002047 * rr + 6.409;
a7 = 61.98 * exp(-0.1629*rr) + 27.87 * exp(-0.02217*rr);
a8 = 6.413 * exp(-0.0002593*rr) - 0.01479 * exp(-0.1318*rr);

% using a-coefficients and adjusted re to calculate attenuation factor:
shadow_function = (1 + a1 + a2 + a1.*tanh(a3.*(re - a4)) + a2 .* tanh(a5.*(re - a6)) + tanh(a7.*(re - a8)) ) ./ (2 + 2*a1 + 2*a2);

% shadow function of full sunlight is one by definition
shadow_function(rr < 0) = 1;

% NRTDM does this (no clue why; to do: check why)
shadow_function(shadow_function < 1e-12) = 0;


