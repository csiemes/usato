%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x,y,z] = ellipsoidal_to_cartesian(L,B,h,varargin)
% [x,y,z] = ellipsoidal_to_cartesian(L,B,h)
% [x,y,z] = ellipsoidal_to_cartesian(L,B,h,a,f)
%
% L, B in radians
% h in meters

if nargin == 3
    % GRS-80
    a = 6378137; % m
    f = 1/298.257222101;
elseif nargin == 5
    a = varargin{1};
    f = varargin{2};
else
    error('Incorrect number of input arguments.')
end

e2 = 2*f-f^2;

N = a ./ sqrt(1 - e2 * sin(B).^2);

x = (N+h).*cos(L).*cos(B);
y = (N+h).*sin(L).*cos(B);
z = ((1-e2)*N+h).*sin(B);
