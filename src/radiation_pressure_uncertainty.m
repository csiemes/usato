%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Ca,Cy,H] = radiation_pressure_uncertainty(dt,n,A,cav,cdv,csv,cai,cdi,csi,id,T,Tb,k,C,Cb,Pb,m,Ps,Pa,Pi,es,ea,ei,Cx1,Cx2)
% [Ca,Cy,H] = radiation_pressure_uncertainty(dt,n,A,cav,cdv,csv,cai,cdi,csi,id,T,Tb,k,C,Cb,Pb,m,Ps,Pa,Pi,es,ea,ei,Cx1,Cx2)
% 
% P .... number of panels
% Sa ... number of radiation sources due to albedo
% Si ... number of radiation sources due to Earth infrared radiation 
%
% dt ... time step from this epoch to the next epoch, unit = s, 1 x 1
% n .... panel normals (outward), P x 1
% A .... panel areas, unit = m2, P x 1
% cav .. absorption coefficients for visible light, P x 1
% cdv .. diffuse reflection coefficients for visible light, P x 1
% csv .. specular reflection coefficients for visible light, P x 1
% cai .. absorption coefficients for infrared light, P x 1
% cdi .. diffuse reflection coefficients for infrared light, P x 1
% csi .. specular reflection coefficients for infrared light, P x 1
% id ... ID of material (integer), P x 1
% T .... panel temperatures at this epoch, unit = K, P x 1
% Tb ... satellite body temperature at this epoch, unit = K, 1 x 1
% k .... heat conductivity between panels and satellite body, unit = W/K, P x 1
% C .... heat capacity of the panels, unit = J/K, P x 1
% Cb ... heat capacity of the satilte body, unit = J/K, 1 x 1
% Pb ... heat generation of the satellite body, unit = W, 1 x 1
% m .... satellite mass, unit = kg, 1 x 1
% Ps ... heat flux from the sun, unit = W/m2, 1 x 1
% Pa ... heat flux from albedo, unit = W/m2, Sa x 1
% Pi ... heat flux from Earth infrared radiation, unit = W/m2, Si x 1
% es ... direction from the satellite to the sun (unit vector), satellite frame
% ea ... direction from the satellite to the albedo surface element (unit vectors), satellite frame
% ei ... direction from the satellite to the infrared surface element (unit vectors), satellite frame
% Cx1 .. covariance matrix of of input parameters (part 1, full matrix)
% Cx2 .. covariance matrix of of input parameters (part 2, diagonal matrix)
% Ca ... covariance matrix of of output parameters: radiation pressure acceleration
% Cy ... covariance matrix of of output parameters: parameters x1 of the next step (Cx1 = Cy)
% H .... partials of radiation pressure acceleration with respect to the input parameters
%
% input vector is sorted as follows:
% x = [T(n) Tb(n) cav cdv csv cai cdi csi A k m Cb Pb Pv Pi]
%
% output vector is sorted as follows:
% y = [a T(n+1) Tb(n+1) cav cdv csv cai cdi csi A k m Cb Pb]
%
% same sorting applies to covariance matrix Cy


% material IDs (output is sorted)
idu = unique(id);

% number of materials
I = numel(idu);

% number of panels
P = numel(A);

% number of radiation sources
Sv = numel(Pa) + numel(Ps);
Si = numel(Pi);

% Stefan Botzmann constant, unit = W/m2/K4
sigma = 5.670374419e-8;

% speed of light, unit = m/s
c = 299792458;


%-----------------------------------
% preparations
%-----------------------------------

% combine sun and albedo for simple access
Pv = [Ps; Pa];
evx = [es(1); ea(:,1)];
evy = [es(2); ea(:,2)];
evz = [es(3); ea(:,3)];

% simple access
eix = ei(:,1);
eiy = ei(:,2);
eiz = ei(:,3);

% simple access
nx = n(:,1);
ny = n(:,2);
nz = n(:,3);

% calculate cosine of angles between the panels normals and the direction
% to the radiation source, P x S
cv = n*[evx evy evz]';
ci = n*[eix eiy eiz]';

% panels do not absorb heat when the source illuminates the  backside
cv(cv<0) = 0;
ci(ci<0) = 0;

%-----------------------------------
% partials
%-----------------------------------

ax_cav_tmp = -A .* (cv*(Pv.*evx))/m/c;
ax_cdv_tmp = -A .* (cv*(Pv.*evx))/m/c - 2/3*A.*nx.*(cv*Pv)/m/c;
ax_csv_tmp = -2*A.*nx.*(cv.^2*Pv)/m/c;
ax_cai_tmp = -A .* (ci*(Pi.*eix))/m/c - 2/3*sigma/m/c*A.*T.^4.*nx;
ax_cdi_tmp = -A .* (ci*(Pi.*eix))/m/c - 2/3*A.*nx.*(ci*Pi)/m/c;
ax_csi_tmp = -2*A.*nx.*(ci.^2*Pi)/m/c;
ax_A = -2/3*sigma/m/c*cai.*T.^4.*nx ...
       - cav.*(cv*(Pv.*evx))/m/c ...
       - 2*csv.*nx.*(cv.^2*Pv)/m/c ...
       - cdv.*(cv*(Pv.*evx))/m/c ...
       - 2/3*cdv.*nx.*(cv*Pv)/m/c ...
       - cai.*(ci*(Pi.*eix))/m/c ...
       - 2*csi.*nx.*(ci.^2*Pi)/m/c ...
       - cdi.*(ci*(Pi.*eix))/m/c ...
       - 2/3*cdi.*nx.*(ci*Pi)/m/c;
ax_T = -8/3*sigma*A.*cai.*T.^3.*nx/m/c;
ax_k = zeros(P,1);
ax_C = zeros(P,1);
ax_m = +2/3*sigma/c/m^2 * (A.*cai)'*(T.^4.*nx) ...
       + (A.*cav)'*cv*(Pv.*evx)/c/m^2 ...
       + 2*(A.*csv.*nx)'*cv.^2*Pv/c/m^2 ...
       + (A.*cdv)'*cv*(Pv.*evx)/c/m^2 ...
       + 2/3*(A.*cdv.*nx)'*cv*Pv/c/m^2 ...
       + (A.*cai)'*ci*(Pi.*eix)/c/m^2 ...
       + 2*(A.*csi.*nx)'*ci.^2*Pi/c/m^2 ...
       + (A.*cdi)'*ci*(Pi.*eix)/c/m^2 ...
       + 2/3*(A.*cdi.*nx)'*ci*Pi/c/m^2;
ax_Tb = 0;
ax_Cb = 0;
ax_Pb = 0;
ax_Pv = - evx.*(cv'*(A.*cav))/m/c ...
        - 2*cv'.^2*(A.*csv.*nx)/m/c ...
        - evx.*(cv'*(A.*cdv))/m/c ...
        - 2/3*cv'*(A.*cdv.*nx)/m/c;
ax_Pi = - eix.*(ci'*(A.*cai))/m/c ...
        - 2*ci'.^2*(A.*csi.*nx)/m/c ...
        - eix.*(ci'*(A.*cdi))/m/c ...
        - 2/3*ci'*(A.*cdi.*nx)/m/c;

ay_cav_tmp = -A .* (cv*(Pv.*evy))/m/c;
ay_cdv_tmp = -A .* (cv*(Pv.*evy))/m/c - 2/3*A.*ny.*(cv*Pv)/m/c;
ay_csv_tmp = -2*A.*ny.*(cv.^2*Pv)/m/c;
ay_cai_tmp = -A .* (ci*(Pi.*eiy))/m/c - 2/3*sigma/m/c*A.*T.^4.*ny;
ay_cdi_tmp = -A .* (ci*(Pi.*eiy))/m/c - 2/3*A.*ny.*(ci*Pi)/m/c;
ay_csi_tmp = -2*A.*ny.*(ci.^2*Pi)/m/c;
ay_A = -2/3*sigma/m/c*cai.*T.^4.*ny ...
       - cav.*(cv*(Pv.*evy))/m/c ...
       - 2*csv.*ny.*(cv.^2*Pv)/m/c ...
       - cdv.*(cv*(Pv.*evy))/m/c ...
       - 2/3*cdv.*ny.*(cv*Pv)/m/c ...
       - cai.*(ci*(Pi.*eiy))/m/c ...
       - 2*csi.*ny.*(ci.^2*Pi)/m/c ...
       - cdi.*(ci*(Pi.*eiy))/m/c ...
       - 2/3*cdi.*ny.*(ci*Pi)/m/c;
ay_T = -8/3*sigma*A.*cai.*T.^3.*ny/m/c;
ay_k = zeros(P,1);
ay_C = zeros(P,1);
ay_m = +2/3*sigma/c/m^2 * (A.*cai)'*(T.^4.*ny) ...
       + (A.*cav)'*cv*(Pv.*evy)/c/m^2 ...
       + 2*(A.*csv.*ny)'*cv.^2*Pv/c/m^2 ...
       + (A.*cdv)'*cv*(Pv.*evy)/c/m^2 ...
       + 2/3*(A.*cdv.*ny)'*cv*Pv/c/m^2 ...
       + (A.*cai)'*ci*(Pi.*eiy)/c/m^2 ...
       + 2*(A.*csi.*ny)'*ci.^2*Pi/c/m^2 ...
       + (A.*cdi)'*ci*(Pi.*eiy)/c/m^2 ...
       + 2/3*(A.*cdi.*ny)'*ci*Pi/c/m^2;
ay_Tb = 0;
ay_Cb = 0;
ay_Pb = 0;
ay_Pv = - evy.*(cv'*(A.*cav))/m/c ...
        - 2*cv'.^2*(A.*csv.*ny)/m/c ...
        - evy.*(cv'*(A.*cdv))/m/c ...
        - 2/3*cv'*(A.*cdv.*ny)/m/c;
ay_Pi = - eiy.*(ci'*(A.*cai))/m/c ...
        - 2*ci'.^2*(A.*csi.*ny)/m/c ...
        - eiy.*(ci'*(A.*cdi))/m/c ...
        - 2/3*ci'*(A.*cdi.*ny)/m/c;

az_cav_tmp = -A .* (cv*(Pv.*evz))/m/c;
az_cdv_tmp = -A .* (cv*(Pv.*evz))/m/c - 2/3*A.*nz.*(cv*Pv)/m/c;
az_csv_tmp = -2*A.*nz.*(cv.^2*Pv)/m/c;
az_cai_tmp = -A .* (ci*(Pi.*eiz))/m/c - 2/3*sigma/m/c*A.*T.^4.*nz;
az_cdi_tmp = -A .* (ci*(Pi.*eiz))/m/c - 2/3*A.*nz.*(ci*Pi)/m/c;
az_csi_tmp = -2*A.*nz.*(ci.^2*Pi)/m/c;
az_A = -2/3*sigma/m/c*cai.*T.^4.*nz ...
       - cav.*(cv*(Pv.*evz))/m/c ...
       - 2*csv.*nz.*(cv.^2*Pv)/m/c ...
       - cdv.*(cv*(Pv.*evz))/m/c ...
       - 2/3*cdv.*nz.*(cv*Pv)/m/c ...
       - cai.*(ci*(Pi.*eiz))/m/c ...
       - 2*csi.*nz.*(ci.^2*Pi)/m/c ...
       - cdi.*(ci*(Pi.*eiz))/m/c ...
       - 2/3*cdi.*nz.*(ci*Pi)/m/c;
az_T = -8/3*sigma*A.*cai.*T.^3.*nz/m/c;
az_k = zeros(P,1);
az_C = zeros(P,1);
az_m = +2/3*sigma/c/m^2 * (A.*cai)'*(T.^4.*nz) ...
       + (A.*cav)'*cv*(Pv.*evz)/c/m^2 ...
       + 2*(A.*csv.*nz)'*cv.^2*Pv/c/m^2 ...
       + (A.*cdv)'*cv*(Pv.*evz)/c/m^2 ...
       + 2/3*(A.*cdv.*nz)'*cv*Pv/c/m^2 ...
       + (A.*cai)'*ci*(Pi.*eiz)/c/m^2 ...
       + 2*(A.*csi.*nz)'*ci.^2*Pi/c/m^2 ...
       + (A.*cdi)'*ci*(Pi.*eiz)/c/m^2 ...
       + 2/3*(A.*cdi.*nz)'*ci*Pi/c/m^2;
az_Tb = 0;
az_Cb = 0;
az_Pb = 0;
az_Pv = - evz.*(cv'*(A.*cav))/m/c ...
        - 2*cv'.^2*(A.*csv.*nz)/m/c ...
        - evz.*(cv'*(A.*cdv))/m/c ...
        - 2/3*cv'*(A.*cdv.*nz)/m/c;
az_Pi = - eiz.*(ci'*(A.*cai))/m/c ...
        - 2*ci'.^2*(A.*csi.*nz)/m/c ...
        - eiz.*(ci'*(A.*cdi))/m/c ...
        - 2/3*ci'*(A.*cdi.*nz)/m/c;

T_cav_tmp = diag(dt*A./C.*(cv*Pv));
T_cdv_tmp = zeros(P,P);
T_csv_tmp = zeros(P,P);
T_cai_tmp = diag(dt*A./C.*(ci*Pi) - dt*sigma*A./C.*T.^4);
T_cdi_tmp = zeros(P,P);
T_csi_tmp = zeros(P,P);
T_A = diag(dt*cav./C.*(cv*Pv) - dt*sigma*cai./C.*T.^4 + dt*cai./C.*(ci*Pi));
T_T = diag(1 - 4*dt*sigma*A./C.*cai.*T.^3 - dt*k./C); 
T_k = diag(-dt*(T-Tb)./C);
T_C =   diag(-dt*A./C.^2.*cav.*(cv*Pv)) ...
      + diag(-dt*A./C.^2.*cai.*(ci*Pi)) ...
      + diag(dt*k./C.^2.*(T-Tb)) ...
      + diag(dt*sigma*A./C.^2.*cai.*T.^4);
T_m = zeros(1,P);
T_Tb = dt*(k./C)';
T_Cb = zeros(1,P);
T_Pb = zeros(1,P);
T_Pv = dt*cv'.*repmat((A./C.*cav)',Sv,1);
T_Pi = dt*ci'.*repmat((A./C.*cai)',Si,1);

Tb_cav_tmp = zeros(P,1);
Tb_cdv_tmp = zeros(P,1);
Tb_csv_tmp = zeros(P,1);
Tb_cai_tmp = zeros(P,1);
Tb_cdi_tmp = zeros(P,1);
Tb_csi_tmp = zeros(P,1);
Tb_A = zeros(P,1);
Tb_T = dt/Cb*k;
Tb_k = dt/Cb*(T-Tb);
Tb_C = zeros(P,1);
Tb_m = 0;
Tb_Tb = 1-dt/Cb*sum(k);
Tb_Cb = -dt*Pb/Cb^2 - dt/Cb^2*sum(k.*(T-Tb));
Tb_Pb = dt/Cb;
Tb_Pv = zeros(Sv,1);
Tb_Pi = zeros(Si,1);


%-----------------------------------
% sum columns with same materials
%-----------------------------------

ax_cav = zeros(I,1);
ax_cdv = zeros(I,1);
ax_csv = zeros(I,1);
ax_cai = zeros(I,1);
ax_cdi = zeros(I,1);
ax_csi = zeros(I,1);

ay_cav = zeros(I,1);
ay_cdv = zeros(I,1);
ay_csv = zeros(I,1);
ay_cai = zeros(I,1);
ay_cdi = zeros(I,1);
ay_csi = zeros(I,1);

az_cav = zeros(I,1);
az_cdv = zeros(I,1);
az_csv = zeros(I,1);
az_cai = zeros(I,1);
az_cdi = zeros(I,1);
az_csi = zeros(I,1);

T_cav = zeros(I,P);
T_cdv = zeros(I,P);
T_csv = zeros(I,P);
T_cai = zeros(I,P);
T_cdi = zeros(I,P);
T_csi = zeros(I,P);

Tb_cav = zeros(I,1);
Tb_cdv = zeros(I,1);
Tb_csv = zeros(I,1);
Tb_cai = zeros(I,1);
Tb_cdi = zeros(I,1);
Tb_csi = zeros(I,1);


for i = 1:I
    ax_cav(i) = sum(ax_cav_tmp(id==idu(i)));
    ax_cdv(i) = sum(ax_cdv_tmp(id==idu(i)));
    ax_csv(i) = sum(ax_csv_tmp(id==idu(i)));
    ax_cai(i) = sum(ax_cai_tmp(id==idu(i)));
    ax_cdi(i) = sum(ax_cdi_tmp(id==idu(i)));
    ax_csi(i) = sum(ax_csi_tmp(id==idu(i)));

    ay_cav(i) = sum(ay_cav_tmp(id==idu(i)));
    ay_cdv(i) = sum(ay_cdv_tmp(id==idu(i)));
    ay_csv(i) = sum(ay_csv_tmp(id==idu(i)));
    ay_cai(i) = sum(ay_cai_tmp(id==idu(i)));
    ay_cdi(i) = sum(ay_cdi_tmp(id==idu(i)));
    ay_csi(i) = sum(ay_csi_tmp(id==idu(i)));

    az_cav(i) = sum(az_cav_tmp(id==idu(i)));
    az_cdv(i) = sum(az_cdv_tmp(id==idu(i)));
    az_csv(i) = sum(az_csv_tmp(id==idu(i)));
    az_cai(i) = sum(az_cai_tmp(id==idu(i)));
    az_cdi(i) = sum(az_cdi_tmp(id==idu(i)));
    az_csi(i) = sum(az_csi_tmp(id==idu(i)));

    T_cav(i,:) = sum(T_cav_tmp(id==idu(i),:),1);
    T_cdv(i,:) = sum(T_cdv_tmp(id==idu(i),:),1);
    T_csv(i,:) = sum(T_csv_tmp(id==idu(i),:),1);
    T_cai(i,:) = sum(T_cai_tmp(id==idu(i),:),1);
    T_cdi(i,:) = sum(T_cdi_tmp(id==idu(i),:),1);
    T_csi(i,:) = sum(T_csi_tmp(id==idu(i),:),1);

    Tb_cav(i) = sum(Tb_cav_tmp(id==idu(i)));
    Tb_cdv(i) = sum(Tb_cdv_tmp(id==idu(i)));
    Tb_csv(i) = sum(Tb_csv_tmp(id==idu(i)));
    Tb_cai(i) = sum(Tb_cai_tmp(id==idu(i)));
    Tb_cdi(i) = sum(Tb_cdi_tmp(id==idu(i)));
    Tb_csi(i) = sum(Tb_csi_tmp(id==idu(i)));
end


%-----------------------------------
% covariance propagation
%-----------------------------------

% input vector
%    x = [T(n) Tb(n) cav cdv csv cai cdi csi A k C m Cb Pb Pv Pi]
% 
% split into
%    xa = [T(n) Tb(n) cav cdv csv cai cdi csi A k C m Cb P]
%    xb = [Pv Pi]
%
%
% output vector
%    y = [a T(n+1) Tb(n+1) cav cdv csv cai cdi csi A k C m Cb Pb]
%
% split into
%    ya = [a T(n+1) Tb(n+1)]
%    yb = [cav cdv csv cai cdi csi A k C m Cb Pb]

% dimensions
Nx1 = P + 1;         % x1 = [T(n) Tb(n)]
Nx2 = 6*I + 3*P + 3; % x2 = [cav cdv csv cai cdi csi A k C m Cb Pb]
Nx3 = Sv + Si;       % x3 = [Pv Pi]

Ny1 = 3;             % y1 = [a]
Ny2 = P + 1;         % y2 = [T(n+1) Tb(n+1)];
Ny3 = 6*I + 3*P + 3; % y3 = [cav cdv csv cai cdi csi A k C m Cb Pb]

Nxa = Nx1 + Nx2;
Nxb = Nx3;

Nya = Ny1 + Ny2;
Nyb = Ny3;


% assemble matrix of partials
Haa = [ax_T' ax_Tb' ax_cav' ax_cdv' ax_csv' ax_cai' ax_cdi' ax_csi' ax_A' ax_k' ax_C' ax_m' ax_Cb' ax_Pb' 
       ay_T' ay_Tb' ay_cav' ay_cdv' ay_csv' ay_cai' ay_cdi' ay_csi' ay_A' ay_k' ay_C' ay_m' ay_Cb' ay_Pb' 
       az_T' az_Tb' az_cav' az_cdv' az_csv' az_cai' az_cdi' az_csi' az_A' az_k' az_C' az_m' az_Cb' az_Pb' 
       T_T'  T_Tb'  T_cav'  T_cdv'  T_csv'  T_cai'  T_cdi'  T_csi'  T_A'  T_k'  T_C'  T_m'  T_Cb'  T_Pb'  
       Tb_T' Tb_Tb' Tb_cav' Tb_cdv' Tb_csv' Tb_cai' Tb_cdi' Tb_csi' Tb_A' Tb_k' Tb_C' Tb_m' Tb_Cb' Tb_Pb'];

Hab = [ax_Pv' ax_Pi'
       ay_Pv' ay_Pi'
       az_Pv' az_Pi'
       T_Pv'  T_Pi'
       Tb_Pv' Tb_Pi'];

% the first three lines are partials for the radiation pressure acceleration
H = [Haa(1:3,:) Hab(1:3,:)];

Hba = [zeros(Ny3,Nx1) eye(Ny3)];

% Hbb = zeros ... no need to define variable


% exploit that Cx2 is a diagonal matrix
Hab = Hab .* repmat(sqrt(Cx2'),Nya,1);

% calculate parts of output covariance matrix
Cy_aa = Haa*Cx1*Haa' + Hab*Hab';
Cy_ab = Haa*Cx1*Hba';
Cy_ba = Cy_ab';
Cy_bb = Hba*Cx1*Hba';

% assemble 
Cy = [Cy_aa Cy_ab
      Cy_ba Cy_bb];

% cut out the parts that we need
Ca = Cy(1:3,1:3);
Cy = Cy(4:end,4:end);

