%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [d] = read_nrtdm(fn_txt)

fid = fopen(fn_txt);

% search how many header lines the file has
NH = 0;
line = fgetl(fid);
while line(1)=='#'
    NH = NH + 1;
    
    % check if the line contains the format string
    if contains(line,'format string','IgnoreCase',true)
        nc = find(line==':',1);
        format_string = line(nc+1:end);
    end
    
    line = fgetl(fid);
end

% line is now either empty or contains the first data line with the time system
if ~isempty(line)
    if contains(line,'GPS')
        ts = 'GPS';
    elseif contains(line,'UTC')
        ts = 'UTC';
    else
        error('Search for time system failed');
    end

    % go back to the beginning and then skip header
    frewind(fid);
    for nh = 1:NH
        fgetl(fid);
    end

    % the format should always start with the date string and the time system
    fmt = ['%d-%d-%d %d:%d:%f ' ts];
    
    % number of columns to read
    NC = 6;
    
    % parse format string
    na = find(format_string=='(');
    nb = find(format_string==')');
    format_string = [format_string(na+1:nb-1) ','];
    nc = find(format_string==',');
    for k = 1:length(nc)-1
        fs = format_string(nc(k)+1:nc(k+1)-1);
        if contains(fs,'f','IgnoreCase',true)
            fmt = append(fmt,' %f');
            NC = NC + 1;
        elseif contains(fs,'e','IgnoreCase',true)
            fmt = append(fmt,' %f');
            NC = NC + 1;
        elseif contains(fs,'i','IgnoreCase',true)
            fmt = append(fmt,' %d');
            NC = NC + 1;
        end
    end
    fmt = append(fmt,'\n');
    
    % read and reshape data
    d = fscanf(fid,fmt);
    d = reshape(d,NC,numel(d)/NC)';
end

fclose(fid);
