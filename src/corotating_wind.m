%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [w] = corotating_wind(r)
% [w] = corotating_wind(r)
%
% Velocity of wind particles in celestial, Earth-centered reference frame.
% (In Earth-fixed reference frame their velocity is zero.)
%
% r   position of satellite in the celestial, Earth-centered reference
%     frame in meters
% w   wind of co-rotating atmosphere in meters/second

% this value is consistent with "earth_rotation_angle.m"

w = [0,0,2*pi*1.00273781191135448/86400];
w = cross(repmat(w,size(r,1),1),r);

