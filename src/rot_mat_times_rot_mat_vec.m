%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [C] = rot_mat_times_rot_mat_vec(A,B)
% C = A*B where A,B,C are rotation matrices stored columnwise in each row, 
% e.g. size(A) = N x 9 --> A(1,:) = rot mat columnwise of first epoch

C = zeros(size(A));

C(:,1) = A(:,1).*B(:,1) + A(:,4).*B(:,2) + A(:,7).*B(:,3);
C(:,2) = A(:,2).*B(:,1) + A(:,5).*B(:,2) + A(:,8).*B(:,3);
C(:,3) = A(:,3).*B(:,1) + A(:,6).*B(:,2) + A(:,9).*B(:,3);

C(:,4) = A(:,1).*B(:,4) + A(:,4).*B(:,5) + A(:,7).*B(:,6);
C(:,5) = A(:,2).*B(:,4) + A(:,5).*B(:,5) + A(:,8).*B(:,6);
C(:,6) = A(:,3).*B(:,4) + A(:,6).*B(:,5) + A(:,9).*B(:,6);

C(:,7) = A(:,1).*B(:,7) + A(:,4).*B(:,8) + A(:,7).*B(:,9);
C(:,8) = A(:,2).*B(:,7) + A(:,5).*B(:,8) + A(:,8).*B(:,9);
C(:,9) = A(:,3).*B(:,7) + A(:,6).*B(:,8) + A(:,9).*B(:,9);

