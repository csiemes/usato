%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = fix_figure()

set(gca,'XColor','k')
set(gca,'YColor','k')
set(gca,'ZColor','k')
set(gcf,'Color','w')
set(gca,'Color','w')
set(groot,'defaultTextColor','k')
gcl = get(gca,'Legend');
if ~isempty(gcl)
    set(gcl,'Color','w')
    set(gcl,'TextColor','k')
end
ht = get(gca,'Title');
if ~isempty(ht)
    set(ht,'Color','k')
end
colororder([     0    0.4470    0.7410
            0.8500    0.3250    0.0980
            0.9290    0.6940    0.1250
            0.4940    0.1840    0.5560
            0.4660    0.6740    0.1880
            0.3010    0.7450    0.9330
            0.6350    0.0780    0.1840])

hc = findobj(gcf,'Type','ColorBar');
for k = 1:numel(hc)
    set(hc,'Color','k')
end
