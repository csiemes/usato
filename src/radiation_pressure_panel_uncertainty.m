%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Sa] = radiation_pressure_panel_uncertainty(es,np,Ap,ca,cd,cs,mp,ca_std,cd_std,cs_std,Phi,ms)
% [Sa] = radiation_pressure_panel(es,np,Ap,ca,cd,cs)
%
% np ... panel normals (unit vector pointing outward), P x 3
% es ... unit vector toward radiation source, S x 3
% Ap ... panel areas, P x 1
% ca ... absorption coefficient, P x 1
% cd ... diffuse reflection coefficient, P x 1
% cs ... specular reflection coefficient, P x 1
% mp ... material index of panels, P x 1
% ca_std ... standard deviation of absorption coefficient, M x 1
% cd_std ... standard deviation of diffuse reflection coefficient, M x 1
% cs_std ... standard deviation of specular reflection coefficient, M x 1
% Phi ... Radiation flux for each radiation source, S x 1
% ms ... mass of satellite, 1 x 1
%
% P ... number of panels
% M ... number of materials
% S ... number of radiation sources
%
% Sa ... covariance matrix of the radiation pressure acceleration, 3x3

S = size(es,1);
P = size(Ap,1);

% material IDs
mu = unique(mp);

% number of materials
M = numel(mu);

% cosine of angle between surface normal and vector to radiation source
gamma = es*np'; % S x P

% force to zero when the outside of the panel is not illuminated
gamma(gamma < 0) = 0; % panel not illuminated


dCax = gamma.*repmat(Ap',S,1).*repmat(es(:,1),1,P); % SxP * repmat(1xP,S,1) .* SxP = SxP
dCay = gamma.*repmat(Ap',S,1).*repmat(es(:,2),1,P);
dCaz = gamma.*repmat(Ap',S,1).*repmat(es(:,3),1,P);

dCsx = -2*gamma.^2.*repmat(Ap',S,1).*repmat(np(:,1)',S,1); % SxP .* repmat(1xP,S,1) .* repmat(1xP,S,1)
dCsy = -2*gamma.^2.*repmat(Ap',S,1).*repmat(np(:,2)',S,1);
dCsz = -2*gamma.^2.*repmat(Ap',S,1).*repmat(np(:,3)',S,1);

dCdx = gamma.*repmat(Ap',S,1).*repmat(es(:,1),1,P) - 2/3*gamma.*repmat(Ap',S,1).*repmat(np(:,1)',S,1);
dCdy = gamma.*repmat(Ap',S,1).*repmat(es(:,2),1,P) - 2/3*gamma.*repmat(Ap',S,1).*repmat(np(:,2)',S,1);
dCdz = gamma.*repmat(Ap',S,1).*repmat(es(:,3),1,P) - 2/3*gamma.*repmat(Ap',S,1).*repmat(np(:,3)',S,1);


Ax = zeros(S,3*M);
Ay = zeros(S,3*M);
Az = zeros(S,3*M);
for m = 1:M
    Ax(:,3*(m-1)+1) = sum(dCax(:,mu(m)==mp),2);
    Ax(:,3*(m-1)+2) = sum(dCsx(:,mu(m)==mp),2);
    Ax(:,3*(m-1)+3) = sum(dCdx(:,mu(m)==mp),2);

    Ay(:,3*(m-1)+1) = sum(dCay(:,mu(m)==mp),2);
    Ay(:,3*(m-1)+2) = sum(dCsy(:,mu(m)==mp),2);
    Ay(:,3*(m-1)+3) = sum(dCdy(:,mu(m)==mp),2);

    Az(:,3*(m-1)+1) = sum(dCaz(:,mu(m)==mp),2);
    Az(:,3*(m-1)+2) = sum(dCsz(:,mu(m)==mp),2);
    Az(:,3*(m-1)+3) = sum(dCdz(:,mu(m)==mp),2);
end

% multiply with flux per source (includes summing over sources)
Ax = Phi'*Ax;
Ay = Phi'*Ay;
Az = Phi'*Az;

% reshape into tripletts of standard deviation of coefficients of
% absorption, diffuse and specular reflection
c_std = reshape([ca_std,cs_std,cd_std]',3*M,1);

% variances for x, y, and z-components of force coefficient vector (separately for each radiation source: S x S)
Sa = [Ax;Ay;Az] * diag(c_std.^2) * [Ax;Ay;Az]';

% account for satellite mass
Sa = Sa/ms^2;
