%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [leap_sec_table] = leap_seconds()
% [leap_sec_table] = leap_seconds()
%
% 1st column = year
% 2nd column = leap second at the end of June
% 3rd column = leap second at the end of December
% 4th column = Julian Date at the end of June
% 5th column = Julian Date at the end of December
%
% Leap seconds occur on (a) June 30, at 23:59:60 UTC
%                       (b) December 31, at 23:59:60 UTC
%
% They have to be added to UTC time at the above dates.

% from http://en.wikipedia.org/wiki/Leap_second
leap_sec_table = [...
1972 +1 +1  2441499.5  2441683.5
1973  0 +1  2441864.5  2442048.5
1974  0 +1  2442229.5  2442413.5
1975  0 +1  2442594.5  2442778.5
1976  0 +1  2442960.5  2443144.5
1977  0 +1  2443325.5  2443509.5
1978  0 +1  2443690.5  2443874.5
1979  0 +1  2444055.5  2444239.5
1980  0  0  2444421.5  2444605.5
1981 +1  0  2444786.5  2444970.5
1982 +1  0  2445151.5  2445335.5
1983 +1  0  2445516.5  2445700.5
1984  0  0  2445882.5  2446066.5
1985 +1  0  2446247.5  2446431.5
1986  0  0  2446612.5  2446796.5
1987  0 +1  2446977.5  2447161.5
1988  0  0  2447343.5  2447527.5
1989  0 +1  2447708.5  2447892.5
1990  0 +1  2448073.5  2448257.5
1991  0  0  2448438.5  2448622.5
1992 +1  0  2448804.5  2448988.5
1993 +1  0  2449169.5  2449353.5
1994 +1  0  2449534.5  2449718.5
1995  0 +1  2449899.5  2450083.5
1996  0  0  2450265.5  2450449.5
1997 +1  0  2450630.5  2450814.5
1998  0 +1  2450995.5  2451179.5
1999  0  0  2451360.5  2451544.5
2000  0  0  2451726.5  2451910.5
2001  0  0  2452091.5  2452275.5
2002  0  0  2452456.5  2452640.5
2003  0  0  2452821.5  2453005.5
2004  0  0  2453187.5  2453371.5
2005  0 +1  2453552.5  2453736.5
2006  0  0  2453917.5  2454101.5
2007  0  0  2454282.5  2454466.5
2008  0 +1  2454648.5  2454832.5
2009  0  0  2455013.5  2455197.5
2010  0  0  2455378.5  2455562.5
2011  0  0  2455743.5  2455927.5
2012 +1  0  2456109.5  2456293.5
2013  0  0  2456474.5  2456658.5
2014  0  0  2456839.5  2457023.5
2015 +1  0  2457204.5  2457388.5
2016  0 +1  2457570.5  2457754.5
2017  0  0  2457935.5  2458119.5
2018  0  0  2458300.5  2458484.5
2019  0  0  2458665.5  2458849.5
2020  0  0  2459031.5  2459215.5
2021  0  0  2459396.5  2459580.5
2022  0  0  2459761.5  2459945.5
2023  0  0  2460126.5  2460310.5]; % note: the very last leap second is not announced yet, but the matrix has to be complete

% in line for 2015:
% "gregorian_to_julian_day(2015,7,1,0,0,0)" and "gregorian_to_julian_day(2016,1,1,0,0,0)"

% start of GPS time at 2444244.5 JD



