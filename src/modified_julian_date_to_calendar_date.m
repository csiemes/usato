%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Y,M,D] = modified_julian_date_to_calendar_date(MJD)
% [Y,M,D] = modified_julian_date_to_calendar_date(MJD)
% 
% Y ... year
% M ... month
% D ... day, including day fractions
% MJD ... modified Julian Day, including day fractions
%
% see Montenbruck and Gill (2000), pages 322-323

a = floor(MJD) + 2400001;

% calculation valid only after 10 October 1582
% (note that the calculation before that date is in the reference given above)
if sum(a < 2299161) > 0
    error('Year outside permitted range!')
end

q = MJD - floor(MJD);

b = floor((a - 1867216.25)/36524.25);

c = a + b - floor(b/4) + 1525;

d = floor((c - 121.1)/365.25);

e = floor(365.25*d);

f = floor((c - e)/30.6001);

D = c - e - floor(30.6001*f) + q;

M = f - 1 - 12*floor(f/14);

Y = d - 4715 - floor((7 + M)/10);




