%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [dm,Ta,Te,dmv,dnv,cmv,molar_mass,ap,f107a,f107,apv] = nrlmsise00(t,r,t_ap,ap,t_f107,f107,varargin)
% [dm,Ta,Te,dmv,dnv,cmv,molar_mass,ap,f107a,f107,apv] = nrlmsise00(t,r,t_ap,ap,t_f107,f107)
% [dm,Ta,Te,dmv,dnv,cmv,molar_mass,ap,f107a,f107,apv] = nrlmsise00(t,r,t_ap,ap,t_f107,f107,switches)
%
% Inputs:
%   t       epoch of positions (Matlab's datenum)
%   r       position in Earth-centered, Earth-fixed reference frame (meters)
%   t_ap    epochs of 3-hourly index (Matlab's datenum)
%   ap      Ap index (3 hourly index, indicator of geomagnetic activity)
%   t_f107  epochs of F10.7 index (Matlab's datenum)
%   f107    F10.7 index (daily index, F10.7 = solar radio flux at 2800 Mhz)
%
% Optional inputs:
%   switches  24 switches that turn on/off parts of the NRLMSISE-00 model
%
% Outputs:
%   dm      total mass density including anomalous oxygen (kg/m^3)
%   Ta      temperature at atlitude (K)
%   Te      exospheric temperature (K)
%   dmv     mass density per species (kg/m^3)
%   dnv     number density per species (#/m^3)
%   cmv     concentration according to mass (-)
%   molar_mass   molar mass of species (1/mol)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% C code was retrieved from 
% https://ccmc.gsfc.nasa.gov/pub/modelweb/atmospheric/msis/nrlmsise00/nrlmsis00_c_version/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a vectorized version, i.e. 7 element vector is in truth a N x 7 matrix.
%
% Input for C code:
% 	year    year, currently ignored
% 	doy     day of year
% 	sec     seconds in day (UT), including fraction of seconds
% 	alt     altitude in kilometers
% 	g_lat   geodetic latitude (degrees)
%   g_long  geodetic longitude (degrees)
% 	lst     local apparent solar time (hours), see note below
% 	f107a   81 day average of F10.7 flux (centered on doy)
% 	f107    daily F10.7 flux for previous day
% 	ap      magnetic index(daily)
%   apv     Vector with 7 elements containing the following magnetic values:
%            apv(1) = daily AP
%            apv(2) = 3 hr AP index for current time
%            apv(3) = 3 hr AP index for 3 hrs before current time
%            apv(4) = 3 hr AP index for 6 hrs before current time
%            apv(5) = 3 hr AP index for 9 hrs before current time
%            apv(6) = Average of eight 3 hr AP indicies from 12 to 33 hrs 
%                     prior to current time
%            apv(7) = Average of eight 3 hr AP indicies from 36 to 57 hrs 
%                     prior to current time  
%   sw      Switches to turn on and off particular variations.
%           Value 0 is "off", value 1 is "on", and value 2 is "main effects
%           off but cross terms on". Explanation:
%            sw(1) = output in meters and kilograms instead of centimeters and grams
%            sw(2) = F10.7 effect on mean
%            sw(3) = time independent
%            sw(4) = symmetrical annual
%            sw(5) = symmetrical semiannual
%            sw(6) = asymmetrical annual
%            sw(7) = asymmetrical semiannual
%            sw(8) = diurnal
%            sw(9) = semidiurnal
%            sw(10) = daily ap [when this is set to -1 (!) the pointer
%                     ap_a in struct nrlmsise_input must
%                     point to a struct ap_array]
%            sw(11) = all UT/long effects
%            sw(12) = longitudinal
%            sw(13) = UT and mixed UT/long
%            sw(14) = mixed AP/UT/LONG
%            sw(15) = terdiurnal
%            sw(16) = departures from diffusive equilibrium
%            sw(17) = all TINF var
%            sw(18) = all TLB var
%            sw(19) = all TN1 var
%            sw(20) = all S var
%            sw(21) = all TN2 var
%            sw(22) = all NLB var
%            sw(23) = all TN3 var
%            sw(24) = turbo scale height var
%
% Output from C code:
%   d       Vector with 9 elements containing the following:
%            d(1) - HE number densities (CM-3)
%            d(2) - O number densities (CM-3)
%            d(3) - N2 number densities (CM-3)
%            d(4) - O2 number densities (CM-3)
%            d(5) - AR number densities (CM-3)                       
%            d(6) - total mass density (GM/CM3), excludes anomalous oxygen
%            d(7) - H number densities (CM-3)
%            d(8) - N number densities(CM-3)
%            d(9) - Anomalous oxygen number densities(CM-3)
%
% 	t       Vector with 2 elements containing the following temperatures:
%            t(1) - exospheric temperature
%            t(2) - temperature at altitude
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% by default, conversion to geodetic coordinates assumes meters as unit
[L,B,h] = cartesian_to_ellipsoidal(r(:,1),r(:,2),r(:,3));

% convert altitude from meters to km
h = h / 1000;

% convert latitude and longitude from radians to degrees
L = L * 180/pi;
B = B * 180/pi;

% date conversion
[year,month,day,hour,minute,second] = datevec(t);
doy = date_to_doy(year,month,day);
second = second + 60*minute + 3600*hour;

% local solar time inferred from time and longitude
lst = second/3600 + L/15;

% default switches for NRLMSISE-00
if nargin == 6
    switches = [0 ones(1,23)];
elseif nargin == 7
    switches = varargin{1};
else
    error('Need 6 or 7 input parameters!')
end

% crop to the time interval that we need: t +/- 41 days (with margin)
ta = t(1) - 121; % 41 days is roughly 0.5 * 81 days
tb = t(end) + 121; % 41 days is roughly 0.5 * 81 days
ap(t_ap < ta | t_ap > tb) = [];
t_ap(t_ap < ta | t_ap > tb) = [];
f107(t_f107 < ta | t_f107 > tb) = [];
t_f107(t_f107 < ta | t_f107 > tb) = [];

% calculate 81-day average of F10.7
f107a = zeros(size(f107));
for n = 41:length(t_f107)
    na = n-40;
    nb = min(n+40,length(f107)); % we might perform the calculation when F10.7 is not yet avaliable for the enxt 40 days
    f107_int = f107(na:nb); % the time step is always one day
    f107a(n) = mean(f107_int(f107_int~=0)); % ignore zero (= missing) values
end

% interpolate F10.7 and its 81-day average to epochs of orbit
f107 = interp1(t_f107,f107,t,'linear');
f107a = interp1(t_f107,f107a,t,'linear');

% calculate daily ap
t_ap_daily = ceil(t_ap(1)):floor(t_ap(end));
ap_daily = zeros(size(t_ap_daily));
for n = 1:length(t_ap_daily)
    ap_daily(n) = mean(ap(t_ap >= t_ap_daily(n) & t_ap < t_ap_daily(n)+1));
end

% calculate daily Ap
apv = zeros(length(t),7);
apv(:,1) = interp1(t_ap_daily,ap_daily,t,'linear');
apv(:,2) = interp1(t_ap,ap,t,'linear');
apv(:,3) = interp1(t_ap,ap,t-3/24,'linear');
apv(:,4) = interp1(t_ap,ap,t-6/24,'linear');
apv(:,5) = interp1(t_ap,ap,t-9/24,'linear');
for n = 1:length(t)
    apv(n,6) = mean(ap(t_ap > t(n)-33 & t_ap<t(n)-12));
    apv(n,7) = mean(ap(t_ap > t(n)-57 & t_ap<t(n)-36));
end
ap_daily = apv(:,1);


% Call C code of NRLMSISE-00 to calculate density and temperatures
[d,T] = nrlmsise00_mex_wrapper(year,doy,second,h,B,L,lst,f107a,f107,ap_daily,apv,switches);
      
% Remove total mass entry
d(:,6) = [];

% convert density from cm^-3 to m^-3
d = d * 1e6;

% molar mass in [g/mol]
molar_mass = [4    % He
              16   % O
              28   % N2
              32   % O2
              40   % Ar
              1    % H
              14   % N
              16]; % O+
          
% convert [g/mol] to [kg/mol]
molar_mass = molar_mass/1000;
          
% Avogardo constant (particles per mol)
Na = 6.02214076e23;

% calculate mass density (small differences to d(6) come from 1/Na = 1.66e-24 in NRLMSISE-00 C code)
dm = d*molar_mass/Na;
Te = T(:,1);
Ta = T(:,2);

% calculate mass density per species
dmv = d.*repmat(molar_mass',length(t),1)/Na;

% number density per species
dnv = d;

% calculate concentration
cmv = dmv./repmat(dm,1,length(molar_mass));

% prepare Ap index for output
ap = apv(:,2);


