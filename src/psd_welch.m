%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [P,f] = psd_welch(x,w,m,fs,s)
% [P,f] = psd_welch(x,window,overlap,fs,s)
%
% Estimates the PSD by Welch's method
%
% x = time series (N x 1 vector)
% w = window function (L x 1 vector)
% m = overlap (fraction of window length)
% fs = sampling frequency (Hz)
% s = switch: 'onesided' or 'twosided' PSD
%
% P = PSD
% f = frequency

N = length(x); % length time series
L = length(w); % length window function
D = L - round(m*L); % distance between starting points of segments

% number of complete segments
K = floor((N-L)/D) + 1;

% scaling factor for window function
U = sum(w.^2) / L;

% init frequency and PSD
f = (0:(L-1))'/L;
P = zeros(L,1);

% loop over complete segments
for k = 1:K
    A = fft(x(1+(k-1)*D:L+(k-1)*D).*w)/L;
    I = L/U * abs(A).^2;
    P = P + I;
end

P = P / K;

% note: the remainder after the complete segments is simply not used

% rescale according to sampling frequency
f = f * fs;
P = P / fs;

% select one-sided or two-sided
if strcmp(s,'onesided')
    L2 = floor(L/2) + 1;
    f = f(1:L2);
    P = P(1:L2);
    if mod(L,2) == 0
        P(2:L2-1) = P(2:L2-1) * 2;
    else
        P(2:L2) = P(2:L2) * 2;
    end
elseif ~strcmp(s,'twosided')
    error('Switch must be ''onesided'' or ''twosided''.')
end


