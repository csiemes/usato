%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [dv] = gps2gps(varargin)
% [GPS_date_time] = gps2gps(GPS_seconds)
%
% GPS_seconds ..... GPS seconds, N x 1 matrix
%
% GPS_date_time ... GPS date and time vector, N x 6 matrix


% convert GPS date/time to TAI Julian date (TAI = GPS + 19 seconds)
if nargin == 1
    
    % check if we have GPS seconds as input
    if size(varargin{1},2) == 1
        gps_sec = varargin{1};
        
        % splitting into integer days and seconds of day gives higher precision in
        % the time conversion (~ 1e-11 s, division by 86400 s causes loss of precision)
        gps_1 = floor(gps_sec/86400);
        gps_2 = gps_sec - gps_1*86400;
        
        gps_jd = [repmat(2444244.5,length(gps_sec),1)+gps_1,gps_2/86400];
    else
        error('input argument must have 1 column')
    end
else
    error('number of input arguments must be 1')
end

% convert GPS Julian date to date vector
dv = interface_Jd2cal(gps_jd);



