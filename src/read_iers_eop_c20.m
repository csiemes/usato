%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [EOP] = read_iers_eop_c20(filename)
% [EOP] = read_iers_eop_c20(filename)
%
% Reads the table with Earth orientation parameters provided by the IERS.
%
% filename ... Filename including path to file with EOP C04 time series.
% 
% EOP ... Struct with fields:
%     EOP.Date_UTC ............ UTC Date (time = 00:00:00 UTC)
%     EOP.MJD_UTC ............. MJD, Modified Julian Day (days)
%     EOP.x ................... Polar motion, x coordinate (arcsec)
%     EOP.y ................... Polar motion, y coordinate (arcsec)
%     EOP.UT1_minus_UTC ....... UT1 - UTC time difference (seconds);
%     EOP.LOD ................. Length of day (seconds)
%     EOP.dX .................. (arcsec)
%     EOP.dY .................. (arcsec)
%
% All parameters are provided with errors (same units as parameters), i.e.
%     EOP.x_err
%     EOP.y_err
%     EOP.UT1_minus_UTC_err
%     EOP.LOD_err
%     EOP.dX_err
%     EOP.dY_err

% open file for reading
fid = fopen(filename);

% skip header (14 lines)
for n = 1:6
    fgetl(fid);
end

% read data of table
d = fscanf(fid,'%d %d %d %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n');
d = reshape(d,21,length(d)/21)';

% assign data to structure
EOP.Date_UTC          = d(:,1:4);
EOP.MJD_UTC           = d(:,5);
EOP.x                 = d(:,6);
EOP.y                 = d(:,7);
EOP.UT1_minus_UTC     = d(:,8);
EOP.dX                = d(:,9);
EOP.dY                = d(:,10);
EOP.xrt               = d(:,11);
EOP.yrt               = d(:,12);
EOP.LOD               = d(:,13);
EOP.x_err             = d(:,14);
EOP.y_err             = d(:,15);
EOP.UT1_minus_UTC_err = d(:,16);
EOP.dX_err            = d(:,17);
EOP.dY_err            = d(:,18);
EOP.xrt_err           = d(:,19);
EOP.yrt_err           = d(:,20);
EOP.LOD_err           = d(:,21);
