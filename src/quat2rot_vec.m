%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [R] = quat2rot_vec(q)
% [R] = quat2rot_vec(q)
%
% returns the rotation matrix corresponding to a quaternion
%
% q = [q0 q1 q2 q3]
%
% q0 ... real part
% q1,q2,q3 ... imaginary part

% one quaternion in each column of "q"
if size(q,1) == 4
    R = [q(1,:).^2+q(2,:).^2-q(3,:).^2-q(4,:).^2
         2*(q(2,:).*q(3,:)+q(1,:).*q(4,:))
         2*(q(2,:).*q(4,:)-q(1,:).*q(3,:)) 
         2*(q(2,:).*q(3,:)-q(1,:).*q(4,:))  
         q(1,:).^2-q(2,:).^2+q(3,:).^2-q(4,:).^2 
         2*(q(3,:).*q(4,:)+q(1,:).*q(2,:)) 
         2*(q(2,:).*q(4,:)+q(1,:).*q(3,:))
         2*(q(3,:).*q(4,:)-q(1,:).*q(2,:))
         q(1,:).^2-q(2,:).^2-q(3,:).^2+q(4,:).^2];

% one quaternion in each row of "q"
elseif size(q,2) == 4
	R = [q(:,1).^2+q(:,2).^2-q(:,3).^2-q(:,4).^2 ...
         2*(q(:,2).*q(:,3)+q(:,1).*q(:,4)) ...
         2*(q(:,2).*q(:,4)-q(:,1).*q(:,3)) ...
         2*(q(:,2).*q(:,3)-q(:,1).*q(:,4)) ...
         q(:,1).^2-q(:,2).^2+q(:,3).^2-q(:,4).^2 ...
         2*(q(:,3).*q(:,4)+q(:,1).*q(:,2)) ...
         2*(q(:,2).*q(:,4)+q(:,1).*q(:,3)) ...
         2*(q(:,3).*q(:,4)-q(:,1).*q(:,2)) ...
         q(:,1).^2-q(:,2).^2-q(:,3).^2+q(:,4).^2];
     
else
    error('size(q,1) ~= 4 and size(q,2) ~= 4')
end
