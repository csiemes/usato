%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [M] = init_earth_infrared_irradiance_maps(pth)

% check if we need to append a forward or backward slash to the path
if ispc()
    if ~isempty(pth) && pth(end) ~= '\'
        pth = [pth '\'];
    end
else
    if ~isempty(pth) && pth(end) ~= '/'
        pth = [pth '/'];
    end
end

% define grid size (depends on files - adjust if you load different files)
n_lon = 144;
n_lat = 72;

M.dimension = [n_lat n_lon];

% load maps (files have to be named using this pattern: 'r*_r.dat')
maps_ir = cell(12,1);
files = dir([pth 'r*_r.dat']);
for n = 1:length(files)
    fid = fopen(filename(files(n)));
    % divide by 255 brings the range to [0,1], then multiply by 400 converts to flux units of W/m2
    maps_ir{n} = double(fread(fid,[144,72],'uint8')')/255*400;
    fclose(fid);
end

% copy maps into structure (reshape to vectors, and prepend December and
% append January for easy interpolation)
M.maps = zeros(14,n_lon*n_lat);
M.maps(1,:) = reshape(maps_ir{12},1,144*72);
for m = 1:12
    M.maps(m+1,:) = reshape(maps_ir{m},1,144*72);
end
M.maps(14,:) = reshape(maps_ir{1},1,144*72);

% center day-of-year of each calendar month, where we prepend December and
% append January for easy interpolation
M.doy = [-15 16 47 75 106 136 167 197 228 259 289 320 350 381];

% step size in the maps' grid, unit = radians
dlon = 360/n_lon * pi/180;
dlat = 180/n_lat * pi/180;

% longitude and latitude axes
maps_lon = dlon/2:dlon:2*pi;
maps_lat = pi/2-dlat/2:-dlat:-pi/2;

% Earth radius, unit = m
R = 6378137;

% the pixel areas are computed assuming that Earth is a sphere
% this produces an insignificant error of less than 1%
maps_pixel_areas = 2*dlon*R^2*sin(dlat/2)*sin(pi/2-maps_lat);

% areas of maps pixels, unit = m2
M.pixel_areas = repmat(maps_pixel_areas',n_lon,1);

% interpret pixel locations on elliposoid to represent the direction of the
% surface normal more accurately - geodetic longitude and latitude of map
% pixels, unit = rad
L = reshape(repmat(maps_lon,length(maps_lat),1),length(maps_lon)*length(maps_lat),1);
B = reshape(repmat(maps_lat',1,length(maps_lon)),length(maps_lon)*length(maps_lat),1);
h = zeros(size(L));

% geodetic longitude and latitude of map pixels, unit = deg
M.longitude = L * 180/pi;
M.latitude = B * 180/pi;

% convert geodetic longitude and latitude to surface normals, unit = m
[maps_x_ecef,maps_y_ecef,maps_z_ecef] = ellipsoidal_to_cartesian(L,B,h);

% surface normals in Earth-centered, Earth-fixed frame, unit = m
M.position = [maps_x_ecef,maps_y_ecef,maps_z_ecef];


% imagesc(maps_lon*180/pi,maps_lat*180/pi,maps_ir{n})
% colorbar
% axis xy




