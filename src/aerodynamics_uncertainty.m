%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [H] = aerodynamics_uncertainty(vr,n,A,rho,m,T,alpha,Tw,aa,e,ms)
% [H] = aerodynamics_uncertainty(vr,n,A,rho,m,T,alpha,Tw,aa,e)
%
% vr ... relative velocity, satellite frame, (m/s), dimension 3 x 1
% n ... outward normal vectors of panels, satellite frame, (-), dimension I x 3
% A ... areas of panels, (m2), dimension I x 1
% rho ... mass density per atmospheric constituent, (kg/m3), dimension J x 1
% m ... molar mass of atmospheric constituents, (g/mol), dimension J x 1
% T ... atmospheric temperature, (K), dimension 1 x 1
% Tw ... wall temperature, (K), dimension I x 1 
% alpha ... energy accommodation coefficient, (-), 1 x 1 
% aa ... aerodynamic acceleration, (m/s2), 3 x 1
% e ... direction from which density is derived, unit vector, 3 x 1
% ms ... satellite mass, (kg), 1 x 1
% 
% P ... number of panels
% J ... number of atmospheric constituents
%
% A = design matrix for debugging

% % DEBUG
% vr = 1.0e+03 * [-5.580703231600000
%                  4.812012331700000
%                 -1.746752227900000];
% n = [ 1.000000000000000                   0                   0
%      -1.000000000000000                   0                   0
%                       0   0.766044000000000  -0.642787000000000
%                       0  -0.766044000000000  -0.642787000000000
%                       0                   0   1.000000000000000
%                       0                   0  -1.000000000000000];
% A = [0.955156700000000
%    0.955156700000000
%    3.155479200000000
%    3.155479200000000
%    6.071112000000000
%    2.167362000000000];
% rho = 1.0e-12 * [0.011436270965833
%    0.452624770143304
%    0.024360806665282
%    0.000563455672398
%    0.000001622287010
%    0.000071504592998
%    0.004444715493482
%    0.000460010175769];
% m = 1.0e+03 * [0.004000000000000
%    0.016000000000000
%    0.028000000000000
%    0.032000000000000
%    0.040000000000000
%    0.001000000000000
%    0.014000000000000
%    0.016000000000000];
% T = 1.103166913159333e+03;
% alpha = 0.850000000000000;
% Tw = [350
%    350
%    350
%    350
%    350
%    350];
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

I = numel(A);
J = numel(rho);

% make sure it is a unit vector
e = e./sqrt(sum(e.^2));

% convert from g/mol to kg/mol
m = m/1000;

% norm of relative velocity
vrn = sqrt(sum(vr.^2));

% normalized drag vectors
uhd = vr / vrn; % 3 x 1

% lift vectors (per panel), norm of lift vectors
ul = -cross(cross(repmat(vr',I,1),n),repmat(vr',I,1)); % I x 3
uln = sqrt(sum(ul.^2,2)); % I x 1
uhl = ul./repmat(uln,1,3); % I x 3

% when the panel is perfectly facing the flow, the lift vector is zero
% but since we normalize, we get nans -> fix this here
ind_no_lift = uln < vrn.^2*1e-14;
uhl(ind_no_lift,:) = 0;

% Gas constant, (J K−1 mol−1)
R = 8.31446261815324;

% most probable speed
cmp = sqrt(2*R*T./m);

% ratio of reflected and incident velocity (v_re / v_inc), I x J
r = sqrt(0.5*(1 + alpha*((4*R*Tw/vrn^2)*(1./m') - 1)));

% some variables from Sentman's equations
gamma = -n*uhd; % I x 1
l = -sum(n.*uhl,2); % I x 1
S = vrn./cmp; % speed ratio, J x 1
P = repmat(1./S',I,1) .* exp(-gamma.^2 * S'.^2); % I x J
Z = 1 + erf(gamma * S'); % I x J

% keyboard

% drag and lift per panel and constituent, I x J
CD = repmat(A,1,J) .* (P/sqrt(pi) + (gamma * (1+1./S'.^2/2)) .* Z + sqrt(pi)/2*repmat(gamma.^2,1,J).*r.*Z + repmat(gamma,1,J).*r.*P/2);
CL = repmat(A,1,J) .* ((l*(1./S'.^2/2)).*Z + repmat(l,1,J).*r/2 .* (sqrt(pi)*repmat(gamma,1,J).*Z + P));

% aerodynamic force vector, 3 x 1
Ca = (CD'*repmat(uhd',I,1) + CL'*uhl)' * (rho/sum(rho));

%%%%%%%%%%%%%%%%%

% dimension 1 x 1
uhdx_vrx = 1/vrn - vr(1)^2/vrn^3;
uhdx_vry = -vr(1)*vr(2)/vrn^3;
uhdx_vrz = -vr(1)*vr(3)/vrn^3;

uhdy_vrx = -vr(2)*vr(1)/vrn^3;
uhdy_vry = 1/vrn - vr(2)^2/vrn^3;
uhdy_vrz = -vr(2)*vr(3)/vrn^3;

uhdz_vrx = -vr(3)*vr(1)/vrn^3;
uhdz_vry = -vr(3)*vr(2)/vrn^3;
uhdz_vrz = 1/vrn - vr(3)^2/vrn^3;

% dimension I x 1
ulx_vrx = vr(3)*n(:,3) + vr(2)*n(:,2);
ulx_vry = vr(1)*n(:,2) - 2*vr(2)*n(:,1);
ulx_vrz = vr(1)*n(:,3) - 2*vr(3)*n(:,1);

uly_vrx = vr(2)*n(:,1) - 2*vr(1)*n(:,2);
uly_vry = vr(1)*n(:,1) + vr(3)*n(:,3);
uly_vrz = vr(2)*n(:,3) - 2*vr(3)*n(:,2);

ulz_vrx = vr(3)*n(:,1) - 2*vr(1)*n(:,3);
ulz_vry = vr(3)*n(:,2) - 2*vr(2)*n(:,3);
ulz_vrz = vr(2)*n(:,2) + vr(1)*n(:,1);

% dimension I x 1
hx = (ul(:,1).*ulx_vrx + ul(:,2).*uly_vrx + ul(:,3).*ulz_vrx);
hy = (ul(:,1).*ulx_vry + ul(:,2).*uly_vry + ul(:,3).*ulz_vry);
hz = (ul(:,1).*ulx_vrz + ul(:,2).*uly_vrz + ul(:,3).*ulz_vrz);

uhlx_vrx = ulx_vrx./uln - ul(:,1)./uln.^3.*hx;
uhlx_vry = ulx_vry./uln - ul(:,1)./uln.^3.*hy;
uhlx_vrz = ulx_vrz./uln - ul(:,1)./uln.^3.*hz;

uhly_vrx = uly_vrx./uln - ul(:,2)./uln.^3.*hx;
uhly_vry = uly_vry./uln - ul(:,2)./uln.^3.*hy;
uhly_vrz = uly_vrz./uln - ul(:,2)./uln.^3.*hz;

uhlz_vrx = ulz_vrx./uln - ul(:,3)./uln.^3.*hx;
uhlz_vry = ulz_vry./uln - ul(:,3)./uln.^3.*hy;
uhlz_vrz = ulz_vrz./uln - ul(:,3)./uln.^3.*hz;

% when the lift vector is zero, the partial should be zero
uhlx_vrx(ind_no_lift) = 0;
uhlx_vry(ind_no_lift) = 0;
uhlx_vrz(ind_no_lift) = 0;

uhly_vrx(ind_no_lift) = 0;
uhly_vry(ind_no_lift) = 0;
uhly_vrz(ind_no_lift) = 0;

uhlz_vrx(ind_no_lift) = 0;
uhlz_vry(ind_no_lift) = 0;
uhlz_vrz(ind_no_lift) = 0;

% dimension I x 1
l_vrx = -(uhlx_vrx.*n(:,1) + uhly_vrx.*n(:,2) + uhlz_vrx.*n(:,3));
l_vry = -(uhlx_vry.*n(:,1) + uhly_vry.*n(:,2) + uhlz_vry.*n(:,3));
l_vrz = -(uhlx_vrz.*n(:,1) + uhly_vrz.*n(:,2) + uhlz_vrz.*n(:,3));

% dimension I x J
r_alpha = (R/vrn^2*Tw*(1./m') - 1/4)./r;

% dimension I x J (note that d(r(i,j))/d(Tw(k)) = 0 for i ~= k --> store only i == k)
r_Tw = alpha*R/vrn^2 * repmat(1./m',I,1) .* (1./r);

% dimension I x J
h = -2./r .* (Tw*(1./m')) * alpha*R/vrn^4;
r_vrx = h * vr(1);
r_vry = h * vr(2);
r_vrz = h * vr(3);

% dimension J x 1
S_T = -0.5*vrn/T./cmp;
S_vrx = vr(1)/vrn./cmp;
S_vry = vr(2)/vrn./cmp;
S_vrz = vr(3)/vrn./cmp;

% dimension I x 1
h = (vr(1)*n(:,1) + vr(2)*n(:,2) + vr(3)*n(:,3)) / vrn^3;
gamma_vrx = vr(1)*h - n(:,1)/vrn;
gamma_vry = vr(2)*h - n(:,2)/vrn;
gamma_vrz = vr(3)*h - n(:,3)/vrn;

% dimension I x J (note that d(CDij)/d(Sk) = 0 for k ~= j --> not needed)
CD_S = repmat(A,1,J) .* (  P/sqrt(pi) .* ((gamma.^2 - 1) * (1./S)') ...
                         - Z .* (gamma * (1./S.^3)') ...
                         - P/2 .* r .* (gamma * (1./S)') );

CD_r =   repmat(A.*gamma.^2*sqrt(pi)/2,1,J) .* Z ...
       + repmat(A.*gamma/2,1,J) .* P;

% dimension I x J (note that d(CDij)/d(gammak) = 0 for k ~= i --> not needed)
CD_gamma = repmat(A,1,J) .* ...
    (  (repmat(gamma/sqrt(pi),1,J) + r/2) .* P ...
     + (1 + repmat(1./S'.^2/2,I,1) + repmat(gamma,1,J).*r*sqrt(pi)) .* Z);

CD_A = CD ./ repmat(A,1,J);

% dimension I x J (note that d(CLij)/d(Sk) = 0 for k ~= j --> not needed)
CL_S = repmat(A.*l,1,J) .* ((gamma*(1./S')).*P/sqrt(pi) - repmat(1./S'.^3,I,1).*Z - r.*repmat(1./S',I,1).*P/2);

CL_r = repmat(A.*l/2,1,J) .* (repmat(gamma,1,J).*Z*sqrt(pi) + P);

% dimension I x J (note that d(CLij)/d(gammak) = 0 for k ~= i --> not needed)
CL_l = CL ./ repmat(l,1,J);

% lift vector is zero
CL_l(ind_no_lift,:) = 0;

CL_gamma = repmat(A.*l,1,J) .* (P/sqrt(pi) + r.*Z*sqrt(pi)/2);

CL_A = CL ./ repmat(A,1,J);

% dimension I x J
CD_vrx = CD_S .* repmat(S_vrx',I,1) + CD_gamma .* repmat(gamma_vrx,1,J) + CD_r .* r_vrx;
CD_vry = CD_S .* repmat(S_vry',I,1) + CD_gamma .* repmat(gamma_vry,1,J) + CD_r .* r_vry;
CD_vrz = CD_S .* repmat(S_vrz',I,1) + CD_gamma .* repmat(gamma_vrz,1,J) + CD_r .* r_vrz;

CL_vrx = CL_S .* repmat(S_vrx',I,1) + CL_gamma .* repmat(gamma_vrx,1,J) + CL_r .* r_vrx + CL_l .* repmat(l_vrx,1,J);
CL_vry = CL_S .* repmat(S_vry',I,1) + CL_gamma .* repmat(gamma_vry,1,J) + CL_r .* r_vry + CL_l .* repmat(l_vry,1,J);
CL_vrz = CL_S .* repmat(S_vrz',I,1) + CL_gamma .* repmat(gamma_vrz,1,J) + CL_r .* r_vrz + CL_l .* repmat(l_vrz,1,J);

% dimension 1 x 1
h = rho/sum(rho); % J x 1

Cax_vrx = sum((CD_vrx*uhd(1) + CD*uhdx_vrx + CL_vrx.*repmat(uhl(:,1),1,J) + CL.*repmat(uhlx_vrx,1,J))*h);
Cax_vry = sum((CD_vry*uhd(1) + CD*uhdx_vry + CL_vry.*repmat(uhl(:,1),1,J) + CL.*repmat(uhlx_vry,1,J))*h);
Cax_vrz = sum((CD_vrz*uhd(1) + CD*uhdx_vrz + CL_vrz.*repmat(uhl(:,1),1,J) + CL.*repmat(uhlx_vrz,1,J))*h);

Cay_vrx = sum((CD_vrx*uhd(2) + CD*uhdy_vrx + CL_vrx.*repmat(uhl(:,2),1,J) + CL.*repmat(uhly_vrx,1,J))*h);
Cay_vry = sum((CD_vry*uhd(2) + CD*uhdy_vry + CL_vry.*repmat(uhl(:,2),1,J) + CL.*repmat(uhly_vry,1,J))*h);
Cay_vrz = sum((CD_vrz*uhd(2) + CD*uhdy_vrz + CL_vrz.*repmat(uhl(:,2),1,J) + CL.*repmat(uhly_vrz,1,J))*h);

Caz_vrx = sum((CD_vrx*uhd(3) + CD*uhdz_vrx + CL_vrx.*repmat(uhl(:,3),1,J) + CL.*repmat(uhlz_vrx,1,J))*h);
Caz_vry = sum((CD_vry*uhd(3) + CD*uhdz_vry + CL_vry.*repmat(uhl(:,3),1,J) + CL.*repmat(uhlz_vry,1,J))*h);
Caz_vrz = sum((CD_vrz*uhd(3) + CD*uhdz_vrz + CL_vrz.*repmat(uhl(:,3),1,J) + CL.*repmat(uhlz_vrz,1,J))*h);

Cax_T = sum((CD_S.*repmat(S_T',I,1)*uhd(1) + CL_S.*repmat(S_T',I,1).*repmat(uhl(:,1),1,J))*h);
Cay_T = sum((CD_S.*repmat(S_T',I,1)*uhd(2) + CL_S.*repmat(S_T',I,1).*repmat(uhl(:,2),1,J))*h);
Caz_T = sum((CD_S.*repmat(S_T',I,1)*uhd(3) + CL_S.*repmat(S_T',I,1).*repmat(uhl(:,3),1,J))*h);

Cax_alpha = sum((CD_r.*r_alpha*uhd(1) + CL_r.*r_alpha.*repmat(uhl(:,1),1,J))*h);
Cay_alpha = sum((CD_r.*r_alpha*uhd(2) + CL_r.*r_alpha.*repmat(uhl(:,2),1,J))*h);
Caz_alpha = sum((CD_r.*r_alpha*uhd(3) + CL_r.*r_alpha.*repmat(uhl(:,3),1,J))*h);

Cax_Tw = (CD_r.*r_Tw*uhd(1) + CL_r.*r_Tw.*repmat(uhl(:,1),1,J))*h;
Cay_Tw = (CD_r.*r_Tw*uhd(2) + CL_r.*r_Tw.*repmat(uhl(:,2),1,J))*h;
Caz_Tw = (CD_r.*r_Tw*uhd(3) + CL_r.*r_Tw.*repmat(uhl(:,3),1,J))*h;

Cax_A = (CD_A*uhd(1) + CL_A.*repmat(uhl(:,1),1,J))*h;
Cay_A = (CD_A*uhd(2) + CL_A.*repmat(uhl(:,2),1,J))*h;
Caz_A = (CD_A*uhd(3) + CL_A.*repmat(uhl(:,3),1,J))*h;


% dimension J x 1
hx = sum(CD*uhd(1) + CL.*repmat(uhl(:,1),1,J),1)'; % J x 1
hy = sum(CD*uhd(2) + CL.*repmat(uhl(:,2),1,J),1)'; % J x 1
hz = sum(CD*uhd(3) + CL.*repmat(uhl(:,3),1,J),1)'; % J x 1
Cax_rho = hx/sum(rho) - sum(rho.*hx)/sum(rho)^2;
Cay_rho = hy/sum(rho) - sum(rho.*hy)/sum(rho)^2;
Caz_rho = hz/sum(rho) - sum(rho.*hz)/sum(rho)^2;

% dimension, 1 x 1
rho_T = -2*(e'*aa)/(e'*Ca)^2*ms/vrn^2 * (e(1)*Cax_T + e(2)*Cay_T + e(3)*Caz_T);

rho_alpha = -2*(e'*aa)/(e'*Ca)^2*ms/vrn^2 * (e(1)*Cax_alpha + e(2)*Cay_alpha + e(3)*Caz_alpha);

% dimension I x 1
rho_Tw = -2*(e'*aa)/(e'*Ca)^2*ms/vrn^2 * (e(1)*Cax_Tw + e(2)*Cay_Tw + e(3)*Caz_Tw);

rho_A = -2*(e'*aa)/(e'*Ca)^2*ms/vrn^2 * (e(1)*Cax_A + e(2)*Cay_A + e(3)*Caz_A);

% dimension 1 x 1
rho_aax = 2*e(1)/(e'*Ca)*ms/vrn^2;
rho_aay = 2*e(2)/(e'*Ca)*ms/vrn^2;
rho_aaz = 2*e(3)/(e'*Ca)*ms/vrn^2;

rho_ms = 2*(e'*aa)/(e'*Ca)/vrn^2;

rho_vrx = -2*(e'*aa)/(e'*Ca)^2*ms/vrn^2 * (e(1)*Cax_vrx + e(2)*Cay_vrx + e(3)*Caz_vrx) - 4*(e'*aa)/(e'*Ca)*ms/vrn^4 * vr(1);
rho_vry = -2*(e'*aa)/(e'*Ca)^2*ms/vrn^2 * (e(1)*Cax_vry + e(2)*Cay_vry + e(3)*Caz_vry) - 4*(e'*aa)/(e'*Ca)*ms/vrn^4 * vr(2);
rho_vrz = -2*(e'*aa)/(e'*Ca)^2*ms/vrn^2 * (e(1)*Cax_vrz + e(2)*Cay_vrz + e(3)*Caz_vrz) - 4*(e'*aa)/(e'*Ca)*ms/vrn^4 * vr(3);

% dimension J x 1
rho_rho = -2*(e'*aa)/(e'*Ca)^2*ms/vrn^2 * (e(1)*Cax_rho + e(2)*Cay_rho + e(3)*Caz_rho);


H.rho_T = rho_T;
H.rho_rho = rho_rho;
H.rho_alpha = rho_alpha;
H.rho_Tw = rho_Tw;
H.rho_A = rho_A;
H.rho_aax = rho_aax;
H.rho_aay = rho_aay;
H.rho_aaz = rho_aaz;
H.rho_ms = rho_ms;
H.rho_vrx = rho_vrx;
H.rho_vry = rho_vry;
H.rho_vrz = rho_vrz;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUGGING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% H.Ca = Ca;
% 
% 
% H.gamma_vrx = gamma_vrx;
% H.gamma_vry = gamma_vry;
% H.gamma_vrz = gamma_vrz;
% 
% H.S_vrx = S_vrx;
% H.S_vry = S_vry;
% H.S_vrz = S_vrz;
% 
% H.r_vrx = r_vrx;
% H.r_vry = r_vry;
% H.r_vrz = r_vrz;
% 
% H.l_vrx = l_vrx;
% H.l_vry = l_vry;
% H.l_vrz = l_vrz;
% 
% H.uhd_vrx = [uhdx_vrx uhdy_vrx uhdz_vrx];
% H.uhd_vry = [uhdx_vry uhdy_vry uhdz_vry];
% H.uhd_vrz = [uhdx_vrz uhdy_vrz uhdz_vrz];
% 
% H.uhl_vrx = [uhlx_vrx uhly_vrx uhlz_vrx];
% H.uhl_vry = [uhlx_vry uhly_vry uhlz_vry];
% H.uhl_vrz = [uhlx_vrz uhly_vrz uhlz_vrz];
% 
% 
% 
% H.Cax_vrx = Cax_vrx;
% H.Cax_vry = Cax_vry;
% H.Cax_vrz = Cax_vrz;
% H.Cax_T = Cax_T;
% H.Cax_alpha = Cax_alpha;
% H.Cax_Tw = Cax_Tw;
% H.Cax_A = Cax_A;
% H.Cax_rho = Cax_rho;
% 
% H.Cay_vrx = Cay_vrx;
% H.Cay_vry = Cay_vry;
% H.Cay_vrz = Cay_vrz;
% H.Cay_T = Cay_T;
% H.Cay_alpha = Cay_alpha;
% H.Cay_Tw = Cay_Tw;
% H.Cay_A = Cay_A;
% H.Cay_rho = Cay_rho;
% 
% H.Caz_vrx = Caz_vrx;
% H.Caz_vry = Caz_vry;
% H.Caz_vrz = Caz_vrz;
% H.Caz_T = Caz_T;
% H.Caz_alpha = Caz_alpha;
% H.Caz_Tw = Caz_Tw;
% H.Caz_A = Caz_A;
% H.Caz_rho = Caz_rho;
% 
% 
% H.CD_vrx = CD_vrx;
% H.CD_vry = CD_vry;
% H.CD_vrz = CD_vrz;
% 
% H.CL_vrx = CL_vrx;
% H.CL_vry = CL_vry;
% H.CL_vrz = CL_vrz;


