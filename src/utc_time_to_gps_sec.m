%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [t] = utc_time_to_gps_sec(varargin)
% [t] = utc_time_to_gps_sec(date_vector)
% [t] = utc_time_to_gps_sec(year,month,day,hour,minute,second)
% [t] = utc_time_to_gps_sec(year,month,day) --> time is 00:00:00

if nargin == 1
    year = varargin{1}(1);
    month = varargin{1}(2);
    day = varargin{1}(3);
    hour = varargin{1}(4);
    minute = varargin{1}(5);
    second = varargin{1}(6);

elseif nargin == 3
    year = varargin{1};
    month = varargin{2};
    day = varargin{3};
    hour = 0;
    minute = 0;
    second = 0;

elseif nargin == 6
    year = varargin{1};
    month = varargin{2};
    day = varargin{3};
    hour = varargin{4};
    minute = varargin{5};
    second = varargin{6};

else
    error('Incorrect number of input arguments.')
end

% t_GPS = 0.0 is at 6 Jan 1980, 00:00
%
% in words:
%
% GPS time was zero at 0h 6-Jan-1980 UTC and since it is
% not perturbed by leap seconds GPS is now ahead of UTC by 16 seconds. 

% % table of leap seconds since 6 Jan 1980:
% leap_sec       = [  -1   -1   -1   -1   -1   -1   -1   -1   -1   -1   -1   -1   -1   -1   -1   -1   -1   -1];
% leap_sec_year  = [1981 1982 1983 1985 1988 1990 1991 1992 1993 1994 1996 1997 1999 2006 2009 2012 2015 2017];   
% leap_sec_month = [   7    7    7    7    1    1    1    7    7    7    1    7    1    1    1    7    7    1];

% the following creates the variables "leap_sec", "leap_sec_year" and "leap_sec_month" from the leap second table
leap_sec_table = leap_seconds();
leap_sec = reshape(leap_sec_table(:,2:3)',2*size(leap_sec_table,1),1);
leap_jd = reshape(leap_sec_table(:,4:5)',2*size(leap_sec_table,1),1);
leap_jd(leap_sec==0) = [];
leap_sec(leap_sec==0) = [];
leap_sec(leap_jd < 2444244.5) = []; % JD = 2444244.5 <--> 6 Jan 1980
leap_jd(leap_jd < 2444244.5) = [];
leap_sec = -leap_sec';
[leap_sec_year,leap_sec_month] = modified_julian_date_to_calendar_date(julian_date_to_modified_julian_date(leap_jd' + 1));


% sum of leap seconds since 6 Jan 1980:
leap_sec_offset = 0;
for k = 1:length(leap_sec)
    if year > leap_sec_year(k)
        leap_sec_offset = leap_sec_offset - leap_sec(k);
    end
    if year == leap_sec_year(k)
        if month >= leap_sec_month(k)
            leap_sec_offset = leap_sec_offset - leap_sec(k);
        end
    end
end

% year to seconds
t = (year - 1980) * 365 * 24 * 60 * 60;

% take away 1-5 Jan
t = t - 5 * 24 * 60 * 60;

% leap days since 1980 to seconds
counter = 0;
for y = 1980:year-1
    if mod(y,400) == 0 || ( mod(y,4) == 0 && mod(y,100) ~= 0 )
        counter = counter + 1;
    end
end

% is this year a leap year and is it already past the leap day?
if mod(year,400) == 0 || ( mod(year,4) == 0 && mod(year,100) ~= 0 )
    if month > 2
        counter = counter + 1;
    end
end
t = t + counter * 24 * 60 * 60;

% month to seconds
cm = cumsum([0 31 28 31 30 31 30 31 31 30 31 30]);
t = t + cm(month) * 24 * 60 * 60;

% day to seconds
t = t + (day-1) * 24 * 60 * 60;

% hour to seconds
t = t + hour * 60 * 60;

% minute to seconds
t = t + minute * 60;

% minute to seconds
t = t + second;

% add leap seconds since since 6 Jan 1980
t = t + leap_sec_offset;
