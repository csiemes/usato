%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [R_GCRS_to_ITRS] = iers2010_gcrs2itrs_matrix(EOP,UTC_JD)
% [R_GCRS_to_ITRS] = iers2010_gcrs2itrs_matrix(EOP,t_utc)
% 
% EOP ..... struct with EOP C04 time series from IERS
% t_utc ... UTC time, N x 2 matrix, Julian days
%
% It is recommended to use the J2000 method for "UTC_JD", i.e.
%   UTC_JD = [2451545.0 <remainder>]



% EOP = read_iers_eop_c04('~/Data/IERS/223_EOP_C04_14.62-NOW.IAU1980223.txt');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load reference input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         TAI          //          Civil date UTC               / Matrix 
% Modified Julian Date //  an  / mois / jour / hour / min / sec / R11 / R12 / R13 / R21 / R22 / R23 / R31 / R32 / R33 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % REF = load('~/Data/IERS/rot_mat_CPO_on_UT1mUTC_on_PM_on.txt');
% REF = load('~/Data/IERS/rot_mat_CPO_off_UT1mUTC_on_PM_on.txt');
% REF_MJD_TAI = REF(:,1);
% % generate precise time (J2000 method)
% utc2 = calendar_date_to_modified_julian_date(REF(:,2),REF(:,3),REF(:,4)) ...
%        + (2400000.5-2451545.0) + (REF(:,5)/24 + REF(:,6)/1440 + REF(:,7)/86400);
% utc1 = repmat(2451545.0,length(utc2),1);
% UTC_JD = [utc1 utc2];
% REF_UTC_Date = REF(:,2:7);
% REF_R = REF(:,[8 11 14 9 12 15 10 13 16]); % order: [R11 R21 ...]
% %
% % Notes: use of {iauXys00} is closer than {iauXy06,iauS06}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % DEBUG: single epoch
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% UTC_JD = UTC_JD(end,:);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % use 4-point Lagrange interpolation as "interp.f", skip other variations
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % warning('over-riding recommended interpolation method')
% % these are the two days in question: MJD = 58863, 58864
% ind1 = find(EOP.MJD_UTC>=58862 & EOP.MJD_UTC<= 58865);
% ind2 = find(EOP.MJD_UTC>=58863 & EOP.MJD_UTC<= 58866);
% Xp = [interpolate_lagrange(EOP.MJD_UTC(ind1),EOP.x(ind1),UTC_MJD(UTC_MJD<58864))
%       interpolate_lagrange(EOP.MJD_UTC(ind2),EOP.x(ind2),UTC_MJD(UTC_MJD>=58864))];
% Yp = [interpolate_lagrange(EOP.MJD_UTC(ind1),EOP.y(ind1),UTC_MJD(UTC_MJD<58864))
%       interpolate_lagrange(EOP.MJD_UTC(ind2),EOP.y(ind2),UTC_MJD(UTC_MJD>=58864))];
% UT1_minus_UTC = [interpolate_lagrange(EOP.MJD_UTC(ind1),EOP.UT1_minus_UTC(ind1),UTC_MJD(UTC_MJD<58864))
%                  interpolate_lagrange(EOP.MJD_UTC(ind2),EOP.UT1_minus_UTC(ind2),UTC_MJD(UTC_MJD>=58864))];
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interolate the EOP time series following the recommendation of the
% IERS conventions 2010, Section 5.5.1, pages 49-50
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate MJD from JD assumming J2000 method
UTC_MJD = (UTC_JD(:,1)-2451545.0) + (UTC_JD(:,2)-(2400000.5-2451545.0));

% interpolate EOP parameters
[Xp,Yp,UT1_minus_UTC] = iers_interp(EOP.MJD_UTC,EOP.x,EOP.y,EOP.UT1_minus_UTC,UTC_MJD);

% arcseconds --> radians
Xp = Xp/3600*pi/180;
Yp = Yp/3600*pi/180;

% time conversion needed for rotation matrices
UT1_JD = interface_Utcut1(UTC_JD,UT1_minus_UTC);
TAI_JD = interface_Utctai(UTC_JD);
TT_JD = interface_Taitt(TAI_JD);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IERS conventions 2010, Section 5.10, page 71: Method 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For the given TT, call the SOFA routine XY06 to obtain the IAU 2006/2000A
% X; Y from series (see Section 5.5.4) ...
XY = interface_Xy06(TT_JD);

% ... and then the routine S06 to obtain s.
S = interface_S06(TT_JD,XY);

% Any CIP corrections X;Y can now be applied, ...
% <to be implemented here>

% ... and the corrected X; Y; s can be used to call the routine C2IXYS,
% giving the GCRS-to-CIRS matrix.
R_GCRS_to_CIRS = interface_C2ixys(XY,S); % = transpose(Q(t))


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG: R_GCRS_to_CIRS --> okay
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X = XY(1);
% Y = XY(2);
% a = 1/2 + 1/8 * (X^2 + Y^2);
% H1 = [1-a*X^2  -a*X*Y   X
%       -a*X*Y   1-a*Y^2  Y
%       -X       -Y       1-a*(X^2+Y^2)]
% H2 = [ cos(S) sin(S) 0
%       -sin(S) cos(S) 0
%        0      0      1]
% approx_R_CIRS_to_GCRS = H1*H2;
% approx_R_GCRS_to_CIRS = approx_R_CIRS_to_GCRS'
% reshape(R_GCRS_to_CIRS,3,3)
% reshape(R_GCRS_to_CIRS,3,3) - approx_R_GCRS_to_CIRS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Next call the routine ERA00 to obtain the ERA corresponding to the
% current UT1, ...
ERA = interface_Era00(UT1_JD); % related to R(t)

% ... and apply it as an R3 rotation using the routine RZ, to form the
% CIRS-to-TIRS matrix. <correction: it is "GCRS-to-TIRS">
R_GCRS_to_TIRS = interface_Rz(ERA,R_GCRS_to_CIRS); % = transpose(Q(t)*R(t))


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG: R_GCRS_to_TIRS --> okay
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% approx_R_TIRS_to_CIRS = [ cos(-ERA) sin(-ERA) 0
%                          -sin(-ERA) cos(-ERA) 0
%                           0         0         1]
% approx_R_CIRS_to_TIRS = approx_R_TIRS_to_CIRS';
% approx_R_TIRS_to_GCRS = approx_R_CIRS_to_GCRS * approx_R_TIRS_to_CIRS % = Q(t) * R(t)
% approx_R_GCRS_to_TIRS = approx_R_TIRS_to_GCRS'
% reshape(R_GCRS_to_TIRS,3,3)
% reshape(R_GCRS_to_TIRS,3,3)-approx_R_GCRS_to_TIRS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Given xp; yp, and obtaining s0 by calling the routine SP00, ...
SP = interface_Sp00(TT_JD);

% ... the polar motion matrix (i.e. TIRS-to-ITRS) is then produced by the
% routine POM00.
R_TIRS_to_ITRS = interface_Pom00([Xp,Yp],SP); % = transpose(W(t))


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG: R_TIRS_to_ITRS --> okay
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% approx_R_ITRS_to_TIRS = [1  -SP -Xp
%                          SP  1   Yp
%                          Xp  -Yp 1 ]
% approx_R_TIRS_to_ITRS = approx_R_ITRS_to_TIRS'
% reshape(R_TIRS_to_ITRS,3,3)
% reshape(R_TIRS_to_ITRS,3,3)-approx_R_TIRS_to_ITRS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% The product of the two matrices (GCRS-to-TIRS and TIRS-to-ITRS), obtained
% by calling the routine RXR, is the GCRS-to-ITRS matrix, ...
R_GCRS_to_ITRS = interface_Rxr(R_TIRS_to_ITRS,R_GCRS_to_TIRS);

% .... which can be inverted by calling the routine TR to give the final
% result.
R_ITRS_to_GCRS = interface_Tr(R_GCRS_to_ITRS);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG: R_GCRS_to_ITRS --> okay
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% approx_R_ITRS_to_GCRS = approx_R_CIRS_to_GCRS * approx_R_TIRS_to_CIRS * approx_R_ITRS_to_TIRS
% reshape(R_ITRS_to_GCRS,3,3)
% reshape(R_ITRS_to_GCRS,3,3)-approx_R_ITRS_to_GCRS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG: R_GCRS_to_ITRS --> not okay
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % note that reference matrix is provided with 13 digits
% R_ref = reshape(REF_R(end,:),3,3)
% R_test = reshape(R_ITRS_to_GCRS(end,:),3,3)
% % R_ref'*R_ref
% % R_test'*R_test
% R_diff = R_ref - R_test
% diag(diag(R_diff))
% R_diff-diag(diag(R_diff))
% Eye_check = R_ref*R_test' - eye(3)
% % try to estimate the timing error:
% % Earth rotation rate is ~ 2*pi/86400 rad/s
% wz = 2*pi/86400; % wz*dt = Eye_check(1,2) --> Eye_check(1,2)/wz = dt
% dt = Eye_check(1,2)/wz
% 
% plot(sum(UTC_JD-UTC_JD(1,:),2),REF_R-R_ITRS_to_GCRS)
% xlabel('Days')
% 
% Eye_check = rot_mat_times_rot_mat_vec(REF_R,R_ITRS_to_GCRS(:,[1 4 7 2 5 8 3 6 9]));
% Eye_check(:,[1 5 9]) = Eye_check(:,[1 5 9]) - ones(size(Eye_check,1),3);
% 
% figure
% plot(sum(UTC_JD-UTC_JD(1,:),2),Eye_check,'LineWidth',2)
% xlabel('Days')
% legend('11','21','31','12','22','32','13','23','33')
% 
% EYE = zeros(size(UTC_JD,1),9);EYE(:,[1 5 9]) = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IERS conventions 2010, Section 5.10, page 71: Method 2 (cross-checking)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % Given TT, the IAU 2006/2000A nutation components dpis, deps are obtained by
% % calling the SOFA routine NUT06A.
% [dpsi,deps] = interface_Nut06a(TT_JD);
% 
% % Any corrections ddpis, ddeps can now be applied.
% % <to be implemented here>
% 
% % Next, the GCRS-to-true matrix is obtained using the routine PN06 (which employs
% % the 4-rotation Fukushima-Williams method described in Section 5.3.4, final paragraph).
% % [depsa,rb,rp,rbp,rn,rbpn] = interface_Pn06(TT_JD,dpsi,deps);
% [~,~,~,~,~,R_GCRS_to_true] = interface_Pn06(TT_JD,dpsi,deps);
% 
% % The classical GCRS-to-true matrix can also be generated by combining
% % separate frame bias, precession and nutation matrices. The SOFA routine
% % BI00 can be called to obtain the frame bias components, the routine P06E
% % to obtain various precession angles, and the routine NUM06A to generate
% % the nutation matrix. The product N x P x B is formed by using the
% % routine RXR.
% % <no need to do this when iauPn06 does this for us>
% 
% % Next call the routine GST06 to obtain the GST corresponding to the current UT1,
% GST = interface_Gst06(UT1_JD,TT_JD,R_GCRS_to_true);
% 
% % and apply it as an R3 rotation using the routine RZ to form the true matrix-to-TIRS.
% R_true_to_TIRS = interface_Rz(GST,R_GCRS_to_true); 
% 
% % Given xp; yp, and obtaining s0 with the routine SP00,
% SP = interface_Sp00(TT_JD);
% 
% % the polar motion matrix (i.e. TIRS-to-ITRS) is then obtained using the routine POM00.
% R_TIRS_to_ITRS = interface_Pom00([Xp,Yp],SP); % = transpose(W(t))
% 
% % The product of the two matrices (GCRS-to-TIRS and TIRS-to-ITRS),
% % obtained by calling the routine RXR, is the GCRS-to-ITRS matrix,
% R_GCRS_to_ITRS_v2 = interface_Rxr(R_TIRS_to_ITRS,R_true_to_TIRS);
% 
% % which can be inverted by calling the routine TR to give the final result.
% R_ITRS_to_GCRS_v2 = interface_Tr(R_GCRS_to_ITRS_v2);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG: R_GCRS_to_ITRS --> okay
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Eye_check = rot_mat_times_rot_mat_vec(REF_R,R_ITRS_to_GCRS_v2(:,[1 4 7 2 5 8 3 6 9]));
% Eye_check(:,[1 5 9]) = Eye_check(:,[1 5 9]) - ones(size(Eye_check,1),3);
% 
% figure
% plot(sum(UTC_JD-UTC_JD(1,:),2),Eye_check,'LineWidth',2)
% xlabel('Days')
% legend('11','21','31','12','22','32','13','23','33')
% 
% figure
% plot(R_ITRS_to_GCRS_v2 - R_ITRS_to_GCRS,'LineWidth',2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






