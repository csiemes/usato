%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [R_irf2efrf] = rot_mat_IRF_to_EFRF_approx(gps_sec)
% [R_irf2efrf] = rot_mat_IRF_to_EFRF_approx(gps_sec)

% A few note on "approx"
% (1) We should enter UT1 time and not GPS time.
%     (the difference is zero in 1980 and 16 seconds in 2012)
% (2) Motion of the celestial pole in the celestial systen is ignored.
% (3) Polar motion in the terrestrial system is ignored.
%
% see IERS conventions 2010, pages 43-52

% [Y,M,D,h,m,s] = gps_sec_to_utc_time(gps_sec);
% D = D + h/24 + m/24/60 + s/24/60/60;
% MJD = calendar_date_to_modified_julian_date(Y,M,D);
% JD = modified_julian_date_to_julian_date(MJD);

JD = gps_sec_to_julian_date(gps_sec);

theta = earth_rotation_angle(JD);

if size(JD,1) == 1 && size(JD,2) == 1
    R_irf2efrf = [ cos(theta) sin(theta) 0
                  -sin(theta) cos(theta) 0
                       0          0      1];
elseif size(JD,1) > 1 && size(JD,2) == 1
    R_irf2efrf = [ cos(theta) ... 
                  -sin(theta) ...
                   zeros(size(theta)) ... 
                   sin(theta) ...
                   cos(theta) ...
                   zeros(size(theta)) ... 
                   zeros(size(theta)) ...
                   zeros(size(theta)) ...
                   ones(size(theta))];
elseif size(JD,1) == 1 && size(JD,2) > 1
    R_irf2efrf = [ cos(theta) 
                  -sin(theta) 
                   zeros(size(theta)) 
                   sin(theta)
                   cos(theta) 
                   zeros(size(theta))
                   zeros(size(theta))
                   zeros(size(theta))
                   ones(size(theta))];
else
    error('size(JD,1) > 1 and size(JD,2) > 1')
end
               