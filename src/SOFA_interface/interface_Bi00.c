#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *dpsibi;  // Frame bias correction: longitude, N x 1 matrix, radians
    double *depsbi;  // Frame bias correction: obliquity, N x 1 matrix, radians
    double *dra;     // ICRS RA of the J2000.0 mean equinox (offset in right ascension), N x 1 matrix, radians
    
    // check for proper number of arguments
    if(nrhs!=0)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauBi00:invalidNumInputs","No inputs.");
    if(nlhs!=3)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauBi00:invalidNumOutputs","3 outputs required.");
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)1,(mwSize)1,mxREAL);
    plhs[1] = mxCreateDoubleMatrix((mwSize)1,(mwSize)1,mxREAL);
    plhs[2] = mxCreateDoubleMatrix((mwSize)1,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    dpsibi = mxGetPr(plhs[0]);
    depsbi = mxGetPr(plhs[1]);
    dra    = mxGetPr(plhs[2]);

    // Call Bi00
    iauBi00( &dpsibi[0], &depsbi[0], &dra[0] );
}
