#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *xy;     // CIP X,Y coordinates, N x 2 matrix, components of unit vector
    double *s;      // CIO locator s, N x 1 matrix, radians
    double *rc2i;   // Celestial−to−intermediate matrix, N x 9 matrix, rotation matrix
    
    // 1) The Celestial Intermediate Pole coordinates are the x,y
    //    components of the unit vector in the Geocentric Celestial
    //    Reference System.
    // 2) The CIO locator s (in radians) positions the Celestial
    //    Intermediate Origin on the equator of the CIP.
    // 3) The matrix rc2i is the first stage in the transformation from
    //    celestial to terrestrial coordinates:
    //
    //      [TRS] = RPOM * R_3(ERA) * rc2i * [CRS]
    //            = RC2T * [CRS]
    //
    //    where [CRS] is a vector in the Geocentric Celestial Reference
    //    System and [TRS] is a vector in the International Terrestrial
    //    Reference System (see IERS Conventions 2003), ERA is the Earth
    //    Rotation Angle and RPOM is the polar motion matrix.

    double rc2i_tmp[3][3];  // Single celestial−to−intermediate matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauC2ixyz:invalidNumInputs","2 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauC2ixyz:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauC2ixyz","Input argument 1 (CIP X,Y coordinates) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauC2ixyz","Input argument 2 (CIO locator s) must be a real N x 1 matrix.");
    
    //  get the inputs
    xy = mxGetPr(prhs[0]);
    s = mxGetPr(prhs[1]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    
    // get a pointer to the real data in the output matrix
    rc2i = mxGetPr(plhs[0]);

    // Calculate Celestial−to−intermediate matrix from CIP X,Y coordinates and CIO locator s
    for( n = 0; n < N; n++ )
    {
        iauC2ixys( xy[n], xy[n+N], s[n], rc2i_tmp );
        
        rc2i[n]     = rc2i_tmp[0][0];
        rc2i[n+N]   = rc2i_tmp[1][0];
        rc2i[n+2*N] = rc2i_tmp[2][0];
        rc2i[n+3*N] = rc2i_tmp[0][1];
        rc2i[n+4*N] = rc2i_tmp[1][1];
        rc2i[n+5*N] = rc2i_tmp[2][1];
        rc2i[n+6*N] = rc2i_tmp[0][2];
        rc2i[n+7*N] = rc2i_tmp[1][2];
        rc2i[n+8*N] = rc2i_tmp[2][2];
    }
}
