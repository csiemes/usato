#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *tt;    // UT1 time, N x 2 matrix, Julian date
    double *sp;    // TIO locator s’, N x 1 matrix, radians
    
    // 2) The TIO locator s’ is obtained from polar motion observations by
    //    numerical integration, and so is in essence unpredictable.
    //    However, it is dominated by a secular drift of about
    //    47 microarcseconds per century, which is the approximation
    //    evaluated by the present function.
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauSp00:invalidNumInputs","1 input required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauSp00:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauSp00","Input argument 1 (TT time) must be a real N x 2 matrix.");
    
    //  get the inputs
    tt = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    sp = mxGetPr(plhs[0]);

    // TIO locator s’ from TT time
    for( n = 0; n < N; n++ )
        sp[n] = iauSp00( tt[n], tt[n+N] );
}
