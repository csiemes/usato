#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *psi;    // Angle, N x 1 matrix, radians
    double *r_in;   // Rotation matrix, N x 9 matrix, rotation matrix
    double *r_out;  // Rotated rotation matrix, N x 9 matrix, rotation matrix
    
    // Sign convention: The matrix can be used to rotate the reference
    // frame of a vector.
    
    double r_tmp[3][3];  // Single rotation matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauRz:invalidNumInputs","2 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauRz:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauRz","Input argument 1 (Angle) must be a real N x 1 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 9 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauRz","Input argument 2 (Rotation matrix) must be a real N x 9 matrix.");
    
    //  get the inputs
    psi = mxGetPr(prhs[0]);
    r_in = mxGetPr(prhs[1]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    
    // get a pointer to the real data in the output matrix
    r_out = mxGetPr(plhs[0]);

    // Rotate rotation matrix (rotation by psi about z-axis)
    for( n = 0; n < N; n++ )
    {
        r_tmp[0][0] = r_in[n];
        r_tmp[1][0] = r_in[n+N];
        r_tmp[2][0] = r_in[n+2*N];
        r_tmp[0][1] = r_in[n+3*N];
        r_tmp[1][1] = r_in[n+4*N];
        r_tmp[2][1] = r_in[n+5*N];
        r_tmp[0][2] = r_in[n+6*N];
        r_tmp[1][2] = r_in[n+7*N];
        r_tmp[2][2] = r_in[n+8*N];
        
        iauRz( psi[n], r_tmp );
        
        r_out[n]     = r_tmp[0][0];
        r_out[n+N]   = r_tmp[1][0];
        r_out[n+2*N] = r_tmp[2][0];
        r_out[n+3*N] = r_tmp[0][1];
        r_out[n+4*N] = r_tmp[1][1];
        r_out[n+5*N] = r_tmp[2][1];
        r_out[n+6*N] = r_tmp[0][2];
        r_out[n+7*N] = r_tmp[1][2];
        r_out[n+8*N] = r_tmp[2][2];
    }
}
