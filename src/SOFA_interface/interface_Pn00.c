#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // Inputs:
    double *tt;     // TT time, N x 2 matrix, Julian date
    double *dpsi;   // Nutation compponent: longitude, N x 1 matrix, radians
    double *deps;   // Nutation compponent: obliquity, N x 1 matrix, radians
    
    // Outputs:
    double *depsa;  // Mean obliquity, N x 1 matrix, radians
    double *rb;     // Frame bias matrix, N x 9 matrix, rotation matrix
    double *rp;     // Precession matrix, N x 9 matrix, rotation matrix
    double *rbp;    // Bias−precession matrix, N x 9 matrix, rotation matrix
    double *rn;     // Nutation matrix, N x 9 matrix, rotation matrix
    double *rbpn;   // GCRS−to−true matrix, N x 9 matrix, rotation matrix
    
    // 1) The caller is responsible for providing the nutation components;
    //    they are in longitude and obliquity, in radians and are with
    //    respect to the equinox and ecliptic of date. For high−accuracy
    //    applications, free core nutation should be included as well as
    //    any other relevant corrections to the position of the CIP.
    // 2) The returned mean obliquity is consistent with the IAU 2006
    //    precession.
    // 3) The matrix rb transforms vectors from GCRS to J2000.0 mean
    //    equator and equinox by applying frame bias.
    // 4) The matrix rp transforms vectors from J2000.0 mean equator and
    //    equinox to mean equator and equinox of date by applying
    //    precession.
    // 5) The matrix rbp transforms vectors from GCRS to mean equator and
    //    equinox of date by applying frame bias then precession. It is
    //    the product rp x rb.
    // 7) The matrix rn transforms vectors from mean equator and equinox
    //    of date to true equator and equinox of date by applying the
    //    nutation (luni−solar + planetary).
    // 8) The matrix rbpn transforms vectors from GCRS to true equator and
    //    equinox of date. It is the product rn x rbp, applying frame
    //    bias, precession and nutation in that order.
    
    double rb_tmp[3][3];    // Single rotation matrix
    double rp_tmp[3][3];    // Single rotation matrix
    double rbp_tmp[3][3];   // Single rotation matrix
    double rn_tmp[3][3];    // Single rotation matrix
    double rbpn_tmp[3][3];  // Single rotation matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=3)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPn00:invalidNumInputs","3 inputs required.");
    if(nlhs!=6)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPn00:invalidNumOutputs","6 outputs required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPn00","Input argument 1 (TT time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPn00","Input argument 2 (Nutation compponent: longitude) must be a real N x 1 matrix.");
    if( !mxIsDouble(prhs[2]) || mxGetM(prhs[2]) != N || mxGetN(prhs[2]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPn00","Input argument 2 (Nutation compponent: obliquity) must be a real N x 1 matrix.");
    
    //  get the inputs
    tt = mxGetPr(prhs[0]);
    dpsi = mxGetPr(prhs[1]);
    deps = mxGetPr(prhs[2]);
    
    // create the output matrices
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    plhs[1] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    plhs[2] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    plhs[3] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    plhs[4] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    plhs[5] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    
    // get a pointer to the real data in the output matrix
    depsa = mxGetPr(plhs[0]);
    rb    = mxGetPr(plhs[1]);
    rp    = mxGetPr(plhs[2]);
    rbp   = mxGetPr(plhs[3]);
    rn    = mxGetPr(plhs[4]);
    rbpn  = mxGetPr(plhs[5]);

    // Call PN06
    for( n = 0; n < N; n++ )
    {
        iauPn00( tt[n], tt[n+N], dpsi[n], deps[n], &depsa[n], rb_tmp, rp_tmp, rbp_tmp, rn_tmp, rbpn_tmp );
        
        rb[n]     = rb_tmp[0][0];
        rb[n+N]   = rb_tmp[1][0];
        rb[n+2*N] = rb_tmp[2][0];
        rb[n+3*N] = rb_tmp[0][1];
        rb[n+4*N] = rb_tmp[1][1];
        rb[n+5*N] = rb_tmp[2][1];
        rb[n+6*N] = rb_tmp[0][2];
        rb[n+7*N] = rb_tmp[1][2];
        rb[n+8*N] = rb_tmp[2][2];
        
        rp[n]     = rp_tmp[0][0];
        rp[n+N]   = rp_tmp[1][0];
        rp[n+2*N] = rp_tmp[2][0];
        rp[n+3*N] = rp_tmp[0][1];
        rp[n+4*N] = rp_tmp[1][1];
        rp[n+5*N] = rp_tmp[2][1];
        rp[n+6*N] = rp_tmp[0][2];
        rp[n+7*N] = rp_tmp[1][2];
        rp[n+8*N] = rp_tmp[2][2];
        
        rbp[n]     = rbp_tmp[0][0];
        rbp[n+N]   = rbp_tmp[1][0];
        rbp[n+2*N] = rbp_tmp[2][0];
        rbp[n+3*N] = rbp_tmp[0][1];
        rbp[n+4*N] = rbp_tmp[1][1];
        rbp[n+5*N] = rbp_tmp[2][1];
        rbp[n+6*N] = rbp_tmp[0][2];
        rbp[n+7*N] = rbp_tmp[1][2];
        rbp[n+8*N] = rbp_tmp[2][2];
        
        rn[n]     = rn_tmp[0][0];
        rn[n+N]   = rn_tmp[1][0];
        rn[n+2*N] = rn_tmp[2][0];
        rn[n+3*N] = rn_tmp[0][1];
        rn[n+4*N] = rn_tmp[1][1];
        rn[n+5*N] = rn_tmp[2][1];
        rn[n+6*N] = rn_tmp[0][2];
        rn[n+7*N] = rn_tmp[1][2];
        rn[n+8*N] = rn_tmp[2][2];
        
        rbpn[n]     = rbpn_tmp[0][0];
        rbpn[n+N]   = rbpn_tmp[1][0];
        rbpn[n+2*N] = rbpn_tmp[2][0];
        rbpn[n+3*N] = rbpn_tmp[0][1];
        rbpn[n+4*N] = rbpn_tmp[1][1];
        rbpn[n+5*N] = rbpn_tmp[2][1];
        rbpn[n+6*N] = rbpn_tmp[0][2];
        rbpn[n+7*N] = rbpn_tmp[1][2];
        rbpn[n+8*N] = rbpn_tmp[2][2];
    }
}
