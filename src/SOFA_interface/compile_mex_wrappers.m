%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% mex -v -setup C++
% mex -v -setup C

c_files = dir('interface*.c');
inc = '-I../../external/SOFA_library/include';
lib = '-L../../external/SOFA_library/lib/ -lsofa_c';
for n = 1:length(c_files)
    cmd = ['mex -O ' c_files(n).name ' ' inc ' ' lib];
    disp(cmd)
    eval(cmd)
end
movefile('interface*.mex*','../../src');


