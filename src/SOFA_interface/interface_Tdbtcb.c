#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *tdb;    // TDB time, N x 2 matrix, Julian date
    double *tcb;    // TCB time, N x 2 matrix, Julian date
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTdbtcb:invalidNumInputs","1 input required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTdbtcb:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTdbtcb","Input argument 1 (TDB time) must be a real N x 2 matrix.");
    
    //  get the inputs
    tdb = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)2,mxREAL);
    
    // get a pointer to the real data in the output matrix
    tcb = mxGetPr(plhs[0]);

    // Calculate TT time from TAI time
    for( n = 0; n < N; n++ )
        iauTdbtcb( tdb[n], tdb[n+N], &tcb[n], &tcb[n+N] );
}
