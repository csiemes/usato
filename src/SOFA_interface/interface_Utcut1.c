#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *utc;            // UTC time, N x 2 matrix, Julian date
    double *ut1_minus_utc;  // UT1 - UTC time difference, N x 1 matrix, seconds
    double *ut1;            // UT1 time, N x 2 matrix, Julian date
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauUtcut1:invalidNumInputs","2 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauUtcut1:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauUtcut1","Input argument 1 (UTC time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauUtcut1","Input argument 2 (UT1 - UTC time difference) must be a real N x 1 matrix.");
    
    //  get the inputs
    utc = mxGetPr(prhs[0]);
    ut1_minus_utc = mxGetPr(prhs[1]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)2,mxREAL);
    
    // get a pointer to the real data in the output matrix
    ut1 = mxGetPr(plhs[0]);

    // Calculate UT1 time from UTC time and UT1 - UTC time difference
    for( n = 0; n < N; n++ )
        iauUtcut1( utc[n], utc[n+N], ut1_minus_utc[n], &ut1[n], &ut1[n+N] );
}
