#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *xy;     // CIP X,Y coordinates, N x 2 matrix, components of unit vector
    double *sp;     // TIO locator s’, N x 1 matrix, radians
    double *rpom;   // Polar−motion matrix, N x 9 matrix, rotation matrix
    
    // 1) The arguments xp and yp are the coordinates (in radians) of the
    //    Celestial Intermediate Pole with respect to the International
    //    Terrestrial Reference System (see IERS Conventions 2003),
    //    measured along the meridians to 0 and 90 deg west respectively.
    // 2) The argument sp is the TIO locator s’, in radians, which
    //    positions the Terrestrial Intermediate Origin on the equator. It
    //    is obtained from polar motion observations by numerical
    //    integration, and so is in essence unpredictable. However, it is
    //    dominated by a secular drift of about 47 microarcseconds per
    //    century, and so can be taken into account by using s’ = −47*t,
    //    where t is centuries since J2000.0. The function iauSp00
    //    implements this approximation.
    // 3) The matrix operates in the sense V(TRS) = rpom * V(CIP), meaning
    //    that it is the final rotation when computing the pointing
    //    direction to a celestial source.

    double rpom_tmp[3][3];  // Single polar motion matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPom00:invalidNumInputs","2 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPom00:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPom00","Input argument 1 (CIP X,Y coordinates) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPom00","Input argument 2 (TIO locator s’) must be a real N x 1 matrix.");
    
    //  get the inputs
    xy = mxGetPr(prhs[0]);
    sp = mxGetPr(prhs[1]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    
    // get a pointer to the real data in the output matrix
    rpom = mxGetPr(plhs[0]);

    // Calculate polar−motion matrix from CIP X,Y coordinates and TIO locator s’
    for( n = 0; n < N; n++ )
    {
        iauPom00( xy[n], xy[n+N], sp[n], rpom_tmp );
        
        rpom[n]     = rpom_tmp[0][0];
        rpom[n+N]   = rpom_tmp[1][0];
        rpom[n+2*N] = rpom_tmp[2][0];
        rpom[n+3*N] = rpom_tmp[0][1];
        rpom[n+4*N] = rpom_tmp[1][1];
        rpom[n+5*N] = rpom_tmp[2][1];
        rpom[n+6*N] = rpom_tmp[0][2];
        rpom[n+7*N] = rpom_tmp[1][2];
        rpom[n+8*N] = rpom_tmp[2][2];
    }
}
