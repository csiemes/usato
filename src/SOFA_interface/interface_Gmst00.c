#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // Inputs:
    double *ut1;    // UT1 time, N x 2 matrix, Julian date
    double *tt;     // TT time, N x 2 matrix, Julian date
    
    // Output:
    double *gmst;    // Greenwich mean sidereal time, N x 1 matrix, radians
    
    // Both UT1 and TT are required, UT1 to predict the Earth rotation
    // and TT to predict the effects of precession−nutation. If UT1 is
    // used for both purposes, errors of order 100 microarcseconds
    // result.

    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGmst00:invalidNumInputs","2 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGmst00:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGmst00","Input argument 1 (UT1 time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGmst00","Input argument 2 (TT time) must be a real N x 2 matrix.");
    
    //  get the inputs
    ut1  = mxGetPr(prhs[0]);
    tt   = mxGetPr(prhs[1]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    gmst = mxGetPr(plhs[0]);

    // Calculate Greenwich mean sidereal time
    for( n = 0; n < N; n++ )
    {
        gmst[n] = iauGmst00( ut1[n], ut1[n+N], tt[n], tt[n+N] );
    }
}
