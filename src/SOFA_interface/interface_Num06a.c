#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // Inputs:
    double *tt;     // TT time, N x 2 matrix, Julian date
    
    // Output:
    double *rmatn;  // Nutation matrix, N x 9 matrix, rotation matrix
    
    // The matrix operates in the sense V(true) = rmatn * V(mean), where
    // the p−vector V(true) is with respect to the true equatorial triad
    // of date and the p−vector V(mean) is with respect to the mean
    // equatorial triad of date.

    double rmatn_tmp[3][3];  // Single polar motion matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauNum06a:invalidNumInputs","1 input required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauNum06a:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauNum06a","Input argument 1 (TT time) must be a real N x 2 matrix.");
    
    //  get the inputs
    tt = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    
    // get a pointer to the real data in the output matrix
    rmatn = mxGetPr(plhs[0]);

    // Call Num06a
    for( n = 0; n < N; n++ )
    {
        iauNum06a( tt[n], tt[n+N], rmatn_tmp );
        
        rmatn[n]     = rmatn_tmp[0][0];
        rmatn[n+N]   = rmatn_tmp[1][0];
        rmatn[n+2*N] = rmatn_tmp[2][0];
        rmatn[n+3*N] = rmatn_tmp[0][1];
        rmatn[n+4*N] = rmatn_tmp[1][1];
        rmatn[n+5*N] = rmatn_tmp[2][1];
        rmatn[n+6*N] = rmatn_tmp[0][2];
        rmatn[n+7*N] = rmatn_tmp[1][2];
        rmatn[n+8*N] = rmatn_tmp[2][2];
    }
}
