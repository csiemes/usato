#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *ra;    // Rotation matrix, N x 9 matrix, rotation matrix
    double *rb;    // Rotation matrix, N x 9 matrix, rotation matrix
    double *ratb;  // Product of two rotation matrices, N x 9 matrix, rotation matrix
    
    // Calculates ratb = ra * rb
    
    double ra_tmp[3][3];  // Single rotation matrix
    double rb_tmp[3][3];  // Single rotation matrix
    double ratb_tmp[3][3];  // Single rotation matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauRxr:invalidNumInputs","2 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauRxr:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 9 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauRxr","Input argument 1 (Rotation matrix) must be a real N x 9 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 9 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauRxr","Input argument 2 (Rotation matrix) must be a real N x 9 matrix.");
    
    //  get the inputs
    ra = mxGetPr(prhs[0]);
    rb = mxGetPr(prhs[1]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    
    // get a pointer to the real data in the output matrix
    ratb = mxGetPr(plhs[0]);

    // Multiply rotation matrices
    for( n = 0; n < N; n++ )
    {
        ra_tmp[0][0] = ra[n];
        ra_tmp[1][0] = ra[n+N];
        ra_tmp[2][0] = ra[n+2*N];
        ra_tmp[0][1] = ra[n+3*N];
        ra_tmp[1][1] = ra[n+4*N];
        ra_tmp[2][1] = ra[n+5*N];
        ra_tmp[0][2] = ra[n+6*N];
        ra_tmp[1][2] = ra[n+7*N];
        ra_tmp[2][2] = ra[n+8*N];
        
        rb_tmp[0][0] = rb[n];
        rb_tmp[1][0] = rb[n+N];
        rb_tmp[2][0] = rb[n+2*N];
        rb_tmp[0][1] = rb[n+3*N];
        rb_tmp[1][1] = rb[n+4*N];
        rb_tmp[2][1] = rb[n+5*N];
        rb_tmp[0][2] = rb[n+6*N];
        rb_tmp[1][2] = rb[n+7*N];
        rb_tmp[2][2] = rb[n+8*N];
        
        iauRxr( ra_tmp, rb_tmp, ratb_tmp );
        
        ratb[n]     = ratb_tmp[0][0];
        ratb[n+N]   = ratb_tmp[1][0];
        ratb[n+2*N] = ratb_tmp[2][0];
        ratb[n+3*N] = ratb_tmp[0][1];
        ratb[n+4*N] = ratb_tmp[1][1];
        ratb[n+5*N] = ratb_tmp[2][1];
        ratb[n+6*N] = ratb_tmp[0][2];
        ratb[n+7*N] = ratb_tmp[1][2];
        ratb[n+8*N] = ratb_tmp[2][2];
    }
}
