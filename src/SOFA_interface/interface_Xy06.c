#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *tt;    // TT time, N x 2 matrix, Julian date
    double *xy;    // CIP X,Y coordinates, N x 2 matrix, components of unit vector
    
    // The X,Y coordinates are those of the unit vector towards the
    // celestial intermediate pole. They represent the combined effects
    // of frame bias, precession and nutation.
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauXy06:invalidNumInputs","1 input required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauXy06:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauXy06","Input argument 1 (TT time) must be a real N x 2 matrix.");
    
    //  get the inputs
    tt = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)2,mxREAL);
    
    // get a pointer to the real data in the output matrix
    xy = mxGetPr(plhs[0]);

    // Calculate CIP X,Y coordinates time from TT time
    for( n = 0; n < N; n++ )
        iauXy06( tt[n], tt[n+N], &xy[n], &xy[n+N] );
}
