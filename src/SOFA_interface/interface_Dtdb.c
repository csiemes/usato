#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *tdb;          // TDB time, N x 2 matrix, Julian date
    double *ut1;          // UT1 time, N x 1 matrix, fraction of one day
    double *elong;        // longitude (east positive), radians
    double *u;            // optional: distance from Earth spin axis (km)
    double *v;            // optional: distance north of equatorial plane (km)
    double *tdb_minus_tt; // optional: TDB − TT time difference (seconds)
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if( !(nrhs==2 || nrhs==5) )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauDtdb:invalidNumInputs","2 or 5 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauDtdb:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauDtdb","Input argument 1 (TDB time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauDtdb","Input argument 2 (UT1 time) must be a real N x 1 matrix.");
    
    // same check for optional input arguments
    if( nrhs == 5 )
    {
        if( !mxIsDouble(prhs[2]) || mxGetM(prhs[2]) != N || mxGetN(prhs[2]) != 1 )
            mexErrMsgIdAndTxt( "MATLAB:sofa_iauDtdb","Input argument 3 (longitude) must be a real N x 1 matrix.");
        if( !mxIsDouble(prhs[3]) || mxGetM(prhs[3]) != N || mxGetN(prhs[3]) != 1 )
            mexErrMsgIdAndTxt( "MATLAB:sofa_iauDtdb","Input argument 4 (distance from Earth spin axis) must be a real N x 1 matrix.");
        if( !mxIsDouble(prhs[4]) || mxGetM(prhs[4]) != N || mxGetN(prhs[4]) != 1 )
            mexErrMsgIdAndTxt( "MATLAB:sofa_iauDtdb","Input argument 5 (distance north of equatorial plane) must be a real N x 1 matrix.");
    }
    
    //  get the inputs
    tdb = mxGetPr(prhs[0]);
    ut1 = mxGetPr(prhs[1]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    tdb_minus_tt = mxGetPr(plhs[0]);
    
    if( nrhs == 5 )
    {
        // get the optional inputs
        elong = mxGetPr(prhs[2]);
        u = mxGetPr(prhs[3]);
        v = mxGetPr(prhs[4]);
        
        // Calculate TDB - TT time difference
        for( n = 0; n < N; n++ )
            tdb_minus_tt[n] = iauDtdb( tdb[n], tdb[n+N], ut1[n], elong[n], u[n], v[n] );
    }
    else // nhrs == 2
    {
        // Calculate TDB - TT time difference
        for( n = 0; n < N; n++ )
            tdb_minus_tt[n] = iauDtdb( tdb[n], tdb[n+N], ut1[n], 0.0, 0.0, 0.0 );
    }
}
