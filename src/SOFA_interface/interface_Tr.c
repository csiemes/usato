#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *r;     // 3 x 3 matrix, N x 9 matrix, 3 x 3 matrix
    double *rt;    // Transposed 3 x 3 matrix, N x 9 matrix, 3 x 3 matrix

    // Calculates rt = transpose(r)
    
    double r_tmp[3][3];   // Single matrix
    double rt_tmp[3][3];  // Single matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTr:invalidNumInputs","1 input required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTr:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 9 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTr","Input argument 1 (Rotation matrix) must be a real N x 9 matrix.");
    
    //  get the inputs
    r = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    
    // get a pointer to the real data in the output matrix
    rt = mxGetPr(plhs[0]);

    // Multiply rotation matrices
    for( n = 0; n < N; n++ )
    {
        r_tmp[0][0] = r[n];
        r_tmp[1][0] = r[n+N];
        r_tmp[2][0] = r[n+2*N];
        r_tmp[0][1] = r[n+3*N];
        r_tmp[1][1] = r[n+4*N];
        r_tmp[2][1] = r[n+5*N];
        r_tmp[0][2] = r[n+6*N];
        r_tmp[1][2] = r[n+7*N];
        r_tmp[2][2] = r[n+8*N];
        
        iauTr( r_tmp, rt_tmp );
        
        rt[n]     = rt_tmp[0][0];
        rt[n+N]   = rt_tmp[1][0];
        rt[n+2*N] = rt_tmp[2][0];
        rt[n+3*N] = rt_tmp[0][1];
        rt[n+4*N] = rt_tmp[1][1];
        rt[n+5*N] = rt_tmp[2][1];
        rt[n+6*N] = rt_tmp[0][2];
        rt[n+7*N] = rt_tmp[1][2];
        rt[n+8*N] = rt_tmp[2][2];
    }
}
