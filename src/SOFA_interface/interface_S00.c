#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *tt;    // TT time, N x 2 matrix, Julian date
    double *xy;    // CIP X,Y coordinates, N x 2 matrix, components of unit vector
    double *s;     // CIO locator s, N x 1 matrix, radians
    
    // 1) The X,Y coordinates are those of the unit vector towards the
    //    celestial intermediate pole. They represent the combined effects
    //    of frame bias, precession and nutation.
    // 2) The CIO locator s is the difference between the right ascensions
    //    of the same point in two systems: the two systems are the GCRS
    //    and the CIP,CIO, and the point is the ascending node of the
    //    CIP equator. The quantity s remains below 0.1 arcsecond
    //    throughout 1900−2100.
    // 3) The series used to compute s is in fact for s+XY/2, where X and Y
    //    are the x and y components of the CIP unit vector; this series
    //    is more compact than a direct series for s would be. This
    //    function requires X,Y to be supplied by the caller, who is
    //    responsible for providing values that are consistent with the
    //    supplied date.
        
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauS06:invalidNumInputs","2 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauS06:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauS06","Input argument 1 (TT time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauS06","Input argument 2 (CIP X,Y coordinates) must be a real N x 2 matrix.");
    
    //  get the inputs
    tt = mxGetPr(prhs[0]);
    xy = mxGetPr(prhs[1]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    s = mxGetPr(plhs[0]);

    // Calculate CIO locator s from TT time and CIP X,Y coordinates
    for( n = 0; n < N; n++ )
        s[n] = iauS00( tt[n], tt[n+N], xy[n], xy[n+N] );
}
