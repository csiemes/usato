#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *tt;     // TT time, N x 2 matrix, Julian date
    double *ut1;    // UT1 time, N x 2 matrix, Julian date
    double *xy;     // CIP xp,yp coordinates, N x 2 matrix, components of unit vector (radians)
    double *rc2t;   // Celestial−to−terrestrial matrix, N x 9 matrix, rotation matrix
    
    // 1) The arguments xp and yp are the coordinates (in radians) of the
    //    Celestial Intermediate Pole with respect to the International
    //    Terrestrial Reference System (see IERS Conventions 2003),
    //    measured along the meridians to 0 and 90 deg west respectively.
    // 2) The matrix rc2t transforms from celestial to terrestrial
    //    coordinates:
    //
    //      [TRS] = RPOM * R_3(ERA) * RC2I * [CRS]
    //            = rc2t * [CRS]
    //
    //    where [CRS] is a vector in the Geocentric Celestial Reference
    //    System and [TRS] is a vector in the International Terrestrial
    //    Reference System (see IERS Conventions 2003), RC2I is the
    //    celestial−to−intermediate matrix, ERA is the Earth rotation
    //    angle and RPOM is the polar motion matrix.

    double rc2t_tmp[3][3];  // Single celestial−to−terrestrial matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=3)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauC2t06a:invalidNumInputs","3 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauC2t06a:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauC2t06a","Input argument 1 (TT time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauC2t06a","Input argument 2 (UT1 time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[2]) || mxGetM(prhs[2]) != N || mxGetN(prhs[2]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauC2t06a","Input argument 3 (CIP xp,yp coordinates) must be a real N x 2 matrix.");
    
    //  get the inputs
    tt = mxGetPr(prhs[0]);
    ut1 = mxGetPr(prhs[1]);
    xy = mxGetPr(prhs[2]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    
    // get a pointer to the real data in the output matrix
    rc2t = mxGetPr(plhs[0]);

    // Calculate Celestial−to−intermediate matrix from CIP X,Y coordinates and CIO locator s
    for( n = 0; n < N; n++ )
    {
        iauC2t06a( tt[n], tt[n+N], ut1[n], ut1[n+N], xy[n], xy[n+N], rc2t_tmp );
        
        rc2t[n]     = rc2t_tmp[0][0];
        rc2t[n+N]   = rc2t_tmp[1][0];
        rc2t[n+2*N] = rc2t_tmp[2][0];
        rc2t[n+3*N] = rc2t_tmp[0][1];
        rc2t[n+4*N] = rc2t_tmp[1][1];
        rc2t[n+5*N] = rc2t_tmp[2][1];
        rc2t[n+6*N] = rc2t_tmp[0][2];
        rc2t[n+7*N] = rc2t_tmp[1][2];
        rc2t[n+8*N] = rc2t_tmp[2][2];
    }
}
