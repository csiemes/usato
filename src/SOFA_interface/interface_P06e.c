#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // Inputs:
    double *tt;    // TT time, N x 2 matrix, Julian date
    
    // Outputs:  N x 1 matrices, radians (applies to all outputs)
    double *eps0;    // epsilon_0     obliquity at J2000.0
    double *psia;    // psi_A         luni−solar precession
    double *oma;     // omega_A       inclination of equator wrt J2000.0 ecliptic
    double *bpa;     // P_A           ecliptic pole x, J2000.0 ecliptic triad
    double *bqa;     // Q_A           ecliptic pole −y, J2000.0 ecliptic triad
    double *pia;     // pi_A          angle between moving and J2000.0 ecliptics
    double *bpia;    // Pi_A          longitude of ascending node of the ecliptic
    double *epsa;    // epsilon_A     obliquity of the ecliptic
    double *chia;    // chi_A         planetary precession
    double *za;      // z_A           equatorial precession: −3rd 323 Euler angle
    double *zetaa;   // zeta_A        equatorial precession: −1st 323 Euler angle
    double *thetaa;  // theta_A       equatorial precession: 2nd 323 Euler angle
    double *pa;      // p_A           general precession
    double *gam;     // gamma_J2000   J2000.0 RA difference of ecliptic poles
    double *phi;     // phi_J2000     J2000.0 codeclination of ecliptic pole
    double *psi;     // psi_J2000     longitude difference of equator poles, J2000.0
    
    // This function returns the set of equinox based angles for the
    // Capitaine et al. "P03" precession theory, adopted by the IAU in
    // 2006. The angles are set out in Table 1 of Hilton et al. (2006).
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauP06e:invalidNumInputs","1 input required.");
    if(nlhs!=16)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauP06e:invalidNumOutputs","16 outputs required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauP06e","Input argument 1 (TT time) must be a real N x 2 matrix.");
    
    //  get the inputs
    tt = mxGetPr(prhs[0]);
    
    // create the output matrices
    for( n = 0; n < 16; n++ )
        plhs[n] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    eps0   = mxGetPr(plhs[0]);
    psia   = mxGetPr(plhs[1]);
    oma    = mxGetPr(plhs[2]);
    bpa    = mxGetPr(plhs[3]);
    bqa    = mxGetPr(plhs[4]);
    pia    = mxGetPr(plhs[5]);
    bpia   = mxGetPr(plhs[6]);
    epsa   = mxGetPr(plhs[7]);
    chia   = mxGetPr(plhs[8]);
    za     = mxGetPr(plhs[9]);
    zetaa  = mxGetPr(plhs[10]);
    thetaa = mxGetPr(plhs[11]);
    pa     = mxGetPr(plhs[12]);
    gam    = mxGetPr(plhs[13]);
    phi    = mxGetPr(plhs[14]);
    psi    = mxGetPr(plhs[15]);

    // Call P06e
    for( n = 0; n < N; n++ )
        iauP06e( tt[n], tt[n+N], &eps0[n], &psia[n], &oma[n], &bpa[n], &bqa[n], &pia[n], &bpia[n], &epsa[n], &chia[n], &za[n], &zetaa[n], &thetaa[n], &pa[n], &gam[n], &phi[n], &psi[n]);
}
