#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // Inputs:
    double *tt;     // TT time, N x 2 matrix, Julian date
    double *epsa;   // Mean obliquity, N x 1 matrix, radians
    double *dpsi;   // Nutation in longitude, N x 1 matrix, radians
    
    // Output:
    double *ee;    // Equation of the equinoxes, N x 1 matrix, radians
    
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=3)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauEe00:invalidNumInputs","3 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauEe00:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real-valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGmst00","Input argument 1 (TT time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGmst00","Input argument 2 (depsa = mean obliquity) must be a real N x 1 matrix.");
    if( !mxIsDouble(prhs[2]) || mxGetM(prhs[2]) != N || mxGetN(prhs[2]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGmst00","Input argument 3 (dpsi = nutation in longitude) must be a real N x 1 matrix.");
    
    //  get the inputs
    tt   = mxGetPr(prhs[0]);
    epsa = mxGetPr(prhs[1]);
    dpsi = mxGetPr(prhs[2]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    ee = mxGetPr(plhs[0]);

    // Calculate Greenwich mean sidereal time
    for( n = 0; n < N; n++ )
    {
        ee[n] = iauEe00( tt[n], tt[n+N], epsa[n], dpsi[n] );
    }
}
