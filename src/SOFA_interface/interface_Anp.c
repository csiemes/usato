#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // Inputs:
    double *a;     // Angle, N x 1 matrix, radians
    
    // Output:
    double *an;    // Normalized angle (0<=an<2pi), N x 1 matrix, radians
    
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauAnp:invalidNumInputs","1 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauAnp:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real-valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauAnp","Input argument 1 (angle) must be a real N x 1 matrix.");

    
    //  get the inputs
    a = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    an = mxGetPr(plhs[0]);

    // Calculate Greenwich mean sidereal time
    for( n = 0; n < N; n++ )
    {
        an[n] = iauAnp( a[n] );
    }
}
