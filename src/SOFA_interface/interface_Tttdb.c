#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *tt;            // TT time, N x 2 matrix, Julian date
    double *tdb_minus_tt;  // TDB - TT time difference, N x 1 matrix, seconds
    double *tdb;           // TDB time, N x 2 matrix, Julian date
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTttdb:invalidNumInputs","2 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTttdb:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTttdb","Input argument 1 (TT time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauTttdb","Input argument 2 (TDB - TT time difference) must be a real N x 1 matrix.");
    
    //  get the inputs
    tt = mxGetPr(prhs[0]);
    tdb_minus_tt = mxGetPr(prhs[1]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)2,mxREAL);
    
    // get a pointer to the real data in the output matrix
    tdb = mxGetPr(plhs[0]);

    // Calculate TDB time from TT time and TDB - TT time difference
    for( n = 0; n < N; n++ )
        iauTttdb( tt[n], tt[n+N], tdb_minus_tt[n], &tdb[n], &tdb[n+N] );
}
