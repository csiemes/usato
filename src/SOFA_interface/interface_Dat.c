#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *dv;      // UTC date vector (year, month, day, hour, minute, second), N x 6 matrix
                     // (the first 5 columns must be integers --> no checks are performed!)
    double *deltat;  // TAI - UTC time difference, N x 1 matrix, seconds
    
    int iy;     // integer year
    int im;     // integer month
    int id;     // integer day
    double fd;  // fraction of day
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauDat:invalidNumInputs","1 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauDat:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real-valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 6 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauDat","Input argument 1 (date vector) must be a real N x 6 matrix.");
    
    //  get the inputs
    dv = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    deltat = mxGetPr(plhs[0]);

    // Convert datevec into Julian date
    for( n = 0; n < N; n++ )
    {
        iy = (int)dv[n];
        im = (int)dv[n+N];
        id = (int)dv[n+2*N];
        fd = dv[n+3*N]/24.0 + dv[n+4*N]/1440.0 + dv[n+5*N]/86400.0;

        // calculate TAI - UTC time difference (seconds)
        iauDat( iy, im, id, fd, &deltat[n] );
    }
}
