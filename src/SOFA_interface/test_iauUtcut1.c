#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
int main(int argc, char* argv[])
{
    double utc[2];            // UTC time, N x 2 matrix, Julian date
    double ut1_minus_utc ;  // UT1 - UTC time difference, N x 1 matrix, seconds
    double ut1[2];            // UT1 time, N x 2 matrix, Julian date
    
    utc[0] = 2451545.0;
    utc[1] = 0.0;
    ut1_minus_utc = 0.1;

    iauUtcut1( utc[0], utc[1], ut1_minus_utc, &ut1[0], &ut1[1] );
    
    printf("UTC = (%f,%f)\n",utc[0],utc[1]);
    printf("UT1 - UTC = %f\n",ut1_minus_utc);
    printf("UT1 = (%f,%f)\n",ut1[0],ut1[1]);
}
