#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // Inputs:
    double *epsa;   // Mean obliquity of date, N x 1 matrix, radians
    double *dpsi;   // Nutation compponent: longitude, N x 1 matrix, radians
    double *deps;   // Nutation compponent: obliquity, N x 1 matrix, radians
    
    // Output:
    double *rmatn;  // Nutation matrix, N x 9 matrix, rotation matrix

    
    double rmatn_tmp[3][3];    // Single rotation matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=3)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauNumat:invalidNumInputs","3 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauNumat:invalidNumOutputs","1 outputs required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real-valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPn00","Input argument 1 (Mean obliquity of date) must be a real N x 1 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPn00","Input argument 2 (Nutation compponent: longitude) must be a real N x 1 matrix.");
    if( !mxIsDouble(prhs[2]) || mxGetM(prhs[2]) != N || mxGetN(prhs[2]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauPn00","Input argument 2 (Nutation compponent: obliquity) must be a real N x 1 matrix.");
    
    //  get the inputs
    epsa = mxGetPr(prhs[0]);
    dpsi = mxGetPr(prhs[1]);
    deps = mxGetPr(prhs[2]);
    
    // create the output matrices
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    
    // get a pointer to the real data in the output matrix
    rmatn = mxGetPr(plhs[0]);

    // Call iauNumat
    for( n = 0; n < N; n++ )
    {
        iauNumat( epsa[n], dpsi[n], deps[n], rmatn_tmp );
        
        rmatn[n]     = rmatn_tmp[0][0];
        rmatn[n+N]   = rmatn_tmp[1][0];
        rmatn[n+2*N] = rmatn_tmp[2][0];
        rmatn[n+3*N] = rmatn_tmp[0][1];
        rmatn[n+4*N] = rmatn_tmp[1][1];
        rmatn[n+5*N] = rmatn_tmp[2][1];
        rmatn[n+6*N] = rmatn_tmp[0][2];
        rmatn[n+7*N] = rmatn_tmp[1][2];
        rmatn[n+8*N] = rmatn_tmp[2][2];
    }
}
