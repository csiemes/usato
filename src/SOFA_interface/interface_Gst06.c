#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // Inputs:
    double *ut1;    // UT1 time, N x 2 matrix, Julian date
    double *tt;     // TT time, N x 1 matrix, Julian date
    double *rnpb;   // Nutation x Precession x Bias matrix, N x 9 matrix, rotation matrix
    
    // Output:
    double *gast;    // Greenwich apparent sidereal time, N x 1 matrix, radians
    
    // Both UT1 and TT are required, UT1 to predict the Earth rotation
    // and TT to predict the effects of precession−nutation. If UT1 is
    // used for both purposes, errors of order 100 microarcseconds
    // result.

    double rnpb_tmp[3][3];  // Single polar motion matrix
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=3)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGst06:invalidNumInputs","3 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGst06:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGst06","Input argument 1 (UT1 time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGst06","Input argument 2 (TT time) must be a real N x 2 matrix.");
    if( !mxIsDouble(prhs[2]) || mxGetM(prhs[2]) != N || mxGetN(prhs[2]) != 9 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauGst06","Input argument 3 (NPB matrix) must be a real N x 9 matrix.");
    
    //  get the inputs
    ut1  = mxGetPr(prhs[0]);
    tt   = mxGetPr(prhs[1]);
    rnpb = mxGetPr(prhs[2]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    gast = mxGetPr(plhs[0]);

    // Calculate polar−motion matrix from CIP X,Y coordinates and TIO locator s’
    for( n = 0; n < N; n++ )
    {
        rnpb_tmp[0][0] = rnpb[n];
        rnpb_tmp[1][0] = rnpb[n+N];
        rnpb_tmp[2][0] = rnpb[n+2*N];
        rnpb_tmp[0][1] = rnpb[n+3*N];
        rnpb_tmp[1][1] = rnpb[n+4*N];
        rnpb_tmp[2][1] = rnpb[n+5*N];
        rnpb_tmp[0][2] = rnpb[n+6*N];
        rnpb_tmp[1][2] = rnpb[n+7*N];
        rnpb_tmp[2][2] = rnpb[n+8*N];

        gast[n] = iauGst06( ut1[n], ut1[n+N], tt[n], tt[n+N], rnpb_tmp );
    }
}
