#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *tt;    // TT time, N x 2 matrix, Julian date
    double *dpsi;  // Nutation compponent: longitude, N x 1 matrix, radians
    double *deps;  // Nutation compponent: obliquity, N x 1 matrix, radians
    
    // 2) The nutation components in longitude and obliquity are in radians
    //    and with respect to the mean equinox and ecliptic of date,
    //    IAU 2006 precession model (Hilton et al. 2006, Capitaine et al. 2005).
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauNut06a:invalidNumInputs","1 input required.");
    if(nlhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauNut06a:invalidNumOutputs","2 outputs required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauNut06a","Input argument 1 (TT time) must be a real N x 2 matrix.");
    
    //  get the inputs
    tt = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    plhs[1] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    dpsi = mxGetPr(plhs[0]);
    deps = mxGetPr(plhs[1]);

    // Call Nut06a
    for( n = 0; n < N; n++ )
        iauNut06a( tt[n], tt[n+N], &dpsi[n], &deps[n] );
}
