#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *jd;  // Time (any time system), N x 2 matrix, Julian date
    double *dv;  // Date vector (year, month, day, hour, minute, second), N x 6 matrix
                 // (the first 5 columns must be integers --> no checks are performed!)
    
    int iy;     // integer year
    int im;     // integer month
    int id;     // integer day
    double fd;  // fraction of day
    
    double hr;  // hour
    double mn;  // minute
    double se;  // second

    size_t N;   // number of elements
    size_t n;   // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauJd2cal:invalidNumInputs","1 inputs required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauJd2cal:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauJd2cal","Input argument 1 (Julian date) must be a real N x 2 matrix.");
    
    //  get the inputs
    jd = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)6,mxREAL);
    
    // get a pointer to the real data in the output matrix
    dv = mxGetPr(plhs[0]);

    // Convert datevec into Julian date
    for( n = 0; n < N; n++ )
    {
        iauJd2cal( jd[n], jd[n+N], &iy, &im, &id, &fd );
        
        fd *= 24.0;      // convert fraction of day to hours
        hr = floor(fd);  // hours
        fd -= hr;        // rest = minutes and seconds, in hours
        
        fd *= 60.0;      // convert hours to minutes
        mn = floor(fd);  // minutes
        fd -= mn;        // rest = seconds, in minutes
        
        se = fd*60.0;    // convert minutes to seconds
        
        dv[n]     = (double)iy;
        dv[n+N]   = (double)im;
        dv[n+2*N] = (double)id;
        dv[n+3*N] = hr;
        dv[n+4*N] = mn;
        dv[n+5*N] = se;
    }
}
