#include "mex.h"
#include "math.h"    // maths functions
#include "stdio.h"   // for error messages
#include "stdlib.h"  // for malloc/free
#include "sofa.h"    // SOFA library

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *ut1;    // UT1 time, N x 2 matrix, Julian date
    double *era;    // Earth rotation angle, N x 1 matrix, radians (range 0-2pi)
    
    // The X,Y coordinates are those of the unit vector towards the
    // celestial intermediate pole. They represent the combined effects
    // of frame bias, precession and nutation.
    
    size_t N;       // number of elements
    size_t n;       // loop variables
    
    // check for proper number of arguments
    if(nrhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauEra00:invalidNumInputs","1 input required.");
    if(nlhs!=1)
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauEra00:invalidNumOutputs","1 output required.");

    // find the number of rows from the first input
    N = mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2 )
        mexErrMsgIdAndTxt( "MATLAB:sofa_iauEra00","Input argument 1 (UT1 time) must be a real N x 2 matrix.");
    
    //  get the inputs
    ut1 = mxGetPr(prhs[0]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    era = mxGetPr(plhs[0]);

    // Calculate Earth rotation angle from UT1 time
    for( n = 0; n < N; n++ )
        era[n] = iauEra00( ut1[n], ut1[n+N] );
}
