%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [b] = rot_mat_times_vec_vec(R,a)
% b = R*a where a,b,R are vectors/rotation matrices stored columnwise in
% each row, e.g. 
% size(R) = N x 9 --> R(1,:) = rot mat columnwise of first epoch
% size(a) = N x 3 --> a(1,:) = vec of first epoch

b(:,1) = R(:,1).*a(:,1) + R(:,4).*a(:,2) + R(:,7).*a(:,3);
b(:,2) = R(:,2).*a(:,1) + R(:,5).*a(:,2) + R(:,8).*a(:,3);
b(:,3) = R(:,3).*a(:,1) + R(:,6).*a(:,2) + R(:,9).*a(:,3);