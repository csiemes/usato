%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [dv] = gps2utc(varargin)
% [UTC_date_time] = gps2utc(GPS_seconds)
% [UTC_date_time] = gps2utc(GPS_date_time)
% [UTC_date_time] = gps2utc(GPS_year,GPS_month,GPS_day,GPS_hour,GPS_minute,GPS_second)
%
% GPS_seconds ..... GPS seconds, N x 1 matrix
%
% GPS_date_time ... GPS date and time vector, N x 6 matrix
%
% GPS_year ........ Element 1 of GPS date and time vector
% GPS_month ....... Element 2 ~
% GPS_day ......... Element 3 ~
% GPS_hour ........ Element 4 ~
% GPS_minute ...... Element 5 ~
% GPS_second ...... Element 6 ~
%
% UTC_date_time ... UTC date and time vector, N x 6 matrix


% convert GPS date/time to TAI Julian date (TAI = GPS + 19 seconds)
if nargin == 1
    
    % check if we have GPS seconds as input
    if size(varargin{1},2) == 1
        gps_sec = varargin{1};
        tai_jd = [repmat(2444244.5,length(gps_sec),1),(gps_sec+19)/86400];
        
    % check if we have GPS date vector as input
    elseif size(varargin{1},2) == 6
        dv = varargin{1};
        tai_jd = interface_Cal2jd(dv);
        tai_jd(:,2) = tai_jd(:,2) + 19/86400;
    else
        error('input argument must have 1 or 6 columns')
    end
    
% check if we have GPS year, month, day, hour, minute, second as input
elseif nargin == 6
    dv = [varargin{1} varargin{2} varargin{3} varargin{4} varargin{5} varargin{6}];
    tai_jd = interface_Cal2jd(dv);
    tai_jd(:,2) = tai_jd(:,2) + 19/86400;
else
    error('number of input arguments must be 1 or 6')
end

% convert TAI Julian date to UTC Julian date
utc_jd = interface_Taiutc(tai_jd);

% convert UTC Julian date to date vector
dv = interface_Jd2cal(utc_jd);



