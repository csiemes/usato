%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Sa] = pos2acc_noise(r,rot_lorf2efrf,Sr,GM)
% r ... positions, defined in EFEC reference frame (ITRF), N x 3, m
% rot_lorf2efrf ... rotation from local orbit reference frame (LORF) to Earth-fixed, Earth-centered (EFEC) frame
% Sr ... position covariance matrix, defined in local orbit reference frame (LORF), N x 9, m2
% Sa ... acceleration covariance matrix, defined in ITRF, N x 9, m2/s4
% GM ... gravitational parameter, m^3/(kg * s^2)


rot_efrf2lorf = rot_lorf2efrf(:,[1 4 7 2 5 8 3 6 9]);

% transform the position covariance matrix from LORF to ITRF
Sr = rot_mat_times_rot_mat_vec(rot_lorf2efrf,Sr);
Sr = rot_mat_times_rot_mat_vec(Sr,rot_efrf2lorf);

% partial of gravity vector with respect to position
r2 = sum(r.^2,2);

h = -GM./sqrt(r2).^5;

H = [h.*(r2-3*r(:,1).^2) ...
     h.*(-3*r(:,1).*r(:,2)) ...
     h.*(-3*r(:,1).*r(:,3)) ...
     h.*(-3*r(:,1).*r(:,2)) ...
     h.*(r2-3*r(:,2).^2) ...
     h.*(-3*r(:,2).*r(:,3)) ...
     h.*(-3*r(:,1).*r(:,3)) ...
     h.*(-3*r(:,2).*r(:,3)) ...
     h.*(r2-3*r(:,3).^2)];
  
% error propagation of position noise to accelertion noise (via evaluation of the gravity vector)
Sa = rot_mat_times_rot_mat_vec(H,Sr);
Sa = rot_mat_times_rot_mat_vec(Sa,H(:,[1 4 7 2 5 8 3 6 9]));
