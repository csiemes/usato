%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Ca,CD,CL] = sentman(v_rel,nrl,A,rho,m,Ta,alpha,Tw)
% [Ca,CD,CL] = sentman(v_rel,nrl,A,rho,m,Ta,alpha,Tw)
%
% v_rel ... relative velocity, satellite frame, (m/s), dimension N x 3
% nrl ... outward normal vectors of panels, satellite frame, (-), dimension K x 3
% A ... areas of panels, (m2), dimension K x 1
% rho ... mass density per atmospheric constituent, (kg/m3), dimension N x J
% m ... molar mass of atmospheric constituents, (g/mol), dimension J x 1
% Ta ... atmospheric temperature, (K), dimension N x 1
% Tw ... wall temperature, (K), dimension N x K
% alpha ... energy accommodation coefficient, (-), 1 x 1 
% 
% N ... number of epochs
% K ... number of panels
% J ... number of atmospheric constituents
%
% Ca = aerodynamic coefficient, dimension 3 x 1
% CD = drag coefficient, dimension 1 x 1
%
% Reference:
% Eelco Doornbos, Dissertion, 2011, pages 67-69


% number of panels
K = length(A);

% number of epochs
N = size(v_rel,1);

% number of atmospheric constituents
J = length(m);



% Gas constant, (J K−1 mol−1)
R = 8.31446261815324;

% most probably speed of molecules, (m/s), dimension N x J
vmp = sqrt(2*R*Ta*(1./m') * 1000); % factor 1000 converts g/mol to kg/mol

% speed ratio, (-), dimension N x J
S = repmat(sqrt(sum(v_rel.^2,2)),1,J)./vmp;

% drag direction, unit vector, dimension N x 3
uD = v_rel ./ repmat(sqrt(sum(v_rel.^2,2)),1,3);

% drag and lift coefficients, dimension N x 3
CD = zeros(N,3);
CL = zeros(N,3);

% % DEBUG
% DEBUG.uhd = uD';
% DEBUG.uhl = zeros(K,3);
% DEBUG.S = S';
% DEBUG.gamma = zeros(K,J);
% DEBUG.l = zeros(K,1);
% DEBUG.r = zeros(K,J);
% DEBUG.CD = zeros(K,J);
% DEBUG.CL = zeros(K,J);

% loop over panels
for k = 1:K
    
    % negative dot product of drag direction and panel normals, dimension N x 1
    gamma = -sum(uD.*repmat(nrl(k,:),N,1),2);
    
    % lift directions per panel, unit vectors, dimension N x 3 per axis
    uL = -cross(cross(uD,repmat(nrl(k,:),N,1)),uD);
    
    % when the relative velocity and the normal of the panel are parallel,
    % then the lift direction is not defined and there is no lift
    uL_norm = sqrt(sum(uL.^2,2));
    if any(uL_norm>1e-10)
        uL(uL_norm>1e-10,:) = uL(uL_norm>1e-10,:) ./ repmat(uL_norm(uL_norm>1e-10),1,3);
    end
    
    % negative dot product of lift direction and panel normals, dimension N x 1
    l = -sum(uL.*repmat(nrl(k,:),N,1),2);
    
    % Calculate CD and CL per constituent, dimension N x J
    Gamma = repmat(gamma,1,J);
    
    % ratio of reflected and incident velocities, dimension N x J
    % (note that in Eelco's diss the 1/(m*1000) is missing)
    VV = sqrt(0.5 * (1 + alpha * (4*R*repmat(Tw(:,k),1,J) ./ (repmat(m'/1000,N,1) .* repmat(sum(v_rel.^2,2),1,J)) - 1)));
    
    G = 0.5./S.^2;
    Q = 1 + G;
    P = exp(-Gamma.^2 .* S.^2) ./ S; % dimension N x J
    Z = 1 + erf(Gamma .* S); % dimension N x J
    
    L = repmat(l,1,J);
    
    H = VV .* (sqrt(pi)*Gamma.*Z + P) / 2; % dimension N x J
    
    CDk = P/sqrt(pi) + Gamma.*Q.*Z + Gamma.*H; % dimension N x J
    CLk = L.*G.*Z + L.*H; % dimension N x J
    
    % aerodynamic coefficient, dimension N x 3
    CD = CD + repmat(sum(rho.*CDk,2)./sum(rho,2),1,3).*uD * A(k); % Aref = 1 by definition
    CL = CL + repmat(sum(rho.*CLk,2)./sum(rho,2),1,3).*uL * A(k); % Aref = 1 by definition

    % DEBUG.uhl(k,:) = uL;
    % DEBUG.gamma(k,:) = gamma;
    % DEBUG.l(k) = l;
    % DEBUG.r(k,:) = VV;
    % DEBUG.CD(k,:) = CDk * A(k);
    % DEBUG.CL(k,:) = CLk * A(k);
end

Ca = CD + CL;

