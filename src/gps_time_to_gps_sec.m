%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [t] = gps_time_to_gps_sec(varargin)
% [t] = gps_time_to_gps_sec(date_vector)
% [t] = gps_time_to_gps_sec(year,month,day,hour,minute,second)
% [t] = gps_time_to_gps_sec(year,month,day) --> time is 00:00:00

if nargin == 1
    year = varargin{1}(1);
    month = varargin{1}(2);
    day = varargin{1}(3);
    hour = varargin{1}(4);
    minute = varargin{1}(5);
    second = varargin{1}(6);

elseif nargin == 3
    year = varargin{1};
    month = varargin{2};
    day = varargin{3};
    hour = 0;
    minute = 0;
    second = 0;

elseif nargin == 6
    year = varargin{1};
    month = varargin{2};
    day = varargin{3};
    hour = varargin{4};
    minute = varargin{5};
    second = varargin{6};

else
    error('Incorrect number of input arguments.')
end


% t_GPS = 0.0 is at 6 Jan 1980, 00:00

% year to seconds
t = (year - 1980) * 365 * 24 * 60 * 60;

% take away 1-5 Jan
t = t - 5 * 24 * 60 * 60;

% leap days since 1980 to seconds
counter = 0;
for y = 1980:year-1
    if mod(y,400) == 0 | ( mod(y,4) == 0 & mod(y,100) ~= 0 )
        counter = counter + 1;
    end
end

% is this year a leap year and is it already past the leap day?
if mod(year,400) == 0 | ( mod(year,4) == 0 & mod(year,100) ~= 0 )
    if month > 2
        counter = counter + 1;
    end
end
t = t + counter * 24 * 60 * 60;

% month to seconds
cm = cumsum([0 31 28 31 30 31 30 31 31 30 31 30]);
t = t + cm(month) * 24 * 60 * 60;

% day to seconds
t = t + (day-1) * 24 * 60 * 60;

% hour to seconds
t = t + hour * 60 * 60;

% minute to seconds
t = t + minute * 60;

% minute to seconds
t = t + second;

