%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [C] = construct_aerodynamic_covariance_matrix(AERO_COV_INPUTS)

% number of panels
P = numel(AERO_COV_INPUTS.Ap_std);

% number of athmospheric species
J = numel(AERO_COV_INPUTS.rho);

% size of covariance matrix
N = J + 2*P + 9;

C = zeros(N,N);

C(1,1) = (AERO_COV_INPUTS.Ta * AERO_COV_INPUTS.Ta_fraction)^2;
C(2:1+J,2:1+J) = diag((AERO_COV_INPUTS.rho .* AERO_COV_INPUTS.rho_fraction).^2);
C(J+2,J+2) = AERO_COV_INPUTS.alpha_E^2;
C(J+3:J+2+P,J+3:J+2+P) = diag(AERO_COV_INPUTS.Tw.^2);
C(J+3+P:J+2+2*P,J+3+P:J+2+2*P) = diag(AERO_COV_INPUTS.Ap_std.^2);
C(J+3+2*P,J+3+2*P) = AERO_COV_INPUTS.ms_std^2;
C(J+4+2*P:J+6+2*P,J+4+2*P:J+6+2*P) = diag(AERO_COV_INPUTS.aa_std.^2);
C(J+7+2*P:J+9+2*P,J+7+2*P:J+9+2*P) = diag(AERO_COV_INPUTS.vr_std.^2);
