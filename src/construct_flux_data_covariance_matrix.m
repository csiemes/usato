%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Cx2] = construct_flux_data_covariance_matrix(Ps,Pa,Pi,Ps_fraction,Pa_fraction,Pi_fraction)


% % % % % speed of light, unit = m/s
% % % % c = 299792458;
% % % % 
% % % % % convert standard deviation of fluxes from W/m2 to N/m2
% % % % Ps_fraction = Ps_fraction/c;
% % % % Pa_fraction = Pa_fraction/c;
% % % % Pi_fraction = Pi_fraction/c;


% x = [T(n) Tb(n) cav cdv csv cai cdi csi A k C m Cb Pb Pv Pi]
%
% split into
%    xa = [T(n) Tb(n) cav cdv csv cai cdi csi A k C m Cb P]
%    xb = [Pv Pi]

Na = numel(Pa_fraction);
Ni = numel(Pi_fraction);

Cx2 = zeros(1+Na+Ni,1);

% store only diagonal elements (std is a fraction of the flux signal)
Cx2(1,1) = (Ps*Ps_fraction)^2;
Cx2(2:Na+1) = (Pa.*Pa_fraction).^2;
Cx2(Na+2:Na+Ni+1) = (Pi.*Pi_fraction).^2;

