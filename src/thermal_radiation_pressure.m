%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [a,T,Tb] = thermal_radiation_pressure(dt,n,A,cav,cai,T,Tb,k,C,Cb,Pb,m,Ps,Pa,Pi,es,ea,ei)
% [a,T,Tb] = thermal_radiation_pressure(dt,n,A,cav,cai,T,Tb,k,C,Cb,Pb,m,Ps,Pa,Pi,es,ea,ei)
%
% P .... number of panels
% Sa ... number of radiation sources due to albedo
% Si ... number of radiation sources due to Earth infrared radiation 
%
% dt ... time step from this epoch to the next epoch, unit = s, 1 x 1
% n .... panel normals (outward), P x 1
% A .... panel areas, unit = m2, P x 1
% cav .. absorption coefficients for visible light, P x 1
% cai .. absorption coefficients for infrared light, P x 1
% T .... panel temperatures at this epoch, unit = K, P x 1
% Tb ... satellite body temperature at this epoch, unit = K, 1 x 1
% k .... heat conductivity between panels and satellite body, unit = W/K, P x 1
% C .... heat capacity of the panels, unit = J/K, P x 1
% Cb ... heat capacity of the satilte body, unit = J/K, 1 x 1
% Pb ... heat generation of the satellite body, unit = W, 1 x 1
% m .... satellite mass, unit = kg, 1 x 1
% Ps ... heat flux from the sun, unit = W/m2, 1 x 1
% Pa ... heat flux from albedo, unit = W/m2, Sa x 1
% Pi ... heat flux from Earth infrared radiation, unit = W/m2, Si x 1
% es ... direction from the satellite to the sun (unit vector), satellite frame
% ea ... direction from the satellite to the albedo surface element (unit vectors), satellite frame
% ei ... direction from the satellite to the infrared surface element (unit vectors), satellite frame
%
% a .... acceleration due to satellite thermal emission at this epoch
% T .... panel temperatures at next epoch, unit = K, P x 1
% Tb ... satellite body temperature at next epoch, unit = K, 1 x 1


%-----------------------------------
% calculate acceleration
%-----------------------------------

% Stefan Botzmann constant, unit = W/m2/K4
sigma = 5.670374419e-8;

% speed of light, unit = m/s
c = 299792458;

% calculate radiation pressure acceleration
a = -2/3*sigma * (n' * (A.*cai.*T.^4)) / m / c;


%-----------------------------------
% update temperatures
%-----------------------------------

% calculate cosine of angles between the panels normals and the direction
% to the radiation source, P x S
cs = n*es';
ca = n*ea';
ci = n*ei';

% panels do not absorb heat when the source illuminates the  backside
cs(cs<0) = 0;
ca(ca<0) = 0;
ci(ci<0) = 0;

% absorbed heat, unit = W, P x 1
P_abs = cav.*A.*(cs*Ps + ca*Pa) + cai.*A.*(ci*Pi);

% emitted heat, unit = W, P x 1
P_emit = sigma * A.*cai.*T.^4;

% heat exchange via conduction to body, unit = W, P x 1
P_cond = k.*(T - Tb);

% net heat changes, unit = W
dP = P_abs - P_emit - P_cond;
dPb = Pb + sum(P_cond);

% temperature changes, unit = K
T = T + dP./C * dt;
Tb = Tb + dPb/Cb * dt;


