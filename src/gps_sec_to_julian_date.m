%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [jd] = gps_sec_to_julian_date(gps_sec)
% [jd] = gps_sec_to_julian_date(gps_sec)
%
% jd ... Julian date (GPS Time)
% gps_sec ... GPS second (GPS Time)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% old working code:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% mjd = zeros(size(gps_sec));
% for n = 1:length(gps_sec)
%     [y,m,d,hh,mm,ss] = gps_sec_to_utc_time(gps_sec(n));
%     mjd(n) = utc_time_to_modified_julian_day(y,m,d,hh,mm,ss);
% end
% jd_old = modified_julian_date_to_julian_date(mjd);

% --> change function call to "function [jd_old,jd] = gps_sec_to_julian_date(gps_sec)"


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% new working code:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get the table with leap seconds
[leap_sec_table] = leap_seconds();

% note: start of GPS time ("gps_sec = 0") is 1980-01-06 00:00:00 UTC --> JD = 2444244.5
% now we have already JD, except for the leap seconds
jd = gps_sec/86400 + 2444244.5;

% note: row = 9 contains 1980-06-30, 24:00:00 UTC and 1980-12-31, 24:00:00 UTC 
% (we should not count leap seconds before --> does not work for negative GPS seconds)
tab = [reshape(leap_sec_table(9:end,4:5)',(size(leap_sec_table,1)-8)*2,1) ...
       reshape(leap_sec_table(9:end,2:3)',(size(leap_sec_table,1)-8)*2,1)];

% reduce the table to those dates, where there was a leap second
tab = tab(tab(:,2)~=0,:);

% adjust for leap seconds (works only corretly with a for loop)
for n = 1:size(tab,1)
    jd(jd > tab(n,1)) = jd(jd > tab(n,1)) - tab(n,2)/86400;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% test code:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% day = [21:30 1:10;]
% month = [6*ones(1,10) 7*ones(1,10)];
% year = 2015*ones(1,20);
% gps_sec = zeros(length(day),1);
% for n = 1:length(day)
%     gps_sec(n) = utc_time_to_gps_sec(year(n),month(n),day(n),0,0,0);
% end
% 
% [jd,jd_test] = gps_sec_to_julian_date(gps_sec);
% diff_in_seconds = (jd - jd_test)*86400
% diff_relative_error = (jd - jd_test)./jd
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% gps_sec = utc_time_to_gps_sec(2015,6,30,23,59,50);
% for n = 2:20
%     gps_sec(n) = gps_sec(n-1) + 1;
% end
% gps_sec = gps_sec';
% 
% 
% [jd,jd_test] = gps_sec_to_julian_date(gps_sec);
% diff_in_seconds = (jd - jd_test)*86400
% diff_relative_error = (jd - jd_test)./jd
% 
% [(50:69)' (jd-jd(1))*86400 (jd_test-jd_test(1))*86400]
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% gps_sec = utc_time_to_gps_sec(2015,6,30,23,59,58);
% for n = 2:40
%     gps_sec(n) = gps_sec(n-1) + 1/10;
% end
% gps_sec = gps_sec';
% 
% [jd,jd_test] = gps_sec_to_julian_date(gps_sec);
% diff_in_seconds = (jd - jd_test)*86400
% diff_relative_error = (jd - jd_test)./jd
% 
% [(58:0.1:62-0.1)' (jd-jd(1))*86400 (jd_test-jd_test(1))*86400]

