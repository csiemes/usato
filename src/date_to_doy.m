%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [doy] = date_to_doy(varargin)
% [doy] = date_to_doy(year,month,day)
% [doy] = date_to_doy(year,month,day,hour,minute,second)
% [doy] = date_to_doy(datevec)
%
% doy ... day of year, including fractional part of day

if nargin == 1
    if numel(varargin{1}) == 1
        dv = datevec(varargin{1});
    else
        dv = varargin{1};
    end
    year = dv(1);
    month = dv(2);
    day = dv(3);
    hour = dv(4);
    minute = dv(5);
    second = dv(6);
elseif nargin == 3
    year = varargin{1};
    month = varargin{2};
    day = varargin{3};
    hour = 0;
    minute = 0;
    second = 0;
elseif nargin == 6
    year = varargin{1};
    month = varargin{2};
    day = varargin{3};
    hour = varargin{4};
    minute = varargin{5};
    second = varargin{6};
end

% number of days per month acc (--> cumsum([0 31 28 31 30 31 30 31 31 30 31 30]))
cum_days = [0,31,59,90,120,151,181,212,243,273,304,334];

% day of year for "normal" year
if size(year,1) == 1
    doy = day + cum_days(month);
else
    doy = day + cum_days(month)';
end

% if leap year, add one day for date > 29 Feb
% if month > 2 && is_leap_year(year)
%     doy = doy + 1;
% end
doy(month > 2 & is_leap_year(year)) = doy(month > 2 & is_leap_year(year)) + 1;

% add fractinoal part
doy = doy + hour/24 + minute/1440 + second/86400;

