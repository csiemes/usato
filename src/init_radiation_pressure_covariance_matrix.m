%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Cx1] = init_radiation_pressure_covariance_matrix(T_std,Tb_std,cav_std,cdv_std,csv_std,cai_std,cdi_std,csi_std,A_std,k_std,C_std,m_std,Cb_std,Pb_std)


% % % % % speed of light, unit = m/s
% % % % c = 299792458;
% % % % 
% % % % % convert standard deviation of fluxes from W/m2 to N/m2
% % % % Ps_std = Ps_std/c;
% % % % Pa_std = Pa_std/c;
% % % % Pi_std = Pi_std/c;

% number of panels
P = numel(A_std);

% number of materials
M = numel(cav_std);

% x = [T(n) Tb(n) cav cdv csv cai cdi csi A k C m Cb Pb Pv Pi]
%
% split into
%    xa = [T(n) Tb(n) cav cdv csv cai cdi csi A k C m Cb P]
%    xb = [Pv Pi]

Cx1 = zeros(4*P+6*M+4,4*P+6*M+4);

Cx1(1:P,1:P) = diag(T_std.^2);
Cx1(P+1,P+1) = Tb_std.^2;
Cx1(P+2:P+1+M,P+2:P+1+M) = diag(cav_std.^2);
Cx1(P+2+M:P+1+2*M,P+2+M:P+1+2*M) = diag(cdv_std.^2);
Cx1(P+2+2*M:P+1+3*M,P+2+2*M:P+1+3*M) = diag(csv_std.^2);
Cx1(P+2+3*M:P+1+4*M,P+2+3*M:P+1+4*M) = diag(cai_std.^2);
Cx1(P+2+4*M:P+1+5*M,P+2+4*M:P+1+5*M) = diag(cdi_std.^2);
Cx1(P+2+5*M:P+1+6*M,P+2+5*M:P+1+6*M) = diag(csi_std.^2);
Cx1(P+2+6*M:2*P+1+6*M,P+2+6*M:2*P+1+6*M) = diag(A_std.^2);
Cx1(2*P+2+6*M:3*P+1+6*M,2*P+2+6*M:3*P+1+6*M) = diag(k_std.^2);
Cx1(3*P+2+6*M:4*P+1+6*M,3*P+2+6*M:4*P+1+6*M) = diag(C_std.^2);
Cx1(4*P+2+6*M,4*P+2+6*M) = m_std^2;
Cx1(4*P+3+6*M,4*P+3+6*M) = Cb_std^2;
Cx1(4*P+4+6*M,4*P+4+6*M) = Pb_std^2;
