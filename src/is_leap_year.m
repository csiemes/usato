%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [leap_year] = is_leap_year(year)
% [ans] = is_leap_year(year_in)
%
% ans = 1, if year is leap year
% ans = 0, if year is not leap year
%
% for algorithm see: http://en.wikipedia.org/wiki/Leap_year

leap_year = false(size(year));
leap_year(mod(year,4)==0) = true;
leap_year(mod(year,100)==0) = false;
leap_year(mod(year,400)==0) = true;

% if mod(year,4) ~= 0        % if year is not divisible by 4 then common year
%     leap_year = false;
% elseif mod(year,100) ~= 0  % else if year is not divisible by 100 then leap year
%     leap_year = true;
% elseif mod(year,400) == 0  % else if year is divisible by 400 then leap year
%     leap_year = true;
% else                       % else common year
%     leap_year = false;
% end

% test code:
% for y = 1998:2111
%     disp(sprintf('%d %d',y,is_leap_year(y))
% end
