%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Ps,Pa,Pi,es,ea,ei] = calculate_radiation_flux_vectors(td,rt,R_c2t,M_alb,M_ir)
% [Ps,Pa,Pi,es,ea,ei] = calculate_radiation_flux_vectors(td,rt,M_alb,M_ir)
%
% td ...... UTC time, unit = days (Matlab's datenum), 1 x 1
% rt ...... satellite position in the Earth-fixed, Earth-centered frame, units = m, 3 x 1
% M_alb ... structure holding albedo maps
% M_ir .... structure holding Earth infrared irradiation maps
%
% radiation flux from:
%   Ps .... sun, unit = W/m2
%   Pa .... albedo map pixels for albedo, unit = W/m2
%   Pi .... Earth infrared map pixels for Earth infrared, unit = W/m2
%
% direction from the satellite to the radiation source
% (unit vector in the Earth-centered, Earth-fixed frame):
%   es .... sun
%   ea .... albedo pixels
%   ei .... Earth infrared pixels


%----------------------------------------------------------------------
% solar flux
%----------------------------------------------------------------------

% convert UTC date (Matlab's datenum) to GPS seconds
gps_sec = utc_time_to_gps_sec(datevec(td));

% calculate the position of the sun in the Earth-centered, celestial frame, unit = m, 3 x 1
rc_sun = low_precision_lunisolar_coordinates_irf(gps_sec);

% calculate the position of the sun in the Earth-centered, Earth-fixed frame, unit = m, 3 x 1
rt_sun = R_c2t * rc_sun;

% 1 astronomical unit
AU = 149597870700; % m

% radiation pressure at distance of 1 AU (W/m2)
P = 1367;

% vector from the sun to the satellite
s2s = rt - rt_sun;

% radiation flux from the sun to the satellite
Ps = P * AU^2 / sum(s2s.^2);

% unit vector that gives the direction from the sun to the satellite, 1 x 3
es = s2s' / sqrt(sum(s2s.^2));


%----------------------------------------------------------------------
% calculate albedo and Earth infrared radiation flux of pixels
%----------------------------------------------------------------------

% interpolate albedo and Earth infrared irradiation maps
ma = interp1(M_alb.doy,M_alb.maps,date_to_doy(td))'; % fraction of radiation that is reflected
mi = interp1(M_ir.doy,M_ir.maps,date_to_doy(td))'; % unit = W/m2

% cosine of angle between vector from the pixel to the sun and the surface normal
cs = sum(-es.*M_alb.position,2) ./ sqrt(sum(M_alb.position.^2,2));

% exclude the parts of Earth from where the solar radiation cannot reach the
% pixel (all pixels where the sun is lower than the local horizon)
cs(cs<0) = 0;

% vectors from the sun to the pixels
s2p = M_alb.position - repmat(rt_sun',numel(M_alb.pixel_areas),1);

% convert fraction of reflected radiation to radiation flux due to albedo, unit = W/m2
ma = P * AU^2 ./ sum(s2p.^2,2) .* ma .* cs;


%----------------------------------------------------------------------
% calculate radiation flux from the pixels to the satellite
%----------------------------------------------------------------------

% vector from pixel to satellite, unit = m
pa2s = repmat(rt',numel(M_alb.pixel_areas),1) - M_alb.position;
pi2s = repmat(rt',numel(M_ir.pixel_areas),1) - M_ir.position;

% unit vectors that give the direction from the pixel to the satellite
ea = pa2s./repmat(sqrt(sum(pa2s.^2,2)),1,3);
ei = pi2s./repmat(sqrt(sum(pi2s.^2,2)),1,3);

% cosine of angle between pixel position and satellite position
ca = sum(ea.*M_alb.position,2) ./ sqrt(sum(M_alb.position.^2,2));
ci = sum(ei.*M_ir.position,2) ./ sqrt(sum(M_ir.position.^2,2));

% exclude the parts of Earth from where the radiation cannot reach the
% satellite (all pixels where the satellite is lower than the local horizon)
ca(ca<0) = 0;
ci(ci<0) = 0;

% radiation flux from pixels to satellite
Pa = ma .* M_alb.pixel_areas .* ca ./ sum(pa2s.^2,2) / pi;
Pi = mi .* M_ir.pixel_areas .* ci ./ sum(pi2s.^2,2) / pi;

% invert direction so that the vectors point from the satellite to the radiation source
es = -es;
ea = -ea;
ei = -ei;


%----------------------------------------------------------------------
% account for Earth's shadow
%----------------------------------------------------------------------

% rotation matrix from Earth-fixed to celestial frame
R_t2c = R_c2t';

% satellite position inc celestial frame, unit = m, 3 x 1
rc = R_t2c * rt;

% shadow fuction (0 = full shadow, 1 = full sun)
sf = solaars_cf_sun_pos_approx(gps_sec,rc');

% % the high precision version:
% sf = solaars_cf(gps_sec,rc');

% reduce radiation flux by shadow function
Ps = Ps * sf;


% % % %=============
% % % R_e2b = reshape(CHK.rot_ecef2sat(1,:),3,3)
% % % R_b2e = R_e2b'
% % % 
% % % % check sun pos --> [OPPOSITE SIGN], relative agreement = 1e-4
% % % h_sun_ecef = R_b2e * CHK.rsun_sat(1,:)'
% % % rt_sun
% % % 
% % % % vector from pixel to satellite in ECEF -> 1e-5 m
% % % pa2s_chk = CHK.rsat_ecef - CHK.rpix_ecef;
% % % pa2s_chk = pa2s_chk(1:size(pa2s_chk,1)/2,:);
% % % x = reshape(reshape(pa2s(:,1),72,144)',144*72,1);
% % % y = reshape(reshape(pa2s(:,2),72,144)',144*72,1);
% % % z = reshape(reshape(pa2s(:,3),72,144)',144*72,1);
% % % h = reshape(reshape(ca,72,144)',144*72,1);
% % % xyz = [x y z];
% % % xyz = xyz(h>0,:);
% % % subplot(3,1,1),plot(pa2s_chk)
% % % subplot(3,1,2),plot(xyz)
% % % subplot(3,1,3),plot(pa2s_chk-xyz)
% % % 
% % % % cos(gamma) -> 1e-11
% % % subplot(2,1,1),plot(h(h>0)),hold on,plot(CHK.cosgamma(1:numel(CHK.cosgamma)/2),'--'),hold off
% % % subplot(2,1,2),plot(h(h>0)-CHK.cosgamma(1:numel(CHK.cosgamma)/2))
% % % 
% % % % pixel area
% % % A = reshape(reshape(M_alb.pixel_areas,72,144)',144*72,1);
% % % h = reshape(reshape(ca,72,144)',144*72,1);
% % % subplot(2,1,1),plot(A(h>0)),hold on,plot(CHK.pix_area(1:numel(CHK.pix_area)/2),'--'),hold off
% % % subplot(2,1,2),plot(A(h>0)-CHK.pix_area(1:numel(CHK.pix_area)/2))
% % % 
% % % % albedo flux
% % % alb = reshape(reshape(ma,72,144)',144*72,1);
% % % h = reshape(reshape(ca,72,144)',144*72,1);
% % % subplot(2,1,1),plot(alb(h>0)),hold on,plot(CHK.pix_alb(1:numel(CHK.pix_alb)/2),'--'),hold off
% % % subplot(2,1,2),plot(alb(h>0)-CHK.pix_alb(1:numel(CHK.pix_alb)/2))
% % % 
