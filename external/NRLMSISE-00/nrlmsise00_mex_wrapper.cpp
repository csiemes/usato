#include "mex.h"
#include "nrlmsise-00.h"   // header for nrlmsise-00.h
#include "math.h"          // maths functions
#include "stdio.h"         // for error messages
#include "stdlib.h"        // for malloc/free

// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    // This is a vectorized version, i.e. 7 element vector is actually a N x 7 matrix.

    double *year;    // year, currently ignored (note: we make the cast to int later)
    double *doy;     // day of year (note: we make the cast to int later)
    double *sec;     // seconds in day (UT), including fraction of seconds
    double *alt;     // altitude in kilometers
    double *g_lat;   // geodetic latitude (degree)
    double *g_long;  // geodetic longitude (degree)
    double *lst;     // local apparent solar time (hours), see note below
    double *f107a;   // 81 day average of F10.7 flux (centered on doy)
    double *f107;    // daily F10.7 flux for previous day
    double *ap;      // magnetic index(daily)
    double *apv;     // Vector with 7 elements containing magnetic values, see calling m file
    double* sw;      // Switches to turn on and off particular variations, see calling m file
    double* d;       // Vector with 9 elements containing densities
    double* t;       // Vector with 2 elements containing temperatures
    
    size_t N;              // number of elements
    const size_t K = 24;   // number of switches
    const size_t M = 7;    // number of magnetic values
    const size_t R = 9;    // number of densities
    const size_t S = 2;    // number of temperatures
    size_t k, m, n, r, s;  // loop variables
    
    struct nrlmsise_output output; // structure for interface to NRLMSISE-00 C code
    struct nrlmsise_input input;   // structure for interface to NRLMSISE-00 C code
    struct nrlmsise_flags flags;   // structure for interface to NRLMSISE-00 C code
    struct ap_array aph;           // structure for interface to NRLMSISE-00 C code
  
    
    // check for proper number of arguments
    if(nrhs!=12)
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper:invalidNumInputs","12 inputs required.");
    if(nlhs!=2)
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper:invalidNumOutputs","2 outputs required.");

    // find the number of elements from the first input
    N = mxGetN(prhs[0])*mxGetM(prhs[0]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetM(prhs[0]) != N || mxGetN(prhs[0]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 1 (year) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 2 (doy) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[2]) || mxGetM(prhs[2]) != N || mxGetN(prhs[2]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 3 (sec) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[3]) || mxGetM(prhs[3]) != N || mxGetN(prhs[3]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 4 (alt) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[4]) || mxGetM(prhs[4]) != N || mxGetN(prhs[4]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 5 (g_lat) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[5]) || mxGetM(prhs[5]) != N || mxGetN(prhs[5]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 6 (g_long) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[6]) || mxGetM(prhs[6]) != N || mxGetN(prhs[6]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 7 (lst) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[7]) || mxGetM(prhs[7]) != N || mxGetN(prhs[7]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 8 (f107a) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[8]) || mxGetM(prhs[8]) != N || mxGetN(prhs[8]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 9 (f107) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[9]) || mxGetM(prhs[9]) != N || mxGetN(prhs[9]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 10 (ap) must be a real N x 1 vector.");
    if( !mxIsDouble(prhs[10]) || mxGetM(prhs[10]) != N || mxGetN(prhs[10]) != M )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 11 (apv) must be a real N x 7 vector.");
    if( !mxIsDouble(prhs[11]) || mxGetM(prhs[11])*mxGetN(prhs[11]) != K )
        mexErrMsgIdAndTxt( "MATLAB:nrlmsise00_mex_wrapper","Input argument 12 (sw) must be a real vector with 24 elements.");

    //  get the inputs
    year   = mxGetPr(prhs[0]);
    doy    = mxGetPr(prhs[1]);
    sec    = mxGetPr(prhs[2]);
    alt    = mxGetPr(prhs[3]);
    g_lat  = mxGetPr(prhs[4]);
    g_long = mxGetPr(prhs[5]);
    lst    = mxGetPr(prhs[6]);
    f107a  = mxGetPr(prhs[7]);
    f107   = mxGetPr(prhs[8]);
    ap     = mxGetPr(prhs[9]);
    apv    = mxGetPr(prhs[10]);
    sw     = mxGetPr(prhs[11]);
    
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix((mwSize)N,(mwSize)9,mxREAL);
    plhs[1] = mxCreateDoubleMatrix((mwSize)N,(mwSize)2,mxREAL);
    
    // get a pointer to the real data in the output matrix
    d = mxGetPr(plhs[0]);
    t = mxGetPr(plhs[1]);

    // copy flags into structure
    for( k = 0; k < K; k++ )
        flags.switches[k] = (int)sw[k];
    
    // calculate densities and temperatures
    for( n = 0; n < N; n++ )
    {
        // copy input values into structure
        input.year = (int)year[n];
        input.doy = (int)doy[n];
        input.sec = sec[n];
        input.alt = alt[n];
        input.g_lat = g_lat[n];
        input.g_long = g_long[n];
        input.lst = lst[n];
        input.f107A = f107a[n];
        input.f107 = f107[n];
        input.ap = ap[n];
        
        for( m = 0; m < M; m++ )
            aph.a[m] = apv[n+m*N];
        
        input.ap_a = &aph;
        
        // call NRLMSISE-00 C code
        gtd7( &input, &flags, &output );
        
        // copy output
        for( r = 0; r < R; r++ )
            d[n+r*N] = output.d[r];
        for( s = 0; s < S; s++ )
            t[n+s*N] = output.t[s];
    }
}
