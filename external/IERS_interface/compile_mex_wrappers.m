% When running Matlab r2023a and earlier:
% system('/usr/local/Cellar/gcc/13.1.0/bin/gfortran -march=x86-64 -c interp.f')

% When running Matlab r2023b onward on MacOS:
system('/opt/homebrew/Cellar/gcc/13.2.0/bin/gfortran -march=native -c interp.f')


mex -O iers_interp.c interp.o

copyfile('iers_interp*.mex*','../../src');
