#include "mex.h"

//*********************************************************************//
//                                                                     //
//  This is the Matlab interface to the Fortran subroutine "interp.f"  //
//  The Fortran subroutine was retrieved from the IERS FTP server.     //
//  It can be used for interpolating the EOP C04 time series.          //
//                                                                     //
//  Matlab call:                                                       //
//                                                                     //
//  [x_int, y_int, ut1_int] = iers_interp( rjd, x, y, ut1, rjd_int );  //
//                                                                     //
//*********************************************************************//

// we use the original provided Fortran code (we need to declare the interface here)
extern void interp_( double *RJD, double *X, double *Y, double *UT1, int *N, double *rjd_int, double *x_int, double *y_int, double *ut1_int );


// the gateway function
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *rjd;               // Epochs, N x 1 matrix, Modified Julian Day (UTC)
    double *x;                 // Polar motion X coordinate, N x 1 matrix, component of unit vector (arcseconds)
    double *y;                 // Polar motion Y coordinate, N x 1 matrix, component of unit vector (arcseconds)
    double *ut1_minus_utc;     // UT1 - UTC time difference, N x 1 matrix, seconds

    double *rjd_int;           // Interpolation epochs, I x 1 matrix, Modified Julian Day (UTC)
    double *x_int;             // Interpolated polar motion X coordinate, I x 1 matrix, component of unit vector (arcseconds)
    double *y_int;             // Interpolated polar motion Y coordinate, I x 1 matrix, component of unit vector (arcseconds)
    double *ut1_minus_utc_int; // Interpolated UT1 time correction, I x 1 matrix, seconds

    int N;                     // Size of input time series "rjd", "x", "y", and "ut1"
    int I;                     // Size of output time series "rjd_int", "x_int", "y_int", and "ut1_int"

    int i;                     // loop variable
    
    
    // check for proper number of arguments
    if(nrhs!=5)
        mexErrMsgIdAndTxt( "MATLAB:iers_interp:invalidNumInputs","5 inputs required.");
    if(nlhs!=3)
        mexErrMsgIdAndTxt( "MATLAB:iers_interp:invalidNumOutputs","3 output required.");

    // find the number of rows from the first input and last input
    N = mxGetM(prhs[0]);
    I = mxGetM(prhs[4]);
    
    // check to make sure the input arguments are real valued and have the right dimension
    if( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:iers_interp","Input argument 1 (Epochs) must be a real N x 1 matrix.");
    if( !mxIsDouble(prhs[1]) || mxGetM(prhs[1]) != N || mxGetN(prhs[1]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:iers_interp","Input argument 2 (Polar motion X coordinate) must be a real N x 1 matrix.");
    if( !mxIsDouble(prhs[2]) || mxGetM(prhs[2]) != N || mxGetN(prhs[2]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:iers_interp","Input argument 3 (Polar motion Y coordinate) must be a real N x 1 matrix.");
    if( !mxIsDouble(prhs[3]) || mxGetM(prhs[3]) != N || mxGetN(prhs[3]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:iers_interp","Input argument 4 (UT1 - UTC time difference) must be a real N x 1 matrix.");
    if( !mxIsDouble(prhs[4]) || mxGetN(prhs[4]) != 1 )
        mexErrMsgIdAndTxt( "MATLAB:iers_interp","Input argument 5 (Interpolation epochs) must be a real I x 1 matrix.");
    
    //  get the inputs
    rjd           = mxGetPr(prhs[0]);
    x             = mxGetPr(prhs[1]);
    y             = mxGetPr(prhs[2]);
    ut1_minus_utc = mxGetPr(prhs[3]);
    rjd_int       = mxGetPr(prhs[4]);
    
    // create the output matrices
    plhs[0] = mxCreateDoubleMatrix((mwSize)I,(mwSize)1,mxREAL);
    plhs[1] = mxCreateDoubleMatrix((mwSize)I,(mwSize)1,mxREAL);
    plhs[2] = mxCreateDoubleMatrix((mwSize)I,(mwSize)1,mxREAL);
    
    // get a pointer to the real data in the output matrix
    x_int             = mxGetPr(plhs[0]);
    y_int             = mxGetPr(plhs[1]);
    ut1_minus_utc_int = mxGetPr(plhs[2]);

    // Interpolate polar motion X, Y coordinates and UT1 - UTC time difference
    // (interpolation adds subdaily variations)
    for( i = 0; i < I; i++ )
        interp_( rjd, x, y, ut1_minus_utc, &N, &rjd_int[i], &x_int[i], &y_int[i], &ut1_minus_utc_int[i] );
}
