%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Uncertainty Specification and Analysis for Thermosphere Observations
%
% Copyright 2024 Christian Siemes
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
%     http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This software uses several external source codes and libraries, which are
% subject to their own licenses. These source codes and libraries are the
% following:
%
% 1) SOFA library
%
%    This software uses the SOFA library for the conversion of time values
%    and for constructing the transformation from the celestial to the 
%    terrestrial reference frame following the IERS conventions. The SOFA
%    library is available under license, where the license text is included
%    in its source code files. This software implements an interface to the
%    SOFA library via Matlab's mex functionality, which was not provided or
%    endorsed by SOFA. Acknowledgement:
% 
%    Software Routines from the IAU SOFA Collection were used. Copyright
%    (c) International Astronomical Union Standards of Fundamental
%    Astronomy (http://www.iausofa.org)
%
% 2) NRLMSISE-00 C code implementation by Dominik Brodowski
%
%    Dominik Brodowski created a C code implementation of the NRLMSISE-00
%    model, which is available in the public domain. The C code can be
%    accessed here: https://www.brodo.de/space/nrlmsise/ (last accessed on
%    7 January 2024)
%
% 3) The "interp.f" source code by Ch. Bizouard, Observatoire de Paris
%
%    The function implements the interpolation of Earth orientation
%    parameters from the IERS. It can be accessed here:
%    ftp://hpiers.obspm.fr/iers/models/interp.f (last accessed on
%    7 January 2024)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%--------------------------------------
% simulation settings
%--------------------------------------

% length of simulation in days and time step in seconds
SIM.period = 0.3; % days
SIM.time_step = 60; % seconds
SIM.consider_averaging = true; % true or false

% option: use real satellite data or simulate orbit and attitude
% SIM.use_real_data = false;
SIM.use_real_data = true;

% 2003
% SIM.sensing_direction = [-1;0;0]; % GA
SIM.sensing_direction = [1;0;0]; % GB

% 2008, 2015, 2017
% SIM.sensing_direction = [1;0;0]; % GA
% SIM.sensing_direction = [-1;0;0]; % GB

SIM.OUTPUT_DIR = './my_output_data/GB_2003_11_01';
% SIM.OUTPUT_DIR = './my_output_data/GA_2003_11_01';

%--------------------------------------
% when using real data (experts only)
%--------------------------------------

DATA.INPUT_FILE = './my_input_data/GB_20031101.txt';
% DATA.INPUT_FILE = './my_input_data/GA_20031101.txt';


%--------------------------------------
% simulated orbit
%--------------------------------------

% define orbit for the case of not using real data (initial state)
ORBIT.altitude_at_apogee = 400e3;       % apogee (m)
ORBIT.altitude_at_perigee = 400e3;      % perigee (m)
ORBIT.inclination = 89;                 % inclination (deg)
ORBIT.argument_of_perigee = 0;          % argument of perigee (deg)
ORBIT.longitude_of_ascending_node = 0;  % longitude of the ascending node (deg)
ORBIT.true_anomaly = 0;                 % true anomaly (deg)


%--------------------------------------
% define satellite (panel model)
%--------------------------------------

% SAT.np = [1 0 0 % front
%          -1 0 0 % rear
%           0 0.766044 -0.642787 % starboard outer
%           0 -0.766044 -0.642787 % port outer
%           0 0 1 % nadir
%           0 0 -1]; % zenith
% SAT.mp = [1; 1; 2; 2; 3; 2]; % material IDs
% SAT.Ap = [0.9551567; 0.9551567; 3.1554792; 3.1554792; 6.0711120; 2.1673620];

% rounded values for paper
SAT.np = [1 0 0 % front
         -1 0 0 % rear
          0 0.7660 -0.6428 % starboard outer
          0 -0.7660 -0.6428 % port outer
          0 0 1 % nadir
          0 0 -1]; % zenith
SAT.mp = [1; 1; 2; 2; 3; 2]; % material IDs
SAT.Ap = [0.955; 0.955; 3.155; 3.155; 6.071; 2.167];

% energy accommodation coefficient of DRIA model
SAT.alpha_E = 0.85;

% initial wall temperature of panels
SAT.Tw = [300,300,300,300,300,300];

% initial body temperature
SAT.Tb = 297;

% reflection coefficients for visible light
SAT.ca_vis = [0.34; 0.34; 0.65; 0.65; 0.12; 0.65];
SAT.cs_vis = [0.40; 0.40; 0.05; 0.05; 0.68; 0.05];
SAT.cd_vis = 1 - SAT.ca_vis - SAT.cs_vis;

% reflection coefficients for infrared light
SAT.ca_ir = [0.62; 0.62; 0.81; 0.81; 0.75; 0.81];
SAT.cs_ir = [0.23; 0.23; 0.03; 0.03; 0.19; 0.03];
SAT.cd_ir = 1 - SAT.ca_ir - SAT.cs_ir;

% heat conductivity and heat capacitance
SAT.kp = [0.1; 0.1; 0.1; 0.1; 0.5; 0.1];
SAT.Cp = [1000; 1000; 5000; 5000; 10000; 5000];
SAT.Cb = 1e5;
SAT.Pb = 70;

% satellite mass (kg) (only used when not using real data)
SAT.ms = 476;


%--------------------------------------
% uncertainty of radiation pressure
%--------------------------------------

RP_COV_INPUTS.kp_std = SAT.kp/5; % 20%
RP_COV_INPUTS.Cp_std = SAT.Cp/5; % 20%
RP_COV_INPUTS.Cb_std = SAT.Cb/5; % 20%
RP_COV_INPUTS.Pb_std = SAT.Pb/5; % 20%
RP_COV_INPUTS.Ap_std = SAT.Ap/50; % 2%
RP_COV_INPUTS.ms_std = 2; % 2 kg / 400 kg = 0.5%

% uncertainty (standard deviation) of reflection coefficients per material
% (the stds must be specified in ascending order of material IDs)
RP_COV_INPUTS.ca_vis_std = [0.1; 0.1; 0.1];
RP_COV_INPUTS.cd_vis_std = [0.1; 0.1; 0.1];
RP_COV_INPUTS.cs_vis_std = [0.1; 0.1; 0.1];
RP_COV_INPUTS.ca_ir_std = [0.1; 0.1; 0.1];
RP_COV_INPUTS.cd_ir_std = [0.1; 0.1; 0.1];
RP_COV_INPUTS.cs_ir_std = [0.1; 0.1; 0.1];

% uncertainty in initial temperatures
RP_COV_INPUTS.T_std = [10 10 10 10 10 10]; % K
RP_COV_INPUTS.Tb_std = 20; % K

% uncertainty in fluxes, as a fraction of the signal
% (global mean albedo factor is about 0.3 - 0.4, depending on season)
% (global mean Earth infrared emission is about 220 W/m2)
RP_COV_INPUTS.Ps_fraction = 0.001; % (0.1%)
RP_COV_INPUTS.Pa_fraction = 0.1; % fraction of pixel (10%)
RP_COV_INPUTS.Pi_fraction = 0.1; % fraction of pixel (10%)


%--------------------------------------
% uncertainty of aerodynamics
%--------------------------------------

AERO_COV_INPUTS.Ta_fraction = 0.2; % std as fraction
AERO_COV_INPUTS.rho_fraction = [0.2; 0.2; 0.2; 0.2; 0.2; 0.2; 0.2; 0.2]; % std as fraction of mass per species in this order: He, O, N2, O2, Ar, H, N, O+
AERO_COV_INPUTS.alpha_E = 0.05; % -
AERO_COV_INPUTS.Ap_std = RP_COV_INPUTS.Ap_std; % m2
AERO_COV_INPUTS.ms_std = RP_COV_INPUTS.ms_std; % kg
AERO_COV_INPUTS.vr_std = [50; 50; 10]; % m/s in satellite body frame ... keep simple for now


%--------------------------------------
% uncertainty of accelerometer/POD
%--------------------------------------

% standard deviation (precision) of accelerometer measurements
ACC.acc_std = [1e-9; 1e-9; 1e-9]; % m/s2 (GRACE)

% define position noise in LORF
ACC.sx = 0.012; % standard deviation assuming one measurement per second (no averaging), m
ACC.sy = 0.012;
ACC.sz = 0.012;

ACC.rho_xy = 0; % no correlation
ACC.rho_xz = 0.9; % x and z are typically highly correlated
ACC.rho_yz = 0;

% averaging length for noise component from evaluating the gravity vector
ACC.dt_gravity_averaging_acc = 86400; % s (assume daily biases)

% averaging window width for accelerations derived from GNSS tracking
ACC.averaging_period = 0.33;
ACC.averaging_unit = 'orbits'; % accepted units are 'orbit', 'seconds', 'days'

% noise coming from numerical differentiation (only along-track is relevant for density)
ACC.position_noise_asd_slope = -0.4; % assume a slight increase (-0.2 gives a factor 10 increase at f = 0.1 mHz)

% sampling period of GNSS tracking
ACC.dt_gnss_tracking = 10.0; % s


