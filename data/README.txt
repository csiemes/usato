The following data is contained in this directory:

1) Earth orientation parameters

   File 'EOP_20_C04_one_file_1962-now.txt' was retrieved from the IERS webpage:
   https://www.iers.org/IERS/EN/DataProducts/EarthOrientationData/eop.html

   It contains the Earth orientation parameters required for constructing the
   transformation from the celestial to the terrestrial reference frame.

2) CERES radiation flux data

   File 'ceres_radiation_flux_data.mat' contains monthly maps of Earth radiation and
   albedo factors, which were derived from the Clouds and the Earth’s Radiant Energy
   System (CERES) project’s Energy Balanced and Filled (EBAF) Top-Of-Atmosphere (TOA)
   all-sky fluxes, edition 4.2, retrieved from the webpage:
   https://ceres.larc.nasa.gov/data/ 

3) F10.7 solar radio flux index and ap index

   The file 'space_weather_indices.mat' contains the F10.7 and ap indices.

   The F10.7 index indicates solar activity and was retrieved from the FTP server:
   ftp://ftp.seismo.nrcan.gc.ca/spaceweather/solar_flux/daily_flux_values

   The ap index indicates geomagnetic activity  and was retrieved from the FTP server:
   ftp://ftp.gfz-potsdam.de/pub/home/obs/kp-ap/wdc/yearly
